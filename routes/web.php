<?php

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use IEXBase\TronAPI\Tron;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', "App\Http\Controllers\User\HomeController@home");
Route::get('/file/{guard}/{id}/{file}',
    function ($guard, $id, $file) {
        $path = storage_path('app/public/tickets/' . $guard . '/' . $id . '/' . $file);
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    })->name('file.ticket.view');

Route::post('convert-to-parameter-save', function (\Illuminate\Http\Request $request) {
    $model = \App\Models\TransactionTron::find($request->id);
    $model->ethers_data = $request->address;
    $model->save();
    return 1;
})->name('convert-to-parameter-save');

// روت هایی برای ورود ادمین
Route::middleware('guest.admin')
    ->prefix('admin')
    ->name('admin.')
    ->namespace('App\Http\Controllers\Admin\Auth')
    ->group(function () {
        Route::get('login', 'LoginController@loginForm')->name('login.form');
        Route::post('login', 'LoginController@login')->name('login');
    });

Route::middleware('guest')->namespace('App\Http\Controllers\User\Auth')->group(function () {
    Route::get('register', 'RegisterController@register')->name('register.form');
    Route::post('register/store', 'RegisterController@store')->name('register.store');
    Route::get('register/{code?}', 'RegisterController@show_registration_form')->name('register');
    Route::get('email/verify/{token}', 'RegisterController@verifyEmail')->name('register.verify.email');
    Route::get('forget/password', 'ForgetPasswordController@forget')->name('forget.form');
    Route::post('forget/send-email', 'ForgetPasswordController@sendEmail')->name('forget.send-email');
    Route::get('forget/verify/{token}', 'ForgetPasswordController@verifyToken')->name('forget.verify-token');
    Route::post('forget/new-password', 'ForgetPasswordController@newPassword')->name('forget.new-password');
    Route::get('login', 'LoginController@loginForm')->name('login.form');
    Route::post('login/user', 'LoginController@login')->name('login.login');
    Route::get('login/step', 'LoginController@login_step_form')->name('login.step.form');
    Route::post('login/step/user', 'LoginController@login_step')->name('login.step')->middleware('2fa.login');
});

// روت های مربوط به کاربر که داخل پنل کاربری لاگین شده
Route::middleware('auth')->namespace('App\Http\Controllers\User')->group(function () {
    Route::get('dashboard', 'HomeController@dashboard')->name('user.dashboard');
    Route::get('settings', 'SettingController@general')->name('user.settings');

    Route::get('invest/cancel/{invest_shares}', 'Invest_SharesController@cancel')->name('user.invest.cancel');
    Route::get('invest/add-to-share/{invest_shares}', 'Invest_SharesController@add_to_share')->name('user.invest.add_to_share');
    Route::get('invest/{invest_shares}/logs', 'Invest_SharesController@logs')->name('user.invest.logs');
    Route::get('shares/{invest_shares}/logs', 'Invest_SharesController@logs')->name('user.shares.logs');
    Route::resource('invest', 'Invest_SharesController', ['as' => 'user']);
    Route::resource('shares', 'Invest_SharesController', ['as' => 'user']);
    Route::get('buy-ctr', 'AssetsController@buy_ctr_view')->name('user.buy-ctr');
    Route::post('buy-ctr', 'AssetsController@buy_ctr_store')->name('user.buy-ctr.store');

    Route::post('register_authenticator', 'SettingController@register_authenticator')->name('user.register_authenticator');
    Route::post('deactivate_authenticator', 'SettingController@deactivate_authenticator')->name('user.deactivate_authenticator');
    Route::post('login_2fa', 'SettingController@login_2fa')->name('user.login_2fa');
    Route::get('logout', 'Auth\LoginController@logout')->name('user.logout');
    Route::get('notifications', 'NotificationController@index')->name('notification.index');
    Route::get('notification/{id}', 'NotificationController@show')->name('notification.show');

    Route::post('assets/{asset_unit}/price-on-usdt', 'AssetsController@price_on_usdt')->name('assets.price_on_usdt');
    Route::post('assets/{asset_unit}/convert-to-usdt', 'AssetsController@convert_to_usdt')->name('assets.convert_to_usdt');

    Route::get('assets', 'AssetsController@index')->name('assets');
    Route::post('assets/get-address-wallet', 'AssetsController@getAddressWallet')->name('assets.get-address-wallet');
    Route::post('assets/get-amount-wallet', 'AssetsController@getAmountWallet')->name('assets.get-amount-wallet');
    Route::post('assets/withdrawal-fee', 'AssetsController@WithdrawalFee')->name('assets.wallet.withdrawal-fee');
    Route::post('assets/withdraw', 'AssetsController@withdraw')->name('assets.withdraw.store')->middleware(['auth', '2fa']);
    Route::post('assets/security-withdraw', 'AssetsController@security_withdraw')->name('assets.withdraw.security');
    Route::post('withdraw', 'AssetsController@withdraw')->name('withdraw.store')->middleware(['auth', '2fa']);
    Route::get('withdraw-report', 'TransactionController@withdraw_report_form')->name('withdraw-report');
    Route::get('/profile', 'DashboardController@profile')->name('profile');
    Route::put('/profile/{user}', 'DashboardController@update_profile')->name('profile.update');
    Route::put('/change-password/{user}', 'DashboardController@change_pass')->name('change-password');
    Route::get('ticket', "TicketController@index")->name('ticket.index');
    Route::get('ticket/create', "TicketController@create")->name('ticket.create');
    Route::get('ticket/{code}/show', "TicketController@show")->name('ticket.show');
    Route::post('ticket/store', "TicketController@store")->name('ticket.store');
    Route::post('ticket/{code}/update', "TicketController@update")->name('ticket.update');
});
