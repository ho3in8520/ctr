<?php

use Illuminate\Support\Facades\Route;
use IEXBase\TronAPI\Tron;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|اینجا وقتیی که کاربر رفته باشه داخل پنلش میتونه دسترسی داشته باشه
روت های لاگین داخل فایل web.php موجود میباشد
*/
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');
Route::post('users/{user}/block-unblock','UsersController@block_unblock')->name('users.block_unblock');
Route::resource('users','UsersController');
Route::get('users/{user}/login-panel', 'UsersController@login_panel')->name('users.login-panel');
Route::resource('invest_shares','Invest_SharesController');
Route::post('invest_shares/profits','Invest_SharesController@inviter_profit_store')->name('invest_shares.inviter_profit.store');
Route::post('invest_shares/cancel-invest','Invest_SharesController@cancel_invest')->name('invest_shares.cancel_invest');
Route::resource('ctr','CtrController')->only(['index','store']);
Route::post('ctr/profits','CtrController@inviter_profit_store')->name('ctr.inviter_profit.store');

Route::get('settings','SettingsController@general')->name('settings.general');
Route::get('settings/withdraws','SettingsController@withdraw')->name('settings.withdraws');
Route::post('settings','SettingsController@update')->name('settings.general.store');
Route::post('settings/currency','SettingsController@currency_update')->name('settings.currency.store');
Route::get('/profile', 'ProfileController@profile')->name('profile');
Route::put('/profile/{user}', 'ProfileController@update_profile')->name('profile.update');
Route::put('/change-password/{user}', 'ProfileController@change_pass')->name('change-password');
Route::get('reports','ReportController@index')->name('reports.index');
Route::resource('notifications','NotificationController');
Route::get('get-user','NotificationController@users')->name('notifications.get_user');
Route::get('get-amount','UsersController@get_amount')->name('get_amount');
Route::post('users/change_wallet','UsersController@change_wallet')->name('users.change_wallet');
Route::post('settings/message','SettingsController@message')->name('settings.message.update');
Route::get('ticket', "TicketsController@index")->name('ticket.index');
Route::get('ticket/create', "TicketsController@create")->name('ticket.create');
Route::get('ticket/{code}/show', "TicketsController@show")->name('ticket.show');
Route::post('ticket/close', "TicketsController@close")->name('ticket.close');
Route::post('ticket/{code}/update', "TicketsController@update")->name('ticket.update');
