// $('.select2').select2({
//     dir: 'rtl',
//     width: '100%',
// });
//////////////// نشان دادن و عدم نشان دادن پسورد /////////////
$(".show-password").click(function () {
    let parent = $(this).parents('.input-group');
    parent.find('span[data-visible]').toggleClass('d-none');
    let elm = parent.find('input');
    if (elm.attr('type') == 'password')
        elm.attr('type', 'text');
    else
        elm.attr('type', 'password');
});
//////////////// نشان دادن و عدم نشان دادن پسورد /////////////

/////////////// اضافه کردن سطر جدید ////////////
/*
حتما سطری که میخواییم ازش کپی بگیریم باید کلاس extended-row رو داشته باشه
حتما باید اون سطر و باتن توی یه والد باشن و والد هم کلاس container-row رو داشته باشه مهم نیس والد چندم باشه
 */
$(".new-row").click(function () {
    let row = $(this).closest('.container-row')
        .find('.extended-row').last();

    row = row.clone();
    row.find('*').val('');
    row.find('textarea').text('');
    $(this).closest('.container-row')
        .find('.extended-row').last().after(row);

})
/////////////// اضافه کردن سطر جدید ////////////

///////////////ساخت اعداد در رنج ////////////
function range(start, end) {
    var ans = [];
    for (let i = start; i <= end; i++) {
        ans.push(parseInt(i));
    }
    return ans;
}

///////////////ساخت اعداد در رنج ////////////

///////////////ساخت اعداد در رنج ////////////
function array_unique(array) {
    return array.filter((v, i, a) => a.indexOf(v) === i);
}

///////////////ساخت اعداد در رنج ////////////

// $('.select2').select2({
//     dir: 'rtl',
//     width: '100%',
// });

var minutes = 1;
var seconds = minutes * 60;

function convertIntToTime(num) {
    var mins = Math.floor(num / 60);
    var secs = num % 60;
    var timerOutput = (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "") + secs;
    return (timerOutput);
}

var countdown = setInterval(function () {
    var current = convertIntToTime(seconds);
    $('timer').html(current);
    if (seconds == 0) {
        clearInterval(countdown);
    }
    seconds--;
    if (seconds >= 0) {
    } else {
        $('timer').hide();
        $('#test').show();
        $('#test').html('<span class="ft-refresh-ccw"</span>');
    }
}, 1000);

$(document).on('focus', ".datePicker", function () {
    $(this).datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        isRTL: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy/mm/dd",
        showAnim: 'slideDown',
        showButtonPanel: true,
        yearRange: "-100:+10",
    });
});


function copyToClipboard(element) {
    console.log('elem', $(element).val());
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).val()).select();

    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch (e) {
        succeed = false;
    }
    $temp.remove();
    return succeed;
}

function swalResponse(status, msg, title = '') {
    switch (status) {
        case 100:
            swal(title, msg, 'success')
            break;
        case 300:
            swal(title, msg, 'warning')
            break;
        case 500:
            swal(title, msg, 'error')
            break;
    }
}

/// ست کردن ایکون دیفالت iconpicker

function setIconPicker() {
    var elem = $(".iconpicker");
    var icon = elem.data('icon-select');
    elem.find('i').attr('class', icon)
    elem.find('input').val(icon)
}

// خروجی گرفتن excel و pdf
$(document).on('click', '.export', function () {
    var export_type = $(this).data('export')
    var url = $(document).find('form').attr('action');
    var data = $(document).find('form').serializeArray();
    var query = $.param(data);
    console.log(url + "?" + query)
    window.open(url + "?" + query + '&export=' + export_type, '_blank');
});

// تابع برای فراخوانی ajax
function ajax(form, func_name = null, params = {}) {
    let action = form.isObject == true ? form.action : form.attr('action');
    let method = form.isObject == true ? form.method : form.attr('method');
    let data = form.isObject == true ? form.data : form.serialize();

    $.ajax({
        url: action,
        type: method,
        data: data,
        success: function (response) {
            // اگه برای برگشت از ajax خواستیم یه تابعی رو اجرا کنیم
            if (func_name != null)
                func_name(response, params);

            if (response.msg) { //
                if (response.status == 200)
                    swal('Success', response['msg'], 'success').then(function () {
                        response.refresh === true ? location.reload() : '';
                    });
                else if (response.status == 500)
                    swal('Error', response['msg'], 'error');
                else if (response.status == 300)
                    swal('Error', response['msg'], 'warning');
            }
        },
        error: function (xhr) {
            if (func_name != null)
                func_name(xhr, params);
            console.log(xhr, xhr.status)
            if (xhr.status === 422) {
                var errorsValidation = JSON.parse(xhr.responseText).errors;
                var errors = '';
                $.each(errorsValidation, function (key, value) {
                    var errArray = key.split('.');
                    var getElement = "";
                    var errorHtml = "<div class='help-block text-danger'>" + value + "</div>";
                    errors += value + '\n';
                    if (typeof errArray[1] === 'undefined') {
                        getElement = "[name='" + key + "']";
                        showHtmlValidation(getElement, errorHtml)
                        getElement = "[name='" + key + "[]" + "']";
                        showHtmlValidation(getElement, errorHtml)
                    } else if (!$.isNumeric(errArray[1])) {
                        getElement = "[name='" + (errArray[0] + "[" + errArray[1] + "]']");
                        showHtmlValidation(getElement, errorHtml)
                    } else if ($.isNumeric(errArray[1])) {
                        getElement = "[name^='" + errArray[0] + "']";
                        showHtmlValidation(getElement, errorHtml, errArray[1])
                    }
                });
                swal('Error', errors, 'error');
            } else
                swal('Error', 'Pls contact with admin', 'error');
        }
    });
}

$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});

// متوقف کردن و شروع دوباره marquee
// $('marquee').mouseover(function() {
//     $(this).attr('scrollamount',0);
// }).mouseout(function() {
//     $(this).attr('scrollamount',5);
// });
