import {ethers} from "../tron/ethers-5.0.esm.min.js";

$(document).ready(async function () {

    var address_hex = $(".address").val();
    var amount = $(".amount").val();
    var route = $(".route").val();
    var csrf = $(".csrf").val();
    var id = $(".id").val();

    const AbiCoder = ethers.utils.AbiCoder;

    const ADDRESS_PREFIX_REGEX = /^(41)/;
    const ADDRESS_PREFIX = "41";

    async function encodeParams(inputs) {
        let typesValues = inputs;
        let parameters = '';

        if (typesValues.length == 0)
            return parameters
        const abiCoder = new AbiCoder();
        let types = [];
        const values = [];

        for (let i = 0; i < typesValues.length; i++) {
            let {type, value} = typesValues[i];
            if (type == 'address')
                value = value.replace(ADDRESS_PREFIX_REGEX, '0x');
            else if (type == 'address[]')
                value = value.map(v => toHex(v).replace(ADDRESS_PREFIX_REGEX, '0x'));
            types.push(type);
            values.push(value);
        }

        try {
            parameters = abiCoder.encode(types, values).replace(/^(0x)/, '');
        } catch (ex) {
            console.log(ex);
        }
        return parameters

    }

    async function decodeParams(inputs) {
        let typesValues = inputs;
        let types = [];
        for (let i = 0; i < typesValues.length; i++) {
            let {type, value} = typesValues[i];
            if (type == 'address')
                value = value.replace(ADDRESS_PREFIX_REGEX, '0x');
            else if (type == 'address[]')
                value = value.map(v => toHex(v).replace(ADDRESS_PREFIX_REGEX, '0x'));
            types.push(type);
            values.push(value);
        }
        const abiCoder = new AbiCoder();
        try {
            var parameters = abiCoder.decode(types, typesValues).replace(/^(0x)/, '');
        } catch (ex) {
            console.log(ex);
        }
        return parameters

    }

    let inputs2 = [
        {type: 'address', value: address_hex},
        {type: 'uint256', value: amount},
    ];

    const owner_address = await encodeParams(inputs2);
    console.log(owner_address)
    $.post(route, {address: owner_address,_token:csrf,id:id})
})
