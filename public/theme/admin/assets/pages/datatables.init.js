/*
 Template Name: Zinzer - Responsive Bootstrap 4 Admin Dashboard
 Author: Themesdesign
 Website: www.themesdesign.in
 File: Datatable js
 */

$(document).ready(function() {
    $('#datatable').DataTable({
        "language": {
            "lengthMenu": "نمایش _MENU_ رکورد در صفحه",
            "zeroRecords": "موردی وجود ندارد",
            "info": "صفحه _PAGE_ از _PAGES_",
            "infoEmpty": "موردی وجود ندارد",
            "infoFiltered": "(فیلتر کردن _MAX_ کل رکورد ها)",
            "paginate": {
                "next": "بعدی",
                "previous": "قبلی"
            },
            "search": "جستجو",
        }
    });

    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container()
        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );
