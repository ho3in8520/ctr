<?php

namespace App\Observers;

use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function created(User $user)
    {
        $asset = [
            ['user_id' => $user->id, 'unit' => 'usdt', 'name' => 'USDT', 'amount' => 0, 'logo' => 'usdt','type_token'=>20],
            ['user_id' => $user->id, 'unit' => 'ctr', 'name' => 'CoinTread', 'amount' => 0, 'logo' => 'ctr','type_token'=>20],
            ['user_id' => $user->id, 'unit' => 'btt', 'name' => 'BitTorrent', 'amount' => 0, 'logo' => 'btt','type_token'=>10],
        ];
        Asset::insert($asset);
        // send message notification user
        $message_notification = BaseData::where('type', 'message_notification')->where('name', 'new_register_user')->first();
        if ($message_notification) {
            $notification = new Notification();
            $notification->user_id = $user->id;
            $notification->title = $message_notification->extra_field1;
            $notification->description = $message_notification->extra_field2;
            $notification->started_at = Carbon::now();
            if ($notification->save())
                return true;
            return false;
        }
    }

    /**
     * Handle the User "updated" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
