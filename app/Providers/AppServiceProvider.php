<?php

namespace App\Providers;

use App\Http\Controllers\User\AssetsController;
use App\Models\BaseData;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $price_currency= (new AssetsController())->price_on_usdt(['btt']);
        $ctr_price= BaseData::ctr_price()->first(['extra_field1']);
        $price_currency['ctr']= $ctr_price?$ctr_price->extra_field1:0;
        View::share('price_currency', $price_currency);
        Schema::defaultStringLength(191);
        Paginator::useBootstrap();
    }
}
