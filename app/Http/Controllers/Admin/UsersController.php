<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Finance_transaction;
use App\Models\Invest_Shares;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    // نشان دادن لیست کاربران
    public function index(Request $request)
    {
        $users = User::query();
        $request->code ? $users->where('code', 'like', '%' . $request->code . '%') : '';
        $request->email ? $users->where('email', 'like', '%' . $request->email . '%') : '';
        $request->first_name ? $users->where('first_name', 'like', '%' . $request->first_name . '%') : '';
        $request->last_name ? $users->where('last_name', 'like', '%' . $request->last_name . '%') : '';
        $request->status ? $users->where('status', $request->status) : '';
        if ($request->has('export')) {
            $data = $users->get();
            switch ($request->export) {
                case 'excel':
                    return exportExcel($this->dataToExport($data));
                    break;
                case 'pdf':
                    return exportPdf($this->dataToExport($data));
                    break;
            }
            return exportExcel($this->dataToExport($data));
        }
        $users = $users->paginate(10);
        return view('admin.users.index', compact('users'));
    }

    // نشان دادن اطلاعات یک کاربر
    public function show(User $user)
    {
        $invest_shares = $user
            ->invest_shares()
            ->join('base_data', function ($join) {
                $join
                    ->on('invest_shares.type', '=', 'base_data.type')
                    ->where(function ($query) {
                        $query
                            ->where(function ($query) {
                                $query
                                    ->where('initial_value', '>=', DB::raw('base_data.extra_field1'))
                                    ->where('initial_value', '<', DB::raw('base_data.extra_field2'));
                            })
                            ->orWhere(function ($query) {
                                $query
                                    ->where('invest_shares.initial_value', '>=', DB::raw('base_data.extra_field1'))
                                    ->where('base_data.extra_field2', '-1');
                            });
                    });
            })
            ->get(['invest_shares.id as id', 'invest_shares.type', 'initial_value', 'invest_shares.status', 'profit', 'extra_field3 as percent', 'invest_shares.created_at', 'invest_shares.updated_at']);
        return view('admin.users.show', compact('user', 'invest_shares'));
    }

    // ویرایش اطلاعات کاربری
    public function update(User $user, Request $request)
    {
        if (!$request->ajax())
            return redirect()->back();
        try {
            if (is_null($request->password) || !$request->password)
                unset($request['password']);
            else
                $request['password'] = Hash::make($request->password);
            $user->update($request->except(['id', 'code', 'google2fa_secret', 'email_verified_at', 'verification', 'time_verification', 'reject_reason', 'remember_token', 'deleted_at', 'created_at', 'updated_at']));
            return response()->json(['status' => 100, 'msg' => 'تغییرات با موفقیت اعمال شد']);
        } catch (\Exception $exception) {
            dd($exception);
            return response()->json(['status' => 500, 'msg' => 'مشکلی پیش آمده است لطفا مجدد تلاش کنید']);
        }
    }

    // بلاک و آنبلاک کردن کاربر
    public function block_unblock(User $user, Request $request)
    {
        if (!$request->ajax())
            return redirect()->back();

        try {
            if ($user->status == 2) {
                $user->status = 0;
                $user->reject_reason = '';
                $msg = 'کاربر مورد نظر با موفقیت آنبلاک شد!

                 وضعیت پنل کاربر به غیرفعال تغییر کرد.';
            } else {
                $user->status = 2;
                $user->reject_reason = $request->reject_reason;
                $msg = 'کاربر مورد نظر با موفقیت بلاک شد!';
            }
            $user->save();
            return response()->json(['status' => 100, 'msg' => $msg]);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی پیش آمده است لطفا مجدد تلاش کنید']);
        }
    }

    // لاگین کردن با کاربر داخل پنل
    public function login_panel(User $user)
    {
        Auth::logout();
        Auth::login($user);
        return redirect('/dashboard');
    }

    public function get_amount(Request $request)
    {
        try {
            $amount = Asset::query()
                ->select('id', 'amount')
                ->where('user_id', $request['user_id'])
                ->where('unit', $request['unit'])
                ->get();

            return response()->json($amount);
        } catch (\Exception $exception) {
            return response()->json(['error' => 'error']);
        }
    }

    public function change_wallet(Request $request)
    {
        $request->validate([
            'unit' => 'required',
            'amount' => 'required|numeric',
            'transact_type' => 'required',
        ]);

        try {
            $asset = Asset::where('user_id', $request->user_id)
                ->where('unit', $request->unit)->first();
            if ($request['amount'] > $asset->amount && $request['transact_type'] == 'decrease') {
                return response()->json(['status' => 500, 'msg' => 'مقدار کاهش بیشتر از موجودی کاربر می باشد.']);
            }
            if ($request['transact_type'] == 'increase') {
                $type = 2;
                $description = 'wallet amount increased by admin (' . $request['unit'] . ')';
                $base_data = BaseData::where('type', 'transactions')->where('extra_field1', 10)->first()->id;
                $amount = $asset->amount + $request['amount'];
            } else {
                $type = 1;
                $description = 'wallet amount decreased by admin (' . $request['unit'] . ')';
                $base_data = BaseData::where('type', 'transactions')->where('extra_field1', 11)->first()->id;
                $amount = $asset->amount - $request['amount'];
            }

            $asset->update([
                'amount' => $amount,
            ]);

            Finance_transaction::create([
                'financeable_id' => $asset->id,
                'financeable_type' => 'App\Models\Asset',
                'tracking_code' => '',
                'user_id' => $request['user_id'],
                'transact_type' => $base_data,
                'amount' => $request['amount'],
                'type' => $type,
                'extra_field1' => '',
                'description' => $description,
            ])->save();

            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام شد.']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است لطفا بعدا امتحان کنید.']);
        }

    }


    protected function dataToExport($data)
    {
        $array = [];
        $i = 1;
        foreach ($data as $item) {
            array_push($array, [
                '#' => $i,
                'نام و نام خانوادگی' => $item->name,
                'ایمیل' => $item->email,
                'کد' => $item->code,
                'وضعیت پنل' => ($item->status == 1) ? 'فعال' : 'غیرفعال',
                'تاریخ ثبت نام' => Carbon::parse($item->created_at)->format('Y/m/d'),
            ]);
            $i++;
        }
        return $array;
    }
}
