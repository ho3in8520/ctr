<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\Finance_transaction;
use App\Models\Invest_Shares;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $assets = Asset::query()
            ->distinct()
            ->get(['unit', 'name']);
        $reports = Finance_transaction::query();
        \request()->type && in_array(\request()->type, [1, 2]) ? $reports->where('type', \request()->type) : '';
        \request()->amount ? $reports->where('amount', \request()->amount) : '';
        \request()->start_created_at ? $reports->where('created_at', '>=', \request()->start_created_at) : '';
        \request()->end_created_at ? $reports->where('created_at', '<=', \request()->end_created_at) : '';
        \request()->description ? $reports->where('description', 'like', '%' . \request()->description . '%') : '';
        if (\request()->currency) {
            $reports->whereHasMorph(
                'financeable',
                [Invest_Shares::class, Asset::class],
                function (Builder $query, $type) {
                    $column = $type === Invest_Shares::class ? 'asset' : 'unit';
                    $query->where($column, \request()->currency);
                }
            );
        }
        if (\request()->email) {
            $reports->whereHas('user',function ($q) use ($request) {
                return $q->where('email','like',"%{$request->email}%");
            });
        }
        if (\request()->code) {
            $reports->whereHas('user',function ($q) use ($request) {
                return $q->where('code','like',"%{$request->code}%");
            });
        }
        $reports->orderBy('created_at', 'desc');
        if ($request->has('export')) {
            $data = $reports->get();
            switch ($request->export) {
                case 'excel':
                    return exportExcel($this->dataToExport($data));
                    break;
                case 'pdf':
                    return exportPdf($this->dataToExport($data));
                    break;
            }
            return exportExcel($this->dataToExport($data));
        }
        $reports = $reports->paginate(20);

        return showData(view('admin.reports.index', compact('reports', 'assets')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Finance_transaction $finance_transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Finance_transaction $finance_transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Finance_transaction $finance_transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Finance_transaction $finance_transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Finance_transaction $finance_transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Finance_transaction $finance_transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Finance_transaction $finance_transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Finance_transaction $finance_transaction)
    {
        //
    }

    protected function dataToExport($data)
    {
        $array = [];
        $i = 1;
        foreach ($data as $item) {
            array_push($array, [
                '#' => $i,
                'واریز/برداشت' => $item->type == 1 ? "برداشت" : "واریز",
                'کاربر' => $item->user->name,
                'مقدار' => $item->amount,
                'ارز' => $item->financeable instanceof \App\Models\Invest_Shares ? $item->financeable->asset : $item->financeable->unit,
                'توضیحات' => $item->description,
                'تاریخ' => $item->created_at,
            ]);
            $i++;
        }
        return $array;
    }

}
