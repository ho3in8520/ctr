<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profile()
    {
        $user = auth()->guard('admin')->user();

        return view('admin.profile', compact('user'));
    }

    public function update_profile(Request $request, $user)
    {
        $user = Admin::findOrFail($user);
        $request->name && $request->has('name') && !empty($request->name) ? $user->name = $request->name : $user->name = '';
        $request->family && $request->has('family') && !empty($request->family) ? $user->family = $request->family : $user->family = '';
        try {
            $user->update();
            return response()->json(['status'=>100,'msg'=>'اطلاعات شما با موفقیت بروزرسانی شد.']);
        } catch (\Exception $exception) {
            return response()->json(['status'=>500,'msg'=>'خطایی رخ داده است. لطفا بعدا امتحان کنید.']);
        }

    }

    public function change_pass(Request $request,$user)
    {
        $user=Admin::findOrFail($user);
        $request->validate([
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
        ]);
        try {
            $user->update([
                'password' => Hash::make($request['password'])
            ]);
            return response()->json(['status'=>100,'msg'=>'رمز عبور شما با موفقیت عوض شد.']);
        }catch (\Exception $exception){
            return response()->json(['status'=>500,'msg'=>'مشکلی رخ داده است.لطفا بعدا امتحان کنید.']);
        }
    }
}
