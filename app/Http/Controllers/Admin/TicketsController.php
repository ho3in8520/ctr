<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TicketMaster;
use App\Models\UploadedFile;
use Illuminate\Http\Request;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = TicketMaster::query();
        if (\request()->has('code') && !empty(\request('code'))) {
            $result->where('code', \request('code'));
        }
        if (\request()->has('title') && !empty(\request('title'))) {
            $result->where('title', 'like', '%' . \request('title') . '%');
        }
        if (\request()->has('priority') && !empty(\request('priority'))) {
            $array = [3 => 0, 1 => 1, 2 => 2];
            $result->where('priority', \request('priority'));
        }
        if (\request()->has('status') && !empty(\request('status'))) {
            $result->where('status', $array[\request('status')]);
        }
        $result = $result->orderBy('id', 'desc')->paginate(10);
        return showData(view('admin.ticket.index', compact('result')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ticket.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function close(Request $request)
    {
        $request->validate([
            'key' => 'required|exists:ticket_masters,id',
        ]);
        try {
            $model = TicketMaster::findOrFail($request->key);
            $model->status = 3;
            $model->save();
            return response()->json(['status' => 100, 'msg' => 'تیکت با موفقیت بسته شد']);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است. لطفا بعدا امتحان کنید.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        $result = TicketMaster::where('code', $code)->with(['ticket_detail' => function ($q) {
            $q->select([
                '*',
                'attachment' => UploadedFile::select('path')->whereColumn("ticket_details.id", 'uploaded_files.uploadable_id')
                    ->where('uploadable_type', 'App\Models\TicketDetail')->limit(1)
            ])->with(['user', 'admin'])->orderBy('id', 'desc');
        }])->firstOrFail();
        return view('admin.ticket.show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code)
    {
        $request->validate([
            'description' => 'required|string',
            'attachment' => 'nullable|mimes:zip,rar,pdf,png,jpg,jpeg|max:2094',
            'g-recaptcha-response' => 'recaptcha',
        ]);
        try {
            $user = auth('admin')->user();
            $master = TicketMaster::where('code', $code)->firstOrFail();
            $detail = $master->ticket_detail()->create([
                'user_id' => $user->id,
                'type' => 2,
                'description' => $request->description,
            ]);
            $master->status = 2;
            $master->save();
            if ($request->has('attachment') && !empty($request->attachment)) {
                $detail->files()->create([
                    'user_id' => $user->id,
                    'mime_type' => $request->file('attachment')->getMimeType(),
                    'size' => $request->file('attachment')->getSize(),
                    'path' => upload_ticket($request->attachment),
                    'type' => 'ticket',
                ]);
            }
            alert('success', 'Operation successfully', 'success');
            return redirect()->back();
        } catch (\Exception $e) {
            alert('error', 'Error performing operations', 'error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
