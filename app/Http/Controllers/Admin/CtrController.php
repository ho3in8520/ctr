<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BaseData;
use Illuminate\Http\Request;

class CtrController extends Controller
{
    public function index()
    {
        $profit_inviter= BaseData::query()
            ->where('type','profit_inviter')
            ->where('name','buy-ctr')
            ->first();
        return view('admin.ctr.index',compact('profit_inviter'));
    }

    /**
     * تنظیم کردن درصد و نوع سود برای بالاسری
     * @param Request $request
     */
    public function inviter_profit_store(Request $request)
    {
        $request->validate([
            'unit' => 'required|in:ctr,btt,usdt',
            'percent' => 'required|numeric',
        ]);
        try {
            BaseData::query()
                ->where('type', 'profit_inviter')
                ->where('name', 'buy-ctr')
                ->update([
                    'extra_field1' => $request->unit,
                    'extra_field2' => $request->percent,
                ]);
            return response()->json(['status' => 100, 'msg' => 'تنظیمات با موفقیت ذخیره شد']);
        }catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => "مشکلی بوجود آمده است!"]);
        }
    }
}
