<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\BaseData;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    // صفحه ی تنظیمات کلی
    public function general()
    {
        $base_data = BaseData::query()
            ->whereIn('type', ['currency_price'])
            ->get();
        $ctr_price = $base_data->where('name', 'ctr')->where('type', 'currency_price')->first()->extra_field1;
        $general = [
            'ctr_price' => $ctr_price,
        ];

        // message welcome new user
        $message = BaseData::where('type', 'message_notification')->first();

        return view('admin.settings', compact('general', 'message'));
    }


//    تنظیمات واریز و برداشت
    public function withdraw()
    {
        $currencies=BaseData::query()
            ->whereIn('type',['min_withdraw_usdt','min_withdraw_btt','min_withdraw_ctr','fee_withdraw_usdt','fee_withdraw_btt','fee_withdraw_ctr'])
            ->get();
        $min_withdraw_usdt=$currencies->where('type','min_withdraw_usdt')->first()->extra_field1;
        $fee_withdraw_usdt=$currencies->where('type','fee_withdraw_usdt')->first()->extra_field1;
        $min_withdraw_btt=$currencies->where('type','min_withdraw_btt')->first()->extra_field1;
        $fee_withdraw_btt=$currencies->where('type','fee_withdraw_btt')->first()->extra_field1;
        $min_withdraw_ctr=$currencies->where('type','min_withdraw_ctr')->first()->extra_field1;
        $fee_withdraw_ctr=$currencies->where('type','fee_withdraw_ctr')->first()->extra_field1;

        $currencies_value=[
            'min_withdraw_usdt'=>$min_withdraw_usdt,
            'fee_withdraw_usdt'=>$fee_withdraw_usdt,
            'min_withdraw_btt'=>$min_withdraw_btt,
            'fee_withdraw_btt'=>$fee_withdraw_btt,
            'min_withdraw_ctr'=>$min_withdraw_ctr,
            'fee_withdraw_ctr'=>$fee_withdraw_ctr,
        ];

        return view('admin.withdraw', compact('currencies_value'));
    }

    // اپدیت کردن تنظیمات
    public function update(Request $request)
    {
        $request->validate([
            'ctr_price' => 'required|numeric|min:0',
        ]);

        try {
            $base_data = BaseData::query()
                ->whereIn('type', ['currency_price'])
                ->get();

            $general = [
                'ctr_price' => ['type' => 'currency_price', 'name' => 'ctr'],
            ];

            $base_data_upsert = [];
            foreach ($general as $key => $item) {
                $base_data2 = $base_data;
                $value = $request["$key"];
                foreach ($general["$key"] as $key => $condition) {
                    $base_data2 = $base_data2->where("$key", "$condition");
                }
                $base_data_item = $base_data2->first();
                $base_data_upsert[] = [
                    'id' => $base_data_item->id,
                    'type' => $base_data_item->type,
                    'name' => $base_data_item->name,
                    'extra_field1' => $value,
                ];
            }// End foreach
            BaseData::query()
                ->upsert($base_data_upsert, ['id'], ['extra_field1']);

            return response()->json(['status' => 100, 'msg' => 'تنظیمات با موفقیت ذخیره شدند']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی پیش آمده است']);
        }
    }

    public function currency_update(Request $request)
    {
        $request->validate([
            'min_withdraw_usdt' => 'required|numeric|min:0',
            'fee_withdraw_usdt' => 'required|numeric|min:0',
            'min_withdraw_ctr' => 'required|numeric|min:0',
            'fee_withdraw_ctr' => 'required|numeric|min:0',
            'min_withdraw_btt' => 'required|numeric|min:0',
            'fee_withdraw_btt' => 'required|numeric|min:0',
        ]);

        try {
            $data = BaseData::query()
                ->whereIn('type', ['min_withdraw_usdt', 'min_withdraw_btt', 'min_withdraw_ctr', 'fee_withdraw_usdt', 'fee_withdraw_btt', 'fee_withdraw_ctr'])
                ->get();

            $total = [
                'min_withdraw_usdt' => ['type' => 'min_withdraw_usdt', 'name' => 'حداقل برداشت usdt'],
                'fee_withdraw_usdt' => ['type' => 'fee_withdraw_usdt', 'name' => 'کارمزد برداشت usdt'],
                'min_withdraw_ctr' => ['type' => 'min_withdraw_ctr', 'name' => 'حداقل برداشت ctr'],
                'fee_withdraw_ctr' => ['type' => 'fee_withdraw_ctr', 'name' => 'کارمزد برداشت ctr'],
                'min_withdraw_btt' => ['type' => 'min_withdraw_btt', 'name' => 'حداقل برداشت btt'],
                'fee_withdraw_btt' => ['type' => 'fee_withdraw_btt', 'name' => 'کارمزد برداشت btt'],
            ];

            $base_data_upsert = [];
            foreach ($total as $key => $item) {
                $base_data2 = $data;
                $value = $request["$key"];
                foreach ($total["$key"] as $key1 => $condition) {
                    $base_data2 = $base_data2->where("$key1", "$condition");
                }
                $base_data_item = $base_data2->first();
                $base_data_upsert[] = [
                    'id' => $base_data_item->id,
                    'type' => $base_data_item->type,
                    'name' => $base_data_item->name,
                    'extra_field1' => $value,
                ];
            }// End foreach

            BaseData::query()
                ->upsert($base_data_upsert, ['id'], ['extra_field1']);

            return response()->json(['status' => 100, 'msg' => 'تنظیمات با موفقیت ذخیره شدند']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی رخ داده است']);
        }
    }

    public function message(Request $request)
    {
        $request->validate([
            'extra_field1' => 'required|string',
            'extra_field2' => 'required|string',
        ]);
        try {
            ///// notification ////
            BaseData::updateOrCreate([
                'type' => 'message_notification'
            ], $request->all());
            ///// email //////
            BaseData::updateOrCreate([
                'type' => 'message_email'
            ], $request->all());
            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجم گردید']);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی رخ داده است']);
        }
    }
}
