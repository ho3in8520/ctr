<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\NotificationView;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        $user = auth('web')->user();
        $notifications = Notification::with('viewNotification')
            ->where('user_id', $user->id)
            ->where('started_at', '<=', Carbon::now())
            ->orWhere('user_id', 0)
            ->where('started_at', '<=', Carbon::now())
            ->orderBy('id', 'desc')->paginate(10);
        return view('user.notification.index', compact('notifications'));
    }

    public function show($id)
    {
        $id = base64_decode($id);
        $user = auth('web')->user();
        $notification = Notification::with('viewNotification')
            ->where('id', $id)
            ->where('user_id', $user->id)
            ->where('started_at', '<=', Carbon::now())
            ->orWhere('user_id', 0)->where('id', $id)
            ->where('started_at', '<=', Carbon::now())
            ->firstOrFail();
        if (!$notification->viewNotification) {
            try {
                $this->updateView($notification, $user);
            } catch (\Exception $e) {
                return redirect()->route('notification.index');
            }
        }
        return view('user.notification.show', compact('notification'));
    }

    private function updateView($data, $user)
    {
        $notification = $data;
        $notification->view = 1;
        $notification->save();
        NotificationView::firstOrCreate([
            'notification_id' => $data->id,
        ], [
            'notification_id' => $data->id,
            'user_id' => $user->id
        ]);
    }
}
