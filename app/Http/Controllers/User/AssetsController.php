<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Jobs\InviterProfit;
use App\Library\tron\Tron;
use App\Mail\sendMessageAdmin;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\GlobalMarketApi;
use App\Models\User;
use App\Models\Finance_transaction;
use App\Rules\Unit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class AssetsController extends Controller
{
    use Tron;

    protected $trc10 = ['btt' => 1002000];

    public function index()
    {
        $user = auth()->user();
        $assets = Asset::where('user_id', $user->id)->get();
        $currency_prices = GlobalMarketApi::select([
            '*',
            'ctr' => BaseData::select('extra_field1')->where('type', 'currency_price')->where('name', 'ctr')->limit(1)
        ])->price_currency()->first();
        if (!empty($assets[0]->token)) {
            $this->checkAmountUnit($assets[0]->address);
        }
        return view('user.assets.index', compact('assets', 'currency_prices'));
    }

    public function getAddressWallet(Request $request)
    {
//        dd(intval('0000000000000000000000000000000000000000000000000000000000002710', 16) / 1000000);
//        dd(Tron()->getBalanceUsdt('0000000000000000000000000a5c42bba10929619126dc5e94c9c5fb230fd55b'));
//        dd(Tron()->getTransaction('0dbca43a999e22b5075de5e6241d0427d067bfb20e5d09b3e2ad66a4c1f0882e')['ret'][0]['contractRet']);

//        dd(Tron()->getBalanceUsdt('0000000000000000000000000a5c42bba10929619126dc5e94c9c5fb230fd55b'));
//        dd(Tron()->getBalance('TAuzHMv4ijyrLeSCCHAShCzhqgCmcNKQA5'));
        $request->validate([
            'type' => [new Unit()]
        ]);
        $account = '';
        $user = auth()->user();
        $asset = Asset::where('user_id', $user->id)->where('unit', $request->type)->first();
        if (empty($asset)) {
            return response()->json(['status' => 300, 'msg' => 'Error performing operations']);
        } elseif (empty($asset->token)) {
            $account = $this->createAccountTron($request->type, $user);
        } else {
            $account = $asset->token;
        }
        $address = json_decode($account)->address_base58;
        return view('user.assets.get_address_wallet', compact('address'));
    }

    private function createAccountTron($type, $user)
    {
        $account = Tron()->createAccount();
        $account = json_encode($account->getRawData());
        $asset = Asset::where('user_id', $user->id)->update(['token' => $account]);
        return $account;
    }

    public function getAmountWallet(Request $request)
    {
        $address = $request->address;
        if (empty($address)) {
            return response()->json(['data' => ['usdt' => 0, 'btt' => 0, 'ctr' => 0], 'status' => 100]);
        }
        $result = [];
        $amount = [];
        $user = auth()->user();
        try {

            $assets = Asset::where('user_id', $user->id)->get();
            $result['usdt'] = Tron()->getBalanceUsdt($address);
            $result['ctr'] = Tron()->getBalanceCtr($address);

            $result['btt'] = Tron()->getTokenBalance($this->trc10['btt'], $assets[0]->addressHex);
            /// Convert to price
            foreach ($assets as $key => $asset) {
                if (array_key_exists($asset->unit, $result) && $asset->type_token == 20) {

                    $amount[$asset->unit] = intval($result[$asset->unit]['constant_result'][0], 16) / 1000000;
                    // if update amount
                    if ($amount[$asset->unit] > $asset->amount) {
                        $difference = $amount[$asset->unit] - $asset->amount;
                        $asset->amount += $difference;
                        $asset->save();
                    }
                } elseif (array_key_exists($asset->unit, $result) && $asset->type_token == 10) {
                    $amount[$asset->unit] = $result[$asset->unit] / 1000000;
                    // if update amount
                    if ($amount[$asset->unit] > $asset->amount) {
                        $difference = $amount[$asset->unit] - $asset->amount;
                        $asset->amount += $difference;
                        $asset->save();
                    }
                }
            }
            $unit_amount = $asset->pluck('amount', 'unit');
            return response()->json(['data' => $unit_amount, 'status' => 100]);
        } catch (\Exception $e) {
            return response()->json(['msg' => 'Error receiving information', 'status' => 500]);
        }
    }

    private function getAllCurrencies($address)
    {
        $result = Tron()->getAccountInfo($address);
        dd($result);
    }

    public function WithdrawalFee(Request $request)
    {
        $request->validate([
            'unit' => [new Unit(), 'required'],
            'trc' => 'in:10,20'
        ]);
        $result = BaseData::select([
            DB::raw("extra_field1 as fee"),
            'min' => BaseData::select('extra_field1')->where('type', 'min_withdraw_' . $request->unit)->limit(1)
        ])->where('type', 'fee_withdraw_' . $request->unit)->first()->toArray();
        return response()->json(['status' => 100, 'data' => $result]);
    }

//برداشت ارز
    public function withdraw(Request $request)
    {
        $user = auth()->user();
        $request->validate([
            'unit' => ['required', new Unit()],
            'amount' => 'required|between:0,99.99',
            'address' => 'required',
            'token' => 'required',
            'code' => [function ($attr, $val, $fail) use ($user) {
                if ($user->confirm_type == 'sms' && $val == '') {
                    $fail('Code is required');
                }
            }],
            'one_time_password' => [function ($attr, $val, $fail) use ($user) {
                if ($user->confirm_type == 'google-authenticator' && $val == '') {
                    $fail('Code is required');
                }
            }],
        ]);
        //بررسی صحت درخواست
        if ($request->token != session()->get('withdraw_' . $request->unit)) {
            return response()->json(['status' => 500, 'msg' => 'Invalid Request']);
        }
        // اگر امنیت برداشت روی پیامک بود
        if ($user->confirm_type == 'sms') {
            // بررسی صحت کد تایید
            if ($user->time_verification > Carbon::now()) {
                if ($user->verification == $request->code) {
                    $user->time_verification = Carbon::now();
                    $user->save();
                    return $this->withdraw_store($request);
                } else {
                    return response()->json(['status' => 500, 'msg' => 'Wrong Code']);
                }
            } else {
                return response()->json(['status' => 500, 'msg' => 'Expire Code']);
            }
        } else {
            $authenticator = app(Authenticator::class)->bootStateless($request);
            return $this->withdraw_store($request);
        }
    }

    private function withdraw_store($request)
    {
        // کارمزد ارز مشخص شده
        $user = User::select(['id','email','code',
            'fee' => BaseData::select(DB::raw("cast(extra_field1 as UNSIGNED)"))->where('type', 'fee_withdraw_' . $request->unit)->limit(1),
            'transact_type' => BaseData::select('id')->where('type', 'transactions')->where('extra_field1', 3)->limit(1),
            'min_withdraw' => BaseData::select(DB::raw("cast(extra_field1 as UNSIGNED)"))->where('type', 'min_withdraw_' . $request->unit)->limit(1),
            'asset_amount' => Asset::select('amount')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'asset_unit' => Asset::select('unit')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'asset_address' => Asset::select('token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'type_token' => Asset::select('type_token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
        ])->where('id', auth()->user()->id)->first();
        // استعلام موجودی کیف پول ادمین برای کارمزد
        $asset_admin_fee = \Tron()->getBalance(env('ADDRESS_TRON_BASE')) / 1000000;
        if ($asset_admin_fee < 4) {
            Mail::to(env('EMAIL_SUPPORT_CTR'))->send(new sendMessageAdmin('TRX', "TRX currency balance is finished, please buy to increase the current balance with the current balance {$asset_admin_fee}"));
        } elseif ($asset_admin_fee < 15) {
            Mail::to(env('EMAIL_SUPPORT_CTR'))->send(new sendMessageAdmin('TRX', "TRX currency balance is running low, please buy to increase the current balance with the current balance {$asset_admin_fee}"));
            return response()->json(['status' => 500, 'msg' => 'Currency conversion is not available at the moment. Please wait another 30 minutes']);
        }
        // استعلام موجودی کیف پول ادمین برای ارز درخواست شده
        $asset_admin_unit = $this->getAmountsAdmin(env('ADDRESS_TRON_BASE'), $user->type_token);
        if (array_key_exists($request->unit, $asset_admin_unit) && ($asset_admin_unit[$request->unit] / 1000000) < $request->amount) {
            $amount_admin_unit = $asset_admin_unit[$request->unit] / 1000000;
            Mail::to(env('EMAIL_SUPPORT_CTR'))->send(new sendMessageAdmin($user->asset_unit, "Your balance has run out. Please increase your current balance {$amount_admin_unit} Amount requested {$request->amount} Request user email {$user->email} {$user->code} Applicant user code"));
            return response()->json(['status' => 500, 'msg' => 'It is not possible to withdraw at the moment. Please wait another 30 minutes']);
        }
        // زمانی ک کاربر کیف پول نساخته بود
        if (!$user->asset_address) {
            return response()->json(['status' => 300, 'msg' => 'Dear user, your wallet has not been made. Please make a wallet. Thank you']);
        }
        //  حداقل برداشت
        if ($user->min_withdraw > $request->amount) {
            return response()->json(['status' => 300, 'msg' => "The minimum withdrawal is " . $user->min_withdraw]);
        }
        // اگر موجودی کاربر کمتر از مبلغ درخواستی بود
        if ($user->asset_amount < $request->amount) {
            return response()->json(['status' => 300, 'msg' => 'Your inventory is low']);
        }
        $amount = $request->amount - $user->fee;
        $hex = Tron()->toHex($request->address);
        $param = convertToParameter($hex, $amount * 1000000);
        /// اگر آدرس اشتباه بود
        if ($param == '') {
            return response()->json(['status' => 500, 'msg' => 'Destination wallet address is incorrect']);
        }
        (!empty(session()->get('withdraw_' . $request->unit))) ? session()->remove('withdraw_' . $request->unit) : '';
        switch ($user->type_token) {
            case 10;
                DB::beginTransaction();
                try {
                    $transfer = Tron()->sendToken($request->address, $amount* 1000000, contractUnit($user->asset_unit)['contract'],env('ADDRESS_TRON_HEX'));
                    $asset = Asset::where('user_id', $user->id)->where('unit', $user->asset_unit)->first();
                    // کاهش موجودی ولت کاربر
                    $asset->amount -= $request->amount;
                    $asset->save();
                    // لاگ تراکنش
                    $asset->transaction()->create([
                        'tracking_code' => $transfer['txID'],
                        'user_id' => $user->id,
                        'transact_type' => $user->transact_type,
                        'amount' => $amount,
                        'type' => 1,
                        'extra_field1' => $request->address,
                        'description' => " Withdrawal of {$user->asset_unit} currency in the amount of {$amount}",
                    ]);
                    DB::commit();
                    return response()->json(['status' => 100, 'msg' => 'The transfer operation was successful']);
                } catch (\Exception $e) {
                    DB::rollBack();
                    return response()->json(['status' => 500, 'msg' => 'Transfer failed. Please contact system management']);
                }
                break;
            case 20;
                DB::beginTransaction();
                try {
                    $transfer = Tron()->triggerSmartContractWithdraw($param, contractUnit($user->asset_unit)['contract'], env('ADDRESS_TRON_HEX'));

                    $asset = Asset::where('user_id', $user->id)->where('unit', $user->asset_unit)->first();
                    // کاهش موجودی ولت کاربر
                    $asset->amount -= $request->amount;
                    $asset->save();
                    // لاگ تراکنش
                    $asset->transaction()->create([
                        'tracking_code' => $transfer['txID'],
                        'user_id' => $user->id,
                        'transact_type' => $user->transact_type,
                        'amount' => $amount,
                        'type' => 1,
                        'extra_field1' => $request->address,
                        'description' => " Withdrawal of {$user->asset_unit} currency in the amount of {$amount}",
                    ]);
                    DB::commit();
                    return response()->json(['status' => 100, 'msg' => 'The transfer operation was successful']);
                } catch (\Exception $e) {
                    DB::rollBack();
                    return response()->json(['status' => 500, 'msg' => 'Transfer failed. Please contact system management']);
                }
                break;
        }
    }

    // امنیت برداشت ارز
    public function security_withdraw(Request $request)
    {
        $request->validate([
            'unit' => ['required', new Unit()],
            'amount' => 'required|numeric|between:0,999999.999999',
            'address' => 'required'
        ]);
        // کارمزد ارز مشخص شده
        $user = User::select(['id',
            'fee' => BaseData::select(DB::raw("cast(extra_field1 as UNSIGNED)"))->where('type', 'fee_withdraw_' . $request->unit)->limit(1),
            'transact_type' => BaseData::select('id')->where('type', 'transactions')->where('extra_field1', 3)->limit(1),
            'min_withdraw' => BaseData::select(DB::raw("cast(extra_field1 as UNSIGNED)"))->where('type', 'min_withdraw_' . $request->unit)->limit(1),
            'asset_amount' => Asset::select('amount')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'asset_unit' => Asset::select('unit')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'asset_address' => Asset::select('token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'type_token' => Asset::select('type_token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
        ])->where('id', auth()->user()->id)->first();
        // استعلام موجودی کیف پول ادمین
        $asset_admin_fee = \Tron()->getBalance(env('ADDRESS_TRON_BASE')) / 1000000;
        if ($asset_admin_fee < 4) {
            Mail::to(env('EMAIL_SUPPORT_CTR'))->send(new sendMessageAdmin('TRX', "TRX currency balance is finished. Please increase your balance. Thanks for the current balance {$asset_admin_fee}"));
        } elseif ($asset_admin_fee < 15) {
            Mail::to(env('EMAIL_SUPPORT_CTR'))->send(new sendMessageAdmin('TRX', "TRX currency balance is running low. Please increase your balance. Thanks for the current balance {$asset_admin_fee}"));
            return response()->json(['status' => 500, 'msg' => 'Currency conversion is not available at the moment. Please wait another 30 minutes']);
        }
        // استعلام موجودی کیف پول ادمین برای ارز درخواست شده
        $asset_admin_unit = $this->getAmountsAdmin(env('ADDRESS_TRON_BASE'), $user->type_token);
        if (array_key_exists($request->unit, $asset_admin_unit) && ($asset_admin_unit[$request->unit] / 1000000) < $request->amount) {
            $amount_admin_unit = $asset_admin_unit[$request->unit] / 1000000;
            Mail::to(env('EMAIL_SUPPORT_CTR'))->send(new sendMessageAdmin($user->asset_unit, "Your balance has run out. Please increase your current balance{$amount_admin_unit}. Amount requested {$request->amount}"));
            return response()->json(['status' => 500, 'msg' => 'It is not possible to withdraw at the moment. Please wait another 30 minutes']);
        }
        // زمانی ک کاربر کیف پول نساخته بود
        if (!$user->asset_address) {
            return response()->json(['status' => 300, 'msg' => 'Dear user, your wallet has not been made. Please make a wallet. Thank you']);
        }
        //  حداقل برداشت
        if ($user->min_withdraw > $request->amount) {
            return response()->json(['status' => 300, 'msg' => "The minimum withdrawal is " . $user->min_withdraw]);
        }
        // اگر موجودی کاربر کمتر از مبلغ درخواستی بود
        if ($user->asset_amount < $request->amount) {
            return response()->json(['status' => 300, 'msg' => 'Your inventory is low']);
        }
        $token = Str::random(100);
        // درصورت موجود بودن توکن حذف شود و توکن جدید ایجاد شود
        session()->get('withdraw_' . $request->unit) ? session()->remove('withdraw_' . $request->unit) : '';
        session()->put('withdraw_' . $request->unit, $token);
        $user = auth()->user();
        // بررسی نوع امنیت برداشت
        $mode = $user->confirm_type;
        if ($mode == 'sms') {
            return view('user.wallet.security_withdraw.sms', compact('token', 'user'));
        } else {
            return view('user.assets.security_withdraw.google_authenticator', compact('token', 'user'));
        }
    }

    public function security_send_sms(Request $request)
    {
        $request['country'] = 'IR';
        $request->validate([
            'number' => 'required|numeric',
            'country' => 'required',
        ]);
        $user = auth()->user();
//        if ($user->time_verification >= Carbon::now()) {
//            $seconds = Carbon::parse($user->time_verification)->diffInSeconds(Carbon::now());
//            return response()->json(['status' => 300, 'msg' => 'کد یک بار برای شما ارسال شده', 'time' => $seconds]);
//        }
        $country = Country::where('code', $request->country)->first();
        $number = '+' . $country->phonecode . $request->number;
        $code = rand(10000, 99999);
        try {
            $user = auth()->user();
            $user->mobile = $request->number;
            $user->verification = $code;
            $user->time_verification = Carbon::now()->addMinutes(1);
            $user->save();
            smsVerify($code, $number, 'verify');

            return response()->json(['status' => 100, 'msg' => 'The code was sent for the desired mobile number', 'time' => 60]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'Error sending']);
        }
    }

    // ویو خریدن ctr
    public function buy_ctr_view()
    {
        $usdt = auth()->user()->assets()->usdt()->first(['amount']);
        $base_data = BaseData::query()
            ->where('type', 'currency_price')
            ->where('name', 'ctr')
            ->first(['extra_field1']);
        return view('user.assets.buy_ctr', compact('usdt', 'base_data'));
    }

    // خریدن ctr
    public function buy_ctr_store(Request $request)
    {
        $request->validate([
            'amount' => 'required|min:1',
        ]);
        try {
            $usdt = auth()->user()->assets()->usdt()->first(['amount']);
            $base_data = BaseData::query()
                ->where('type', 'currency_price')
                ->where('name', 'ctr')
                ->first(['extra_field1']);
            $ctr_to_usdt = $base_data->extra_field1 * $request->amount;
            if ($ctr_to_usdt > $usdt->amount) {
                return response()->json(['status' => 500, 'msg' => 'not enough inventory.']);
            }

            // اضافه کرده به جدول Finance Transaction
            $transactions9 = BaseData::query()->where('type', 'transactions')->where('extra_field1', 9)->first(['id']);
            $usdt_asset = tap(auth()->user()->assets()->usdt())->update(['amount' => intval($usdt->amount) - intval($ctr_to_usdt)])->first();
            $ctr_asset = tap(auth()->user()->assets()->ctr())->update(['amount' => DB::raw("amount + {$request->amount}")])->first();

            $date = date('Y-m-d H:i:s');
            $description = "Buy {$request->amount} Ctr";
            Finance_transaction::query()
                ->insert([
                    [
                        'financeable_id' => $usdt_asset->id,
                        'financeable_type' => 'App\Models\Asset',
                        'user_id' => auth()->user()->id, // ایدی کاربر
                        'type' => 1, // کاهش
                        'transact_type' => $transactions9->id, // ایدی از بیس دیتا
                        'amount' => $ctr_to_usdt, // مقدار اضافه شدن
                        'description' => $description,
                        'created_at' => $date,
                        'updated_at' => $date
                    ],
                    [
                        'financeable_id' => $ctr_asset->id,
                        'financeable_type' => 'App\Models\Asset',
                        'user_id' => auth()->user()->id, // ایدی کاربر
                        'type' => 2, // افزایش
                        'transact_type' => $transactions9->id, // ایدی از بیس دیتا
                        'amount' => $request->amount, // مقدار اضافه شدن
                        'description' => $description,
                        'created_at' => $date,
                        'updated_at' => $date
                    ]
                ]);

            // اضافه کردن سود به بالادستی
            $des = "Profit Buy {$request->amount} CTR by subset";
            InviterProfit::dispatch(auth()->user(), $ctr_to_usdt, null, $des,'buy-ctr');

            return response()->json(['status' => 100, 'msg' => 'Buy successfully']);

        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'Problem. pls contact with manager']);
        }
    }

    // قیمت ارز های بر اساس usdt
    public function price_on_usdt($asset_unit)
    {
        $price_currency = GlobalMarketApi::query()
            ->where('type', 'price_currency')
            ->first(['value']);

        if (!$price_currency)
            return false;
        $data = json_decode($price_currency->value, true);
        $binance = $data['global']['binance'];

        if (is_array($asset_unit)) {
            $converted = [];
            foreach ($asset_unit as $item)
                if (isset($binance["$item"]))
                    $converted["$item"] = $binance["$item"];
            return $converted;
        } else
            if (isset($binance["$asset_unit"]))
                return $binance["$asset_unit"];
        return false;
    }

    // تبدیل ارزهای کاربر به usdt
    public function convert_to_usdt(Request $request, $asset_unit)
    {
        if (!in_array($asset_unit,['btt'])) {
            return response()->json(['status' => 500, 'msg' => "this is wrong!"]);
        }
        $request->validate([
            'amount' => 'required|numeric|min:0',
        ]);
        try {
            $user_asset = auth()->user()
                ->assets()
                ->where('unit', $asset_unit)
                ->first();
            if ($user_asset->amount < $request->amount)
                return response()->json(['status' => 500, 'msg' => "max $asset_unit: {$user_asset->amount}"]);
            $on_usdt = $this->price_on_usdt($asset_unit);
            $usdt_amount = $request->amount * $on_usdt;

            if ($usdt_amount <= 0)
                return response()->json(['status' => 500, 'msg' => "amount is not valid"]);

            $user_asset->amount = $user_asset->amount - $request->amount;
            $user_asset->save();

            $usdt_asset= auth()->user()
                ->assets()
                ->usdt()
                ->first(['id','amount']);
            $usdt_asset->amount=  $usdt_asset->amount + $usdt_amount;
            $usdt_asset->save();

            $swap_base_data= BaseData::query()
                ->where('type','transactions')
                ->where('extra_field1','14')
                ->first(['id'])->id;
            $des= "Convert {$request->amount}{$asset_unit} to {$usdt_amount}usdt";
            $date = date('Y-m-d H:i:s');
            Finance_transaction::insert([
                [
                    'financeable_id' => $user_asset->id,
                    'financeable_type' => 'App\Models\Asset',
                    'user_id' => auth()->user()->id, // کاربر
                    'type' => 1, // کاهش
                    'transact_type' => $swap_base_data, // ایدی از بیس دیتا
                    'amount' => $request->amount, // مقدار ارز کم شده
                    'description' => $des,
                    'created_at' => $date,
                    'updated_at' => $date
                ],
                [
                    'financeable_id' => $usdt_asset->id,
                    'financeable_type' => 'App\Models\Asset',
                    'user_id' => auth()->user()->id, // کاربر
                    'type' => 2, // افزایش
                    'transact_type' => $swap_base_data, // ایدی از بیس دیتا
                    'amount' => $usdt_amount, // مقدار ارز اضافه شده شده
                    'description' => $des,
                    'created_at' => $date,
                    'updated_at' => $date
                ],
            ]);

            return response()->json(['status' => 200, 'msg' => "increase $usdt_amount usdt your usdt wallet ", 'refresh' => true]);
        }catch (\Exception $exception) {
            dd($exception);
            return response()->json(['status' => 500, 'msg' => "pls Contact to admin"]);
        }


    }
}
