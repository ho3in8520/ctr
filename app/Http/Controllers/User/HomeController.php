<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\BaseData;
use App\Models\Invest_Shares;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboard()
    {
        $result=[];
        $result['count_notification']=countNotificationNotView();
        $invests= auth()->user()->invest()->active()->get();
        $share= auth()->user()->invest()->active()->first();
        $cancel_invest = parameter_cancel_invest();
        return view('user.dashboard',compact('result','invests','cancel_invest','share'));
    }

    public function home()
    {
        return view('home');
    }
}
