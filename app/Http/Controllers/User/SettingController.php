<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    // تنظیمات عمومی کاربر
    public function general(Request $request)
    {
        $user = auth()->user();
        $google_2fa = $user->google2fa_secret == false ? create_google2fa() : false;
        return view('user.settings', compact('user', 'google_2fa'));
    }

    // فعال کردن و ست کردن اتنتیکاتور
    public function register_authenticator(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'secret' => 'required',
            'one_time_password' => 'required',
        ]);
        $user = \auth()->user();
        if ($user->google2fa_secret == false) {
            $user->google2fa_secret = $request->input('secret');
            if (getCurrentOtp($request->one_time_password) === true) {
                $user->save();
                return response()->json(['status' => 100, 'msg' => 'The authenticator was built successfully', 'type' => 'success']);
            } else {
                return response()->json(['status' => 500, 'msg' => 'Authenticator code was was wrong!', 'type' => 'error']);
            }
        } else {
            return response()->json(['status' => 500, 'msg' => 'Your authenticator is already built', 'type' => 'error']);
        }
    }

    // غیرفعال کردن اتنتیکاتور
    public function deactivate_authenticator(Request $request)
    {
        $request->validate([
            'one_time_password' => 'required'
        ]);
        $user = \auth()->user();
        if ($user->google2fa_secret) {
            if (getCurrentOtp($request->one_time_password) === true) {
                $user->google2fa_secret = null;
                $user->save();
                return response()->json(['status' => 100, 'msg' => 'The authenticator was deactivate successfully', 'type' => 'success']);
            } else {
                return response()->json(['status' => 500, 'msg' => 'Authenticator code was was wrong!', 'type' => 'error']);
            }
        } else {
            return response()->json(['status' => 500, 'msg' => 'Your authenticator in not set', 'type' => 'error']);
        }
    }

    // فعال یا غیرفعال کردن ورود دومرحله ای
    public function login_2fa(Request $request)
    {
        $request->validate([
            'one_time_password' => 'required',
            'type' => 'required|in:active,deactivate'
        ]);
        try {
            if (!auth()->user()->google2fa_secret)
                return response()->json(['status' => 500, 'msg' => "Google authenticator not active yet"]);
            if (getCurrentOtp($request->one_time_password) === false)
                return response()->json(['status' => 500, 'msg' => "Authenticator code was was wrong!"]);

            auth()->user()->login_2fa = $request->type == 'active' ? 1 : 0;
            auth()->user()->save();

            return response()->json(['status' => 100, 'msg' => "Two Factor Authentication was {$request->type}!"]);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => "please contact with admin"]);
        }

    }
}
