<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PragmaRX\Google2FA\Google2FA;

class LoginController extends Controller
{
    private $guard = 'web';

    public function loginForm()
    {
        return view('user.auth.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'g-recaptcha-response' => 'recaptcha',
        ]);
        return redirect()->back()->with('flash', ['type' => 'danger', 'msg' => 'You Can not login at this time!']);
        $user = User::where('email', $request->email)->first();
        return $this->checkAndLogin($request, $user);
    }

    public function only(Request $request)
    {
        return $request->only(['email', 'password']);
    }

    public function attempt(Request $request, $remember)
    {
        return Auth::guard($this->guard)->attempt($this->only($request), $remember);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login.form');
    }

    /*
     * بررسی کردن اطلاعات ورود کاربر
     */
    public function checkAndLogin(Request $request, $user)
    {
        if (!$user) {
            return redirect()->back()->withErrors(['not_match' => 'Email and password dose not match!']);
        }
        if ($user->email != $request->email) {
            return redirect()->back()->withErrors(['not_match' => 'Email and password dose not match!']);
        } elseif (!Hash::check($request->password, $user->password)) {
            return redirect()->back()->withErrors(['not_match' => 'Email and password dose not match!']);
        }
        if ($user && $user->status == 2) {// blocked account
            $msg = "Your account has been Blocked \n";
            $msg .= "reason: " . $user->reject_reason;
            return redirect()->back()->with('flash', ['type' => 'danger', 'msg' => $msg]);
        }
        if (!$user->email_verified_at) {// account not active
            return redirect()->back()->with('flash', ['type' => 'danger', 'msg' => 'Your account is not active!']);
        }

        return $this->stepLogin($request, $user);
    }

    private function stepLogin(Request $request, $user)
    {
        $remember = $request->has('remember');
        if ($user->login_2fa) {
            session()->forget('step_login');
            session()->push('step_login', $request->email . '|' . $remember);
            return redirect()->route('login.step.form');
        } else {
            if ($this->attempt($request, $remember))
                return redirect()->route('user.dashboard');
            return redirect()->back()->withErrors(['not_match' => 'Email and password dose not match!']);
        }
    }

    public function login_step_form()
    {
        if (!session()->has('step_login')) {
            abort(403);
        }
        return view('user.auth.google2fa');
    }

    public function login_step(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $remember = ($request->remember) ? true : false;
        if (Auth::loginUsingId($user->id, $remember)) {
            return redirect()->route('user.dashboard');
        }
        return redirect()->back()->withErrors(['code' => 'The code entered is incorrect']);
    }
}
