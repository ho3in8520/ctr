<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\EmailForgetPassword;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ForgetPasswordController extends Controller
{
    public function forget()
    {
        return view('user.auth.forget_password');
    }

    public function sendEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email'
        ]);
        $token = Str::random(50);
        $date_time = Carbon::now()->addMinute(30);
        $user = User::where('email', $request->email)->first();
        try {
            $user->verification = $token;
            $user->time_verification = $date_time;
            $user->save();
            EmailForgetPassword::dispatch($user, $token);
            return redirect()->route('login.form')->with('flash', ['type' => 'success', 'msg' => 'An email containing an activation link has been sent to you']);
        } catch (\Exception $e) {
            return redirect()->back()->with('flash', ['type' => 'danger', 'msg' => 'Error performing operations']);
        }
    }

    public function verifyToken($token)
    {
        $user = User::where('verification', $token)->firstOrFail();
        if ($user->time_verification >= Carbon::now()) {
            $user->email_verified_at = Carbon::now();
            return view('user.auth.new_password', compact('user'));
        } else {
            return redirect()->route('login.form')->with('flash', ['type' => 'danger', 'msg' => 'The link has expired']);
        }
    }

    public function newPassword(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'password' => 'required|min:8|confirmed'
        ]);
        $user = User::where('verification', $request->token)->first();
        if (!$user) {
            return redirect()->back()->with('flash', ['type' => 'danger', 'msg' => 'Invalid request']);
        }
        if ($user->time_verification >= Carbon::now()) {
            $user->verification = '';
            $user->time_verification = '';
            $user->password = Hash::make($request->password);
            if ($user->save())
                return redirect()->route('login.form')->with('flash', ['type' => 'success', 'msg' => 'Your password has been successfully changed']);
            return redirect()->back()->with('flash', ['type' => 'danger', 'msg' => 'Error performing operations']);
        } else {
            return redirect()->route('login.form')->with('flash', ['type' => 'danger', 'msg' => 'The link has expired']);
        }
    }

}
