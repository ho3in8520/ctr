<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\EmailNewRegister;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisterController extends Controller
{

    public function register()
    {
        return view('user.auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|confirmed',
            'ref_code' => 'nullable|numeric|exists:users,code',
            'terms' => 'required',
            'g-recaptcha-response' => 'recaptcha',
        ]);
        return redirect()->back()->with('flash', ['type' => 'danger', 'msg' => 'You Can not register at this time!']);
       $referral_code= $request->ref_code?$request->ref_code:0;
        try {
            $token = Str::random(50);
            $date_time = Carbon::now()->addMinute(30);
            $user = User::create([
                'email' => $request->email,
                'code' => rand(10000, 99999),
                'referral_code' => $referral_code,
                'verification' => $token,
                'time_verification' => $date_time,
                'password' => Hash::make($request->password)
            ]);
            EmailNewRegister::dispatch($user, $token);
            return redirect()->route('login.form')->with('flash', ['type' => 'success', 'msg' => 'An email containing an activation link has been sent to you']);
        } catch (\Exception $e) {
            return redirect()->back()->with('flash', ['type' => 'danger', 'msg' => 'Error performing operations']);
        }
    }

    public function verifyEmail($token)
    {
        $user = User::where('verification', $token)->firstOrFail();
        if ($user->time_verification >= Carbon::now()) {
            $user->email_verified_at = Carbon::now();
            $user->verification = '';
            $user->time_verification = '';
            if ($user->save())
                return redirect()->route('login.form')->with('flash', ['type' => 'success', 'msg' => 'Your account has been successfully activated']);
            return redirect()->route('login.form')->with('flash', ['type' => 'danger', 'msg' => 'More about this source text']);
        } else {
            return redirect()->route('login.form')->with('flash', ['type' => 'danger', 'msg' => 'The link has expired']);
        }
    }

    public function show_registration_form()
    {
        $ref=[];
        $code = request('code');
        if ($code) {
            $ref['code'] = base64_decode($code);
            $ref['email'] = User::select('email')->where('code',$ref['code'])->firstOrFail()->email;
        } else {
            $ref = '';
        }
        return view('user.auth.register', compact('ref'));
    }
}
