<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Finance_transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use function PHPUnit\Framework\isEmpty;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function withdraw_report_form(Request $request)
    {
        $user = auth()->user();
        $date_from = Carbon::parse($request['date_from'])->format('Y-m-d 00:00:00');
        $date_to = Carbon::parse($request['date_to'])->format('Y-m-d 23:59:59');

        if (!empty($request->type) && $request->type ==3)
            $transact_type=BaseData::query()->select('id')->where('type','transactions')->whereIn('extra_field1' ,[3,11])->get()->toArray();
        elseif (!empty($request->type) && $request->type ==4)
            $transact_type=BaseData::query()->select('id')->where('type','transactions')->whereIn('extra_field1' ,[4,10])->get('id')->toArray();
        $withdraws = Finance_transaction::query('financeable')->select([
            '*',
            'transact_type'=>BaseData::select('extra_field2')->wherecolumn('finance_transactions.transact_type','base_data.id')->limit(1),
            'extra_field1'=>BaseData::select('extra_field1')->wherecolumn('finance_transactions.transact_type','base_data.id')->limit(1)
        ])->where('user_id', $user->id);

        $request->unit ?
            $withdraws->whereHasMorph('financeable', [Asset::class], function ($query) use ($request) {
                $query->where('unit', $request->unit);
            }) : '';


        $request->date_from && $request->has('date_from') && !empty($request->date_from)? $withdraws->where('created_at', '>=', $date_from) : '';
        $request->date_to && $request->has('date_to') && !empty($request->date_to)? $withdraws->where('created_at', '<', $date_to) : '';

        $request->amount_from && $request->has('amount_from') && !empty($request->amount_from)? $withdraws->where('amount', '>=', $request->amount_from) : '';
        $request->amount_to && $request->has('amount_to') && !empty($request->amount_to)? $withdraws->where('amount', '<', $request->amount_to) : '';

        $request->destination_address && $request->has('destination_address') && !empty($request->destination_address)? $withdraws->where('extra_field1', '=', $request->destination_address) : '';
        $request->type && $request->has('type') && !empty($request->type)? $withdraws->whereIn('transact_type', $transact_type) : '';

        $withdraws->orderBy('created_at','desc');
        $withdraws = $withdraws->paginate(10);


        return showData(view('user.transactions.withdraw_report', compact('withdraws')));

    }

}
