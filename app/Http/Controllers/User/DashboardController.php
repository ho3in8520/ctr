<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profile()
    {
        $user = auth()->user();
        $user->referral_code ? $referral_email = User::where('code', $user->referral_code)->value('email') : $referral_email = '';

        return view('user.profile', compact('user', 'referral_email'));
    }

    public function update_profile(Request $request, $user)
    {
        $user = User::findOrFail($user);
        $request->validate([
            'referral_code' => ['exists:users,code', function ($attr, $val, $fail) use ($user) {
                if ($user->code == $val) {
                    $fail('You can not enter your code');
                }
            }, 'nullable']
        ]);
        $request->first_name && $request->has('first_name') && !empty($request->first_name) ? $user->first_name = $request->first_name : $user->first_name = '';
        $request->last_name && $request->has('last_name') && !empty($request->last_name) ? $user->last_name = $request->last_name : $user->last_name = '';
        $request->referral_code && $request->has('referral_code') && !empty($request->referral_code) ? $user->referral_code = $request->referral_code : $user->referral_code = 0;
        try {
            $user->update();
            return response()->json(['status' => 100, 'msg' => 'Your information updated successfully']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'Some thing was wrong! Please try later.']);
        }

    }

    public function change_pass(Request $request, $user)
    {
        $user = User::findOrFail($user);
        $request->validate([
            'current_password' => ['required', function ($attr, $val, $fail) {
                if (!Hash::check($val, auth()->user()->password)) {
                    $fail("The current password is incorrect");
                }
            }],
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
        ]);
        try {
            $user->update([
                'password' => Hash::make($request['password'])
            ]);
            auth()->guard('web')->logout();
            return response()->json(['status' => 100, 'msg' => 'Password changed successfully.']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'Something was wrong! Please try later.']);
        }
    }

}
