<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Finance_transaction;
use App\Models\Invest_Shares;
use Carbon\Traits\Date;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;


class Invest_SharesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = Route::currentRouteName() == 'user.invest.index' ? 'invest' : 'shares';
        $buy_type = $type == 'invest' ? 'plan' : 'update';
        $invest_shares = Invest_Shares::query()
            ->where('user_id', auth()->user()->id)
            ->where('invest_shares.type', $type)
            ->join('base_data', function ($join) {
                $join
                    ->on('invest_shares.type', '=', 'base_data.type')
                    ->where(function ($query) {
                        $query
                            ->where(function ($query) {
                                $query
                                    ->where('initial_value', '>=', DB::raw('base_data.extra_field1'))
                                    ->where('initial_value', '<', DB::raw('base_data.extra_field2'));
                            })
                            ->orWhere(function ($query) {
                                $query
                                    ->where('invest_shares.initial_value', '>=', DB::raw('base_data.extra_field1'))
                                    ->where('base_data.extra_field2', '-1');
                            });
                    });
            })
            ->get(['invest_shares.id as id', 'initial_value', 'invest_shares.status', 'profit', 'extra_field3 as percent', 'invest_shares.created_at', 'invest_shares.updated_at']);
        $shares_amount = $type == 'shares' ? auth()->user()->assets()->btt()->first()->amount : false;
        $cancel_invest = $type == 'invest' ? parameter_cancel_invest() : false;
        return view('user.invest_shares.index', compact('invest_shares', 'type', 'buy_type', 'cancel_invest', 'shares_amount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = Route::currentRouteName() == 'user.invest.create' ? 'invest' : 'shares';
        $profits = BaseData::query()->active();
        $type == 'invest' ? $profits->invest() : $profits->shares();
        $profits = $profits->orderBy('extra_field1')->get();
        $USDT_asset = auth()->user()
            ->assets()
            ->where('unit', 'usdt')
            ->first();
        return view('user.invest_shares.create', compact('profits', 'USDT_asset', 'type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = Route::currentRouteName() == 'user.invest.store' ? 'invest' : 'shares';

        $request->validate([
            'amount' => 'required|numeric'
        ]);

        try {
            $USDT_asset = auth()->user()
                ->assets()
                ->where('unit', 'usdt')
                ->where('amount', '>=', $request->amount)
                ->first(['amount']);

            // مقداری که کاربر وارد کرده بیشتر از موجودی کاربر باشه
            if (is_null($USDT_asset)) {
                return response()->json(['status' => 500, 'msg' => 'your Inventory not enough. amount']);
            }

            // مقداری که کاربر وارد کرده داخل بازه هایی که مدیریت تعیین کرده نباشه
            if ($percent = $this->check_out_of_range($request->amount, $type) == false) {
                return response()->json(['status' => 500, 'msg' => 'out of range']);
            }

            $buy_type = $type == 'invest' ? 'plan' : 'update'; // این رو بعدا از توی دیتابیس باید بخونیم و داینامیک باشه

            $invest_shares = null;
            if ($buy_type == 'update') {
                $invest_shares = Invest_Shares::query()
                    ->where('user_id', auth()->user()->id)
                    ->where('type', $type)
                    ->latest()
                    ->first();
            }
            if ($buy_type == 'plan' || ($buy_type == 'update' && $invest_shares == null)) {
                $invest_shares = Invest_Shares::query()
                    ->create([
                        'user_id' => auth()->user()->id,
                        'initial_value' => $request->amount,
                        'type' => $type,
                    ]);
                if ($type == 'shares')
                    Artisan::call('dividends:calc', ['id' => $invest_shares->id]);
            } else if ($buy_type == 'update' && $invest_shares) {
                $invest_shares->initial_value += $request->amount;
                $invest_shares->save();
            } else {
                return response()->json(['status' => 500, 'msg' => 'Wrong! pleas contact with admin']);
            }
            return response()->json(['status' => 100, 'msg' => 'Successfully added to your portfolio']);
        } catch (\Exception $exception) {
            dd($exception);
            return response()->json(['status' => 500, 'msg' => 'Wrong! pleas contact with admin']);
        }
    }

    /**
     * نشان دادن آخرین عملیات انجام گرفته روی این پلن
     *
     * @param \App\Models\Invest_Shares $share
     * @return \Illuminate\Http\Response
     */
    public function logs($share_id)
    {
        $type = Route::currentRouteName() == 'user.invest.logs' ? 'invest' : 'shares';
        $invest_shares = Invest_Shares::query()
            ->where('user_id', auth()->user()->id)
            ->findOrFail($share_id);
        $invest_shares_log = $invest_shares->transaction;
        return view('user.invest_shares.logs', compact('invest_shares', 'invest_shares_log', 'type'));
    }

    // قابلیت کنسل کردن invest
    public function cancel($invest_Shares_id)
    {
        $invest_shares = auth()->user()
            ->invest()
            ->where('id', $invest_Shares_id)
            ->active()
            ->firstOrFail(['id', 'status', 'initial_value']);
        try {
            $cancel_invest = parameter_cancel_invest();
            $percent = $cancel_invest['percent'];
            if ($invest_shares) {
                $invest_shares->status = 0;
                $invest_shares->save();
                $USDT_asset = auth()->user()
                    ->assets()
                    ->where('unit', 'usdt')
                    ->first(['id', 'amount']);

                $value = $invest_shares->initial_value * (100 - $percent) / 100;
                $USDT_asset->amount = $USDT_asset->amount + $value;
                $USDT_asset->save();
                $base_data = BaseData::query()
                    ->where('type', 'transactions')
                    ->where('extra_field1', 7)
                    ->first(['id']);

                $date = date('Y-m-d H:i:s');
                $des = 'Cancel Invest and return USDT to your asset.';
                $des .= intval($percent) > 0 ? " (Cancellation percentage: $percent)" : '';

                Finance_transaction::insert([
                    [
                        'financeable_id' => $invest_shares->id,
                        'financeable_type' => 'App\Models\Invest_Shares',
                        'refer_id' => null,
                        'user_id' => auth()->user()->id, // کاربر
                        'type' => 1, // کاهش
                        'transact_type' => $base_data->id, // ایدی از بیس دیتا
                        'amount' => $value, // مقدار usdt برگشت که کاربر کنسل کرده
                        'description' => $des,
                        'created_at' => $date,
                        'updated_at' => $date
                    ],
                    [
                        'financeable_id' => $USDT_asset->id,
                        'financeable_type' => 'App\Models\Asset',
                        'refer_id' => $invest_shares->id,
                        'user_id' => auth()->user()->id, // کاربر کاربر
                        'type' => 2, // افزایش
                        'transact_type' => $base_data->id, // ایدی از بیس دیتا
                        'amount' => $value, // مقدار usdt برگشت که کاربر کنسل کرده
                        'description' => $des,
                        'created_at' => $date,
                        'updated_at' => $date
                    ],
                ]);
                alert()->success('Success', 'Cancellation is success and increase your usdt inventory');
            }

            return redirect()->back();

        } catch (\Exception $exception) {
            dd($exception);
            alert()->error('Error', 'pls contact with admin');
            return redirect()->back();
        }
    }

    // قابلیت اضافه کردن به سهام ها در invest
    public function add_to_share($invest_Shares_id)
    {
        $invest_shares = auth()->user()
            ->invest()
            ->where('id', $invest_Shares_id)
            ->active()
            ->firstOrFail(['id', 'status', 'initial_value']);
        try {
            $cancel_invest = parameter_cancel_invest();
            $percent = $cancel_invest['percent'];
            if ($invest_shares) {
                $invest_shares->status = 0;
                $invest_shares->unsetEventDispatcher();
                $invest_shares->save();
                $last_share = auth()->user()
                    ->shares()
                    ->latest()
                    ->first(['id', 'initial_value']);
                $value = $invest_shares->initial_value * (100 - $percent) / 100;
                if (!$last_share) {
                    $last_share = new Invest_Shares(['type' => 'shares', 'initial_value' => 0, 'user_id' => auth()->user()->id]);
                }
                $last_share->initial_value = $last_share->initial_value + $value;
                $last_share->unsetEventDispatcher();
                $last_share->save();

                $base_data = BaseData::query()
                    ->where('type', 'transactions')
                    ->where('extra_field1', 7)
                    ->first(['id']);

                $date = date('Y-m-d H:i:s');
                $des = 'Add To Share Invest and return USDT to your last shares.';
                $des .= intval($percent) > 0 ? " (Cancellation percentage: $percent)" : '';

                Finance_transaction::insert([
                    [
                        'financeable_id' => $invest_shares->id,
                        'financeable_type' => 'App\Models\Invest_Shares',
                        'refer_id' => null,
                        'user_id' => auth()->user()->id, // کاربر کاربر
                        'type' => 1, // کاهش
                        'transact_type' => $base_data->id, // ایدی از بیس دیتا
                        'amount' => $value, // مقدار usdt کم شده به از سرمایه گذاری کاربر
                        'description' => $des,
                        'created_at' => $date,
                        'updated_at' => $date
                    ],
                    [
                        'financeable_id' => $last_share->id,
                        'financeable_type' => 'App\Models\Invest_Shares',
                        'refer_id' => $invest_shares->id,
                        'user_id' => auth()->user()->id, // کاربر کاربر
                        'type' => 2, // افزایش
                        'transact_type' => $base_data->id, // ایدی از بیس دیتا
                        'amount' => $value, // مقدار usdt اضافه شده به آخرین سهام کاربر
                        'description' => $des,
                        'created_at' => $date,
                        'updated_at' => $date
                    ],
                ]);
            }
            alert()->success('Success', 'Cancellation is success and increase your usdt inventory');
            return redirect()->back();

        } catch (\Exception $exception) {
            alert()->error('Error', 'pls contact with admin');
            return redirect()->back();
        }
    }

    // بررسی اینکه مقداری که کاربر وارد کرده صحیح باشه و داخل بازه ها باشه
    private function check_out_of_range($value, $type)
    {
        $profits = BaseData::query()->active();
        $type == 'invest' ? $profits->invest() : $profits->shares();
        $profits = $profits->orderBy('extra_field1')->get();
        foreach ($profits as $item) {
            $max = $item->extra_field2 == -1 ? $value+1 : $item->extra_field2;
            if ($value >= $item->extra_field1 && $value < $max)
                return $item->extra_field3;
        }
        return false;
    }

}
