<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\TicketMaster;
use App\Models\UploadedFile;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth('web')->user();
        $result = TicketMaster::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(10);
        return view('user.ticket.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.ticket.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'priority' => 'required|in:0,1,2',
            'description' => 'required|string',
            'attachment' => 'nullable|mimes:zip,rar,pdf,png,jpg,jpeg|max:2094',
            'g-recaptcha-response' => 'recaptcha',
        ]);
        try {
            $user = auth('web')->user();
            $master = TicketMaster::create([
                'user_id' => $user->id,
                'title' => $request->title,
                'priority' => $request->priority,
                'status' => 1,
            ]);

            $detail = $master->ticket_detail()->create([
                'user_id' => $user->id,
                'type' => 1,
                'description' => $request->description,
            ]);
            if ($request->has('attachment') && !empty($request->attachment)) {
                $detail->files()->create([
                    'user_id' => $user->id,
                    'mime_type' => $request->file('attachment')->getMimeType(),
                    'size' => $request->file('attachment')->getSize(),
                    'path' => upload_ticket($request->attachment),
                    'type' => 'ticket',
                ]);
            }
            alert('success', 'Operation successfully', 'success');
            return redirect()->route('ticket.index');
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'Error performing operations']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        $user = auth()->user();
        $result = TicketMaster::where('user_id', $user->id)->where('code', $code)->with(['ticket_detail' => function ($q) {
            $q->select([
                '*',
                'attachment' => UploadedFile::select('path')->whereColumn("ticket_details.id", 'uploaded_files.uploadable_id')
                    ->where('uploadable_type', 'App\Models\TicketDetail')->limit(1)
            ])->with(['user', 'admin'])->orderBy('id', 'desc');
        }])->firstOrFail();
        return view('user.ticket.show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code)
    {
        $request->validate([
            'description' => 'required|string',
            'attachment' => 'nullable|mimes:zip,rar,pdf,png,jpg,jpeg|max:2094',
            'g-recaptcha-response' => 'recaptcha',
        ]);
        try {
            $user = auth('web')->user();
            $master = TicketMaster::where('code', $code)->firstOrFail();
            $detail = $master->ticket_detail()->create([
                'user_id' => $user->id,
                'type' => 1,
                'description' => $request->description,
            ]);
            $master->status=1;
            $master->save();
            if ($request->has('attachment') && !empty($request->attachment)) {
                $detail->files()->create([
                    'user_id' => $user->id,
                    'mime_type' => $request->file('attachment')->getMimeType(),
                    'size' => $request->file('attachment')->getSize(),
                    'path' => upload_ticket($request->attachment),
                    'type' => 'ticket',
                ]);
            }
            alert('success', 'Operation successfully', 'success');
            return redirect()->back();
        } catch (\Exception $e) {
            alert('error', 'Error performing operations', 'error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
