<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use PragmaRX\Google2FA\Google2FA;

class Google2FALogin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->validate([
            'code' => 'required|numeric'
        ]);
        $session = session('step_login');
        $ex = explode('|', $session[0]);
        $user = User::where('email', $ex[0])->firstOrFail();

        $google2fa = new Google2FA();
        $code = $google2fa->getCurrentOtp($user->google2fa_secret);
        if ($code == $request->code) {
            $request['email'] = $user->email;
            $request['remember'] = $ex[1];
            session()->remove('step_login');
            return $next($request);
        }
        return redirect()->back()->withErrors(['code' => 'The code entered is incorrect']);
    }
}
