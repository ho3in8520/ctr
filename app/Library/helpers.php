<?php

use App\Models\BaseData;
use Illuminate\Support\Facades\Date;
use PragmaRX\Google2FA\Google2FA;
use PragmaRX\Google2FALaravel\Support\Authenticator;
use Maatwebsite\Excel\Facades\Excel;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

function countNotificationNotView()
{
    $user = auth('web')->user();
    $notification = \App\Models\Notification::where('user_id', $user->id)
        ->where('started_at', '<=', \Carbon\Carbon::now())
        ->doesntHave('viewNotification')
        ->orWhere('user_id', 0)
        ->where('started_at', '<=', \Carbon\Carbon::now())
        ->doesntHave('viewNotification')
        ->count();
    return $notification;
}

function notificationNotView()
{
    $user = auth('web')->user();
    $notification = \App\Models\Notification::where('user_id', $user->id)
        ->where('started_at', '<=', \Carbon\Carbon::now())
        ->doesntHave('viewNotification')
        ->orWhere('user_id', 0)
        ->where('started_at', '<=', \Carbon\Carbon::now())
        ->doesntHave('viewNotification')
        ->limit(5)->orderBy('id', 'desc')->get();
    return $notification;
}

function Tron($private_key = '380d061edaab92ed6459a7f5a685daefc3895d4f1a2954cd407bdcd4deb5d493')
{
    $fullNode = new \IEXBase\TronAPI\Provider\HttpProvider('https://api.trongrid.io');
    $solidityNode = new \IEXBase\TronAPI\Provider\HttpProvider('https://api.trongrid.io');
    $eventServer = new \IEXBase\TronAPI\Provider\HttpProvider('https://api.trongrid.io');

    try {
        $tron = new \App\Library\TronCtr($fullNode, $solidityNode, $eventServer, null, null, $private_key);
    } catch (\IEXBase\TronAPI\Exception\TronException $e) {
        exit('خطا');
    }
    return $tron;
}

function convertToParameter($address, $amount)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "api.ctrproject.com/convert-data",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "address=" . $address . "&amount=" . $amount . "",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    curl_close($curl);
    if ($httpcode == 200) {
        return $response;
    }
    return false;
}

// بررسی وریفای google authenticator
function google2faVerify(\Illuminate\Http\Request $request)
{
    $authenticator = app(Authenticator::class)->boot($request);
    if ($authenticator->isAuthenticated()) {
        return true;
    }
    return $authenticator->makeRequestOneTimePasswordResponse();
}

// ایجاد google authenticator
function create_google2fa(): array
{
    // Initialise the 2FA class
    $google2fa = app('pragmarx.google2fa');

    // Add the secret key to the registration data
    $registration_data["google2fa_secret"] = $google2fa->generateSecretKey();

    // Save the registration data to the user session for just the next request
    request()->session()->flash('registration_data', $registration_data);

    // Generate the QR image. This is the image the user will scan with their app
    // to set up two factor authentication
    $QR_Image = $google2fa->getQRCodeInline(
        config('app.name'),
        auth()->user()->email,
        $registration_data['google2fa_secret']
    );

    return ['QR_Image' => $QR_Image, 'secret' => $registration_data['google2fa_secret']];
}

function index($data, $loop)
{
    return ($data->currentPage() - 1) * $data->perPage() + 1 + $loop->index;
}

function showData($view, array $array = [])
{
    if (request()->ajax()) {
        $temp = ["status" => "100", "FormatHtml" => $view->renderSections()['content']];
        $temp = array_merge($temp, $array);
        return json_encode($temp);
    } else
        return $view;
}

// تبدیل تتر به ctr
function tether_to_ctr($tether)
{
    $ctr_price = \App\Models\BaseData::query()
        ->active()
        ->where('type', 'currency_price')
        ->where('name', 'ctr')
        ->first(['extra_field1']);
    if (!is_null($ctr_price))
        return $ctr_price->extra_field1 * $tether;
    return 0;
}

// تبدیل تتر به btt
function tether_to_btt($tether)
{
    $price_currency = \App\Models\GlobalMarketApi::price_currency()
        ->first(['value']);
    if (!is_null($price_currency) && !is_null($price_currency->value))
        return $tether / $price_currency->BttToUsdt;
    return 0;
}

// برگرداندن پارمتر های مورد نیاز کنسل کردن اینوست : تاریخ، درصد خسارت
function parameter_cancel_invest()
{
    $base_data = BaseData::query()->whereIn('type', ['date_cancel_invest', 'percent_cancel_invest'])->get(['extra_field1', 'type']);
    $date = $base_data->where('type', 'date_cancel_invest')->first();
    $percent = $base_data->where('type', 'percent_cancel_invest')->first();

    $date = $date ? $date->extra_field1 : null;
    $percent = $percent ? $percent->extra_field1 : null;
    $now = Date::now()->format('Y-m-d');
    return [
        'date' => $now == $date ? true : ($date > $now ? $date : false),
        'percent' => $now != $date ? $percent : false
    ];
}

function contractUnit($unit)
{
    $array = [
        'usdt' => ['type_token' => 20, 'contract' => 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t'],
        'trx' => ['type_token' => 20, 'contract' => 'TNUC9Qb1rRpS5CbWLmNMxXBjyFoydXjWFR'],
        'ctr' => ['type_token' => 20, 'contract' => 'TGdnjZ5QFY71eDrywzw856FpVh7xMBsF4k'],
        'btt' => ['type_token' => 10, 'contract' => '1002000']
    ];
    return $array[$unit];
}

function exportExcel(array $data, $name = 'reporting.xlsx')
{
    return Excel::download(new \App\Exports\ExportExcel($data), $name);
}

function exportPdf($data, $name = 'reporting.pdf')
{
    $data = [
        'data' => $data
    ];
    $pdf = PDF::loadView('templates.pdf.index', $data);
    return $pdf->stream('reporting.pdf');
}

function getCurrentOtp($code)
{
    $google2fa = new Google2FA();
    $user = auth('web')->user();
    $otp = $google2fa->getCurrentOtp($user->google2fa_secret);
    if ($code == $otp) {
        return true;
    }
    return false;
}

function upload_ticket($file)
{
    $guard = 'web';
    if (\Illuminate\Support\Facades\Auth::guard('admin')->check()) {
        $guard = 'admin';
    }
    $path = \Illuminate\Support\Facades\Storage::disk('ticket')->put($guard . '/' . auth($guard)->user()->id, $file);
    return $path;
}

function get_file_ticket($path)
{
    $ex = explode('/', $path);
    return route("file.ticket.view", ['guard' => $ex[0], 'id' => $ex[1], 'file' => $ex[2]]);
}

function countTicketNotAnswer()
{
    $model = \App\Models\TicketMaster::where('status', 1)->count();
    return $model;
}
