<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UploadedFile extends Model
{
    use HasFactory;

    protected $fillable=[
        'path',
        'user_id',
        'type',
    ];

    public function uploadable(){
        return $this->morphTo(__FUNCTION__,"uploadable_type","uploadable_id");
    }
}
