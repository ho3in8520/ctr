<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $fillable=[
        'user_id',
        'title',
        'description',
        'color',
        'icon',
        'started_at',
        'view',
    ];
    public function viewNotification()
    {
        return $this->hasOne(NotificationView::class,'notification_id');
    }
}
