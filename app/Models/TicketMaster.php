<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketMaster extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'code',
        'title',
        'priority',
        'status',
        'accept_id',
    ];

    public function ticket_detail()
    {
        return $this->hasMany(TicketDetail::class, 'master_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function status($lan = 'en')
    {
        switch ($this->status) {
            case 1;
                $status = ($lan == 'en') ? 'pending' : 'در انتظار پاسخ';
                break;
            case 2;
                $status = ($lan == 'en') ? 'answered' : 'پاسخ داده شده';
                break;
            case 3;
                $status = ($lan == 'en') ? 'closed' : 'بسته شده';
                break;
        }
        return $status;
    }
}
