<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'first_name',
        'last_name',
        'email',
        'email_verified_at',
        'password',
        'google2fa_secret',
        'login_2fa',
        'referral_code',
        'status',
        'reject_reason',
        'verification',
        'time_verification',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'google2fa_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // رابطه با جدول assets
    public function assets()
    {
        return $this->hasMany(Asset::class);
    }

    // سهام های کاربر
    public function shares()
    {
        return $this->hasMany(Invest_Shares::class)->where('type','shares');
    }

    // invest های کاربر
    public function invest()
    {
        return $this->hasMany(Invest_Shares::class)->where('type','invest');
    }

    // سهام ها و invest های کاربر
    public function invest_shares()
    {
        return $this->hasMany(Invest_Shares::class);
    }

    public function referral()
    {
        return $this->belongsTo(User::class,'referral_code','code');
    }

    // گرفتن نام کامل کاربر ( نام + نام خانوادگی)
    public function getNameAttribute()
    {
        return (!empty($this->first_name) || !empty($this->last_name)) ?
            $this->first_name . ' ' . $this->last_name : 'بدون نام';
    }

    // گرفتن آواتار کاربر
    public function getAvatarAttribute()
    {
//        $avatar = $this->files()->where('type', 'profile_pic')->first(['path']);
//        if ($avatar)
//            return getImage($avatar->path);
        return asset('general/img/no_avatar.png');
    }

    public function setGoogle2faSecretAttribute($value)
    {
        $this->attributes['google2fa_secret'] = encrypt($value);
    }

//    public function getGoogle2faSecretAttribute($value): bool
//    {
//        return is_null($value) || empty($value) ? false : decrypt($value);
//    }
    public function getGoogle2faSecretAttribute($value)
    {
        return (!empty($value)) ? decrypt($value) : null;
    }

}
