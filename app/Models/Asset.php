<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory;
    protected $hidden = ['token'];

    protected $guarded;

    public function transaction()
    {
        return $this->morphMany(Finance_transaction::class, 'financeable');
    }

    public function scopeUsdt($q)
    {
        return $q->where('unit','usdt');
    }
    public function scopeCtr($q)
    {
        return $q->where('unit','ctr');
    }
    public function scopeBtt($q)
    {
        return $q->where('unit','btt');
    }
    public function getAddressHexAttribute()
    {
        $address = $this->attributes['token'];
        if (empty($address))
            return '';
        $address = json_decode($address);

        return $address->address_hex;
    }
    public function getAddressAttribute()
    {
        $address = $this->attributes['token'];
        if (empty($address))
            return '';
        $address = json_decode($address);

        return $address->address_base58;
    }
}
