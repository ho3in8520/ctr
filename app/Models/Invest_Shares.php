<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invest_Shares extends Model
{
    protected $table= 'invest_shares';
    protected $guarded= [];
    protected $fillable= [
        'user_id',
        'initial_value',
        'profit',
        'type',
        'status',
    ];
    use HasFactory;

    public function transaction()
    {
        return $this->morphMany(Finance_transaction::class, 'financeable');
    }

    public function scopeShares($query)
    {
        return $query->where('type','shares');
    }

    public function scopeInvest($query)
    {
        return $query->where('type','invest');
    }

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }
}
