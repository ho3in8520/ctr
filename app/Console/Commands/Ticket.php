<?php

namespace App\Console\Commands;

use App\Models\TicketDetail;
use App\Models\TicketMaster;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class Ticket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticket:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ticket close';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $array = [];
        $model = TicketMaster::select([
            'id',
            'date' => TicketDetail::select('created_at')->whereColumn('ticket_masters.id', 'ticket_details.master_id')
                ->orderBy('id', 'desc')->limit(1)
        ])->where('status', '!=', 3)->get();
        if (count($model) > 0) {
            foreach ($model as $item) {
                if (Carbon::now()->diffInDays($item->date) > 10) {
                    $array[] = $item->id;
                }
            }
        }
        TicketMaster::whereIn('id', $array)->update(['status' => 3]);
        $this->comment('success');
    }
}
