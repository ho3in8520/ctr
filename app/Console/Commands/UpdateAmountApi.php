<?php

namespace App\Console\Commands;

use App\Models\GlobalMarketApi;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class UpdateAmountApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:amount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $result = Http::get('https://api.nobitex.ir/market/stats', ['srcCurrency' => 'btc,eth,ltc,usdt,xrp,bch,bnb,eos,xlm,etc,trx,doge,pmn', 'dstCurrency' => 'rls'])->body();
        if (json_decode($result)->status == 'ok') {
            GlobalMarketApi::where('type', 'price_currency')->update(['value' => $result]);
        }
        return 0;
    }
}
