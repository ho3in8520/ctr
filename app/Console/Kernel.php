<?php

namespace App\Console;

use App\Console\Commands\CurrencyTransferAdmin;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CurrencyTransferAdmin::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('update:amount')->everyMinute();
        $schedule->command('transfer:start')->everyMinute();
        $schedule->command('dividends:calc')->daily();
        $schedule->command('queue:work --stop-when-empty')->everyMinute();
        $schedule->command('ticket:close')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
