<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_data', function (Blueprint $table) {
            $table->id();
            $table->string('type', 255);
            $table->string('name', 255)->collation("utf8_general_ci");
            $table->string('extra_field1', 2000)->collation("utf8_general_ci")->nullable();
            $table->string('extra_field2', 2000)->collation("utf8_general_ci")->nullable();
            $table->string('extra_field3', 2000)->collation("utf8_general_ci")->nullable();
            $table->boolean('status')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });

        $array = [
            ['type' => 'message_notification', 'name' => 'new_register_user', 'extra_field1' => 'welcome', 'extra_field2' => 'message', 'extra_field3' => json_encode(["%first_name%", "%last_name%", "%email%"])],
            ['type' => 'message_email', 'name' => 'new_register_user', 'extra_field1' => 'welcome', 'extra_field2' => 'message', 'extra_field3' => json_encode(["%first_name%", "%last_name%", "%email%"])]
        ];
        \App\Models\BaseData::insert($array);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_data');
    }
}
