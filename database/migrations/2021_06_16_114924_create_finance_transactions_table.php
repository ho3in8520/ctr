<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanceTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finance_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("financeable_id");
            $table->string("financeable_type");
            $table->string("tracking_code")->nullable();
            $table->unsignedBigInteger("refer_id")->nullable();
            $table->unsignedBigInteger("user_id");
            $table->foreign("user_id")->references("id")->on("users");
            $table->unsignedBigInteger("transact_type");
            $table->foreign("transact_type")->references("id")->on("base_data");
            $table->float("amount", 20, 2)->default(0);
            $table->tinyInteger('type')->default(1)->comment("1=>Decrease 2=>Increase");
            $table->string('extra_field1', 500)->collation("utf8_general_ci")->nullable();
            $table->text("description")->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        $array = [
            ['type' => 'transactions', 'name' => 'swap', 'extra_field1' => '2', 'extra_field2' => 'swap', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'withdraw', 'extra_field1' => '3', 'extra_field2' => 'withdraw', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'deposite', 'extra_field1' => '4', 'extra_field2' => 'deposite', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'سود ctr حاصل از invest (usdt->ctr) کاربر', 'extra_field1' => '5', 'extra_field2' => 'daily profit invest (usdt to ctr)', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'سود btt حاصل ازshares (usdt->btt) کاربر', 'extra_field1' => '6', 'extra_field2' => 'daily profit shares (usdt to btt)', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'مقدار برگشتی از کنسل کردن invest', 'extra_field1' => '7', 'extra_field2' => 'return amount of invest cansellation', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'سود حاصل از خرید فرد زیر مجموعه', 'extra_field1' => '8', 'extra_field2' => 'profit buy of subset', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'خرید ctr', 'extra_field1' => '9', 'extra_field2' => 'buy ctr', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'شارژ کیف پول کاربر از طریق مدیریت', 'extra_field1' => '10', 'extra_field2' => 'increase wallet amount by admin', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'کاهش کیف پول کاربر از طریق مدیریت', 'extra_field1' => '11', 'extra_field2' => 'decrease wallet amount by admin', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'خرید کردن invest', 'extra_field1' => '12', 'extra_field2' => 'buy invest', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'خرید کردن shares', 'extra_field1' => '13', 'extra_field2' => 'buy shares', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'تبدیل ارزها', 'extra_field1' => '14', 'extra_field2' => 'swap', 'extra_field3' => '', 'status' => '1'],

            ['type' => 'manage_swap_rial', 'name' => 'مدیریت سواپ rial ', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'manage_swap_usdt', 'name' => 'مدیریت سواپ  usdt', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'manage_swap_btt', 'name' => 'مدیریت سواپ  btt', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'manage_swap_trx', 'name' => 'trx', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_usdt', 'name' => 'حداقل برداشت usdt', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_btt', 'name' => 'حداقل برداشت btt', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_trx', 'name' => 'حداقل برداشت trx', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_ctr', 'name' => 'حداقل برداشت ctr', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_usdt', 'name' => 'کارمزد برداشت usdt', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_btt', 'name' => 'کارمزد برداشت btt', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_ctr', 'name' => 'کارمزد برداشت ctr', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_trx', 'name' => 'کارمزد برداشت trx', 'extra_field1' => '1', 'extra_field2' => '1', 'extra_field3' => '', 'status' => '1'],
        ];
        \App\Models\BaseData::insert($array);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance_transactions');
    }
}
