<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_masters', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign("user_id")->references('id')->on('users');
            $table->integer('code')->unique();
            $table->string('title');
            $table->tinyInteger('priority')->default(0)->comment("0=>Low 1=>Medium 2=>High");
            $table->tinyInteger('status')->default(0)->comment('	1 =>Pending, 2 =>answered, 3 =>closed');
            $table->unsignedBigInteger('accept_id')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        $query = "DROP TRIGGER IF EXISTS `after_ticket_master_update`;
        CREATE TRIGGER `after_ticket_master_update` BEFORE INSERT ON `ticket_masters`
 FOR EACH ROW BEGIN
IF (NEW.code IS NULL OR NEW.code IS NOT NULL) THEN
        set @max_ticket_code = 0;
        select ifnull(max(cast(code as unsigned )),500) into @max_ticket_code from ticket_masters;
        set new.code = @max_ticket_code + 1;
        END IF;
    END";

        \Illuminate\Support\Facades\DB::unprepared($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::unprepared("DROP TRIGGER IF EXISTS `after_ticket_master_update`;");
        Schema::dropIfExists('ticket_masters');
    }
}
