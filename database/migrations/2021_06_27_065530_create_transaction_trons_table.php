<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTronsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_trons', function (Blueprint $table) {
            $table->id();
            $table->string('first_hash');
            $table->string('commission_hash')->nullable();
            $table->string('transfer_hash')->nullable();
            $table->string('ethers_data',1000)->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->float('amount', 20, 8)->default(0);
            $table->tinyInteger('status')->default(0)->comment('0=>new 1=>update amount asset 2=>request withdraw admin 3=>fail withdraw admin 4=>request withdraw user 5=>check withdraw 6=>fail withdraw user 7=>success');
            $table->string('unit');
            $table->float('amount_commission', 20, 8)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_trons');
    }
}
