<?php

namespace Database\Seeders;

use App\Models\Invest_Shares;
use Illuminate\Database\Seeder;

class ShareSeeder extends Seeder
{
    private $share = array(
        array(
            "user_id" => 6,
            "initial_value" => 100.00,
            "profit" => 823.20,
            "type" => "shares",
        ),
        array(
            "user_id" => 19,
            "initial_value" => 14.00,
            "profit" => 246,
            "type" => "shares",
        ),
        array(
            "user_id" => 113,
            "initial_value" => 10.00,
            "profit" => 193,
            "type" => "shares",
        ),
        array(
            "user_id" => 178,
            "initial_value" => 150.00,
            "profit" => 3990,
            "type" => "shares",
        ),
        array(
            "user_id" => 191,
            "initial_value" => 58.00,
            "profit" => 1277,
            "type" => "shares",
        ),
        array(
            "user_id" => 206,
            "initial_value" => 19.00,
            "profit" => 329,
            "type" => "shares",
        ),
        array(
            "user_id" => 236,
            "initial_value" => 20.00,
            "profit" => 394,
            "type" => "shares",
        ),
        array(
            "user_id" => 241,
            "initial_value" => 18.00,
            "profit" => 376,
            "type" => "shares",
        ),
        array(
            "user_id" => 244,
            "initial_value" => 230.00,
            "profit" => 1654.19,
            "type" => "shares",
        ),
        array(
            "user_id" => 310,
            "initial_value" => 10.00,
            "profit" => 58.31,
            "type" => "shares",
        ),
        array(
            "user_id" => 311,
            "initial_value" => 10.00,
            "profit" => 55.99,
            "type" => "shares",
        ),
        array(
            "user_id" => 315,
            "initial_value" => 200.00,
            "profit" => 2059.28,
            "type" => "shares",
        ),
        array(
            "user_id" => 316,
            "initial_value" => 100.00,
            "profit" => 815.30,
            "type" => "shares",
        ),
        array(
            "user_id" => 320,
            "initial_value" => 20.00,
            "profit" => 308,
            "type" => "shares",
        ),
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Invest_Shares::insert($this->share);
    }
}
