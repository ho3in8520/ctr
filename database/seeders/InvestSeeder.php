<?php

namespace Database\Seeders;

use App\Models\Invest_Shares;
use Illuminate\Database\Seeder;

class InvestSeeder extends Seeder
{
    private $invest = array(
        array(
            "user_id" => 78,
            "initial_value" => "100",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 81,
            "initial_value" => "10300",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 77,
            "initial_value" => "100",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 226,
            "initial_value" => "10",
            "profit" => "28",
            "type" => "invest",
        ),
        array(
            "user_id" => 65,
            "initial_value" => "10",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 74,
            "initial_value" => "20",
            "profit" => "21",
            "type" => "invest",
        ),
        array(
            "user_id" => 75,
            "initial_value" => "1000",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 70,
            "initial_value" => "100000",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 2,
            "initial_value" => "10",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 72,
            "initial_value" => "2180",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 1,
            "initial_value" => "11",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 52,
            "initial_value" => "1000",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 315,
            "initial_value" => "10",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 6,
            "initial_value" => "100",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 316,
            "initial_value" => "50",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 71,
            "initial_value" => "112",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 8,
            "initial_value" => "1000",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 244,
            "initial_value" => "100",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 310,
            "initial_value" => "10",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 54,
            "initial_value" => "1000",
            "profit" => "0",
            "type" => "invest",
        ),
        array(
            "user_id" => 311,
            "initial_value" => "10",
            "profit" => "0",
            "type" => "invest",
        ),
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Invest_Shares::insert($this->invest);
    }
}
