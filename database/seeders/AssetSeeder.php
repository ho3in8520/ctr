<?php

namespace Database\Seeders;

use App\Models\Asset;
use Illuminate\Database\Seeder;

class AssetSeeder extends Seeder
{
    private $users = array(
        array(
            "id" => 1,
        ),
        array(
            "id" => 2,
        ),
        array(
            "id" => 3,
        ),
        array(
            "id" => 6,
        ),
        array(
            "id" => 7,
        ),
        array(
            "id" => 8,
        ),
        array(
            "id" => 9,
        ),
        array(
            "id" => 13,
        ),
        array(
            "id" => 14,
        ),
        array(
            "id" => 15,
        ),
        array(
            "id" => 16,
        ),
        array(
            "id" => 18,
        ),
        array(
            "id" => 19,
        ),
        array(
            "id" => 24,
        ),
        array(
            "id" => 25,
        ),
        array(
            "id" => 32,
        ),
        array(
            "id" => 34,
        ),
        array(
            "id" => 35,
        ),
        array(
            "id" => 36,
        ),
        array(
            "id" => 37,
        ),
        array(
            "id" => 38,
        ),
        array(
            "id" => 39,
        ),
        array(
            "id" => 40,
        ),
        array(
            "id" => 41,
        ),
        array(
            "id" => 42,
        ),
        array(
            "id" => 43,
        ),
        array(
            "id" => 44,
        ),
        array(
            "id" => 45,
        ),
        array(
            "id" => 46,
        ),
        array(
            "id" => 47,
        ),
        array(
            "id" => 48,
        ),
        array(
            "id" => 49,
        ),
        array(
            "id" => 50,
        ),
        array(
            "id" => 51,
        ),
        array(
            "id" => 52,
        ),
        array(
            "id" => 53,
        ),
        array(
            "id" => 54,
        ),
        array(
            "id" => 55,
        ),
        array(
            "id" => 56,
        ),
        array(
            "id" => 57,
        ),
        array(
            "id" => 58,
        ),
        array(
            "id" => 59,
        ),
        array(
            "id" => 60,
        ),
        array(
            "id" => 61,
        ),
        array(
            "id" => 62,
        ),
        array(
            "id" => 63,
        ),
        array(
            "id" => 64,
        ),
        array(
            "id" => 65,
        ),
        array(
            "id" => 66,
        ),
        array(
            "id" => 67,
        ),
        array(
            "id" => 68,
        ),
        array(
            "id" => 69,
        ),
        array(
            "id" => 70,
        ),
        array(
            "id" => 71,
        ),
        array(
            "id" => 72,
        ),
        array(
            "id" => 73,
        ),
        array(
            "id" => 74,
        ),
        array(
            "id" => 75,
        ),
        array(
            "id" => 76,
        ),
        array(
            "id" => 77,
        ),
        array(
            "id" => 78,
        ),
        array(
            "id" => 79,
        ),
        array(
            "id" => 80,
        ),
        array(
            "id" => 81,
        ),
        array(
            "id" => 82,
        ),
        array(
            "id" => 83,
        ),
        array(
            "id" => 84,
        ),
        array(
            "id" => 85,
        ),
        array(
            "id" => 86,
        ),
        array(
            "id" => 87,
        ),
        array(
            "id" => 88,
        ),
        array(
            "id" => 89,
        ),
        array(
            "id" => 90,
        ),
        array(
            "id" => 91,
        ),
        array(
            "id" => 92,
        ),
        array(
            "id" => 93,
        ),
        array(
            "id" => 94,
        ),
        array(
            "id" => 95,
        ),
        array(
            "id" => 96,
        ),
        array(
            "id" => 97,
        ),
        array(
            "id" => 98,
        ),
        array(
            "id" => 99,
        ),
        array(
            "id" => 100,
        ),
        array(
            "id" => 101,
        ),
        array(
            "id" => 102,
        ),
        array(
            "id" => 103,
        ),
        array(
            "id" => 104,
        ),
        array(
            "id" => 105,
        ),
        array(
            "id" => 106,
        ),
        array(
            "id" => 107,
        ),
        array(
            "id" => 108,
        ),
        array(
            "id" => 109,
        ),
        array(
            "id" => 110,
        ),
        array(
            "id" => 111,
        ),
        array(
            "id" => 112,
        ),
        array(
            "id" => 113,
        ),
        array(
            "id" => 114,
        ),
        array(
            "id" => 115,
        ),
        array(
            "id" => 116,
        ),
        array(
            "id" => 117,
        ),
        array(
            "id" => 118,
        ),
        array(
            "id" => 119,
        ),
        array(
            "id" => 120,
        ),
        array(
            "id" => 121,
        ),
        array(
            "id" => 122,
        ),
        array(
            "id" => 123,
        ),
        array(
            "id" => 124,
        ),
        array(
            "id" => 125,
        ),
        array(
            "id" => 126,
        ),
        array(
            "id" => 127,
        ),
        array(
            "id" => 128,
        ),
        array(
            "id" => 129,
        ),
        array(
            "id" => 130,
        ),
        array(
            "id" => 131,
        ),
        array(
            "id" => 132,
        ),
        array(
            "id" => 133,
        ),
        array(
            "id" => 134,
        ),
        array(
            "id" => 135,
        ),
        array(
            "id" => 136,
        ),
        array(
            "id" => 137,
        ),
        array(
            "id" => 138,
        ),
        array(
            "id" => 139,
        ),
        array(
            "id" => 140,
        ),
        array(
            "id" => 141,
        ),
        array(
            "id" => 142,
        ),
        array(
            "id" => 143,
        ),
        array(
            "id" => 144,
        ),
        array(
            "id" => 145,
        ),
        array(
            "id" => 146,
        ),
        array(
            "id" => 147,
        ),
        array(
            "id" => 148,
        ),
        array(
            "id" => 149,
        ),
        array(
            "id" => 150,
        ),
        array(
            "id" => 151,
        ),
        array(
            "id" => 152,
        ),
        array(
            "id" => 153,
        ),
        array(
            "id" => 154,
        ),
        array(
            "id" => 155,
        ),
        array(
            "id" => 156,
        ),
        array(
            "id" => 157,
        ),
        array(
            "id" => 158,
        ),
        array(
            "id" => 159,
        ),
        array(
            "id" => 160,
        ),
        array(
            "id" => 161,
        ),
        array(
            "id" => 162,
        ),
        array(
            "id" => 163,
        ),
        array(
            "id" => 164,
        ),
        array(
            "id" => 165,
        ),
        array(
            "id" => 166,
        ),
        array(
            "id" => 167,
        ),
        array(
            "id" => 168,
        ),
        array(
            "id" => 169,
        ),
        array(
            "id" => 170,
        ),
        array(
            "id" => 171,
        ),
        array(
            "id" => 172,
        ),
        array(
            "id" => 173,
        ),
        array(
            "id" => 174,
        ),
        array(
            "id" => 175,
        ),
        array(
            "id" => 176,
        ),
        array(
            "id" => 177,
        ),
        array(
            "id" => 178,
        ),
        array(
            "id" => 179,
        ),
        array(
            "id" => 180,
        ),
        array(
            "id" => 181,
        ),
        array(
            "id" => 182,
        ),
        array(
            "id" => 183,
        ),
        array(
            "id" => 184,
        ),
        array(
            "id" => 185,
        ),
        array(
            "id" => 186,
        ),
        array(
            "id" => 187,
        ),
        array(
            "id" => 188,
        ),
        array(
            "id" => 189,
        ),
        array(
            "id" => 190,
        ),
        array(
            "id" => 191,
        ),
        array(
            "id" => 192,
        ),
        array(
            "id" => 193,
        ),
        array(
            "id" => 194,
        ),
        array(
            "id" => 195,
        ),
        array(
            "id" => 196,
        ),
        array(
            "id" => 197,
        ),
        array(
            "id" => 198,
        ),
        array(
            "id" => 199,
        ),
        array(
            "id" => 200,
        ),
        array(
            "id" => 201,
        ),
        array(
            "id" => 202,
        ),
        array(
            "id" => 203,
        ),
        array(
            "id" => 204,
        ),
        array(
            "id" => 205,
        ),
        array(
            "id" => 206,
        ),
        array(
            "id" => 207,
        ),
        array(
            "id" => 208,
        ),
        array(
            "id" => 209,
        ),
        array(
            "id" => 210,
        ),
        array(
            "id" => 211,
        ),
        array(
            "id" => 212,
        ),
        array(
            "id" => 213,
        ),
        array(
            "id" => 214,
        ),
        array(
            "id" => 215,
        ),
        array(
            "id" => 216,
        ),
        array(
            "id" => 217,
        ),
        array(
            "id" => 218,
        ),
        array(
            "id" => 219,
        ),
        array(
            "id" => 220,
        ),
        array(
            "id" => 221,
        ),
        array(
            "id" => 222,
        ),
        array(
            "id" => 223,
        ),
        array(
            "id" => 224,
        ),
        array(
            "id" => 225,
        ),
        array(
            "id" => 226,
        ),
        array(
            "id" => 227,
        ),
        array(
            "id" => 228,
        ),
        array(
            "id" => 229,
        ),
        array(
            "id" => 230,
        ),
        array(
            "id" => 231,
        ),
        array(
            "id" => 232,
        ),
        array(
            "id" => 233,
        ),
        array(
            "id" => 234,
        ),
        array(
            "id" => 235,
        ),
        array(
            "id" => 236,
        ),
        array(
            "id" => 237,
        ),
        array(
            "id" => 238,
        ),
        array(
            "id" => 239,
        ),
        array(
            "id" => 240,
        ),
        array(
            "id" => 241,
        ),
        array(
            "id" => 242,
        ),
        array(
            "id" => 243,
        ),
        array(
            "id" => 244,
        ),
        array(
            "id" => 245,
        ),
        array(
            "id" => 246,
        ),
        array(
            "id" => 247,
        ),
        array(
            "id" => 248,
        ),
        array(
            "id" => 249,
        ),
        array(
            "id" => 250,
        ),
        array(
            "id" => 251,
        ),
        array(
            "id" => 252,
        ),
        array(
            "id" => 253,
        ),
        array(
            "id" => 254,
        ),
        array(
            "id" => 255,
        ),
        array(
            "id" => 256,
        ),
        array(
            "id" => 258,
        ),
        array(
            "id" => 259,
        ),
        array(
            "id" => 260,
        ),
        array(
            "id" => 261,
        ),
        array(
            "id" => 262,
        ),
        array(
            "id" => 263,
        ),
        array(
            "id" => 264,
        ),
        array(
            "id" => 265,
        ),
        array(
            "id" => 266,
        ),
        array(
            "id" => 267,
        ),
        array(
            "id" => 268,
        ),
        array(
            "id" => 269,
        ),
        array(
            "id" => 270,
        ),
        array(
            "id" => 271,
        ),
        array(
            "id" => 272,
        ),
        array(
            "id" => 273,
        ),
        array(
            "id" => 274,
        ),
        array(
            "id" => 275,
        ),
        array(
            "id" => 276,
        ),
        array(
            "id" => 277,
        ),
        array(
            "id" => 278,
        ),
        array(
            "id" => 279,
        ),
        array(
            "id" => 280,
        ),
        array(
            "id" => 281,
        ),
        array(
            "id" => 282,
        ),
        array(
            "id" => 283,
        ),
        array(
            "id" => 284,
        ),
        array(
            "id" => 285,
        ),
        array(
            "id" => 286,
        ),
        array(
            "id" => 287,
        ),
        array(
            "id" => 288,
        ),
        array(
            "id" => 289,
        ),
        array(
            "id" => 290,
        ),
        array(
            "id" => 291,
        ),
        array(
            "id" => 292,
        ),
        array(
            "id" => 293,
        ),
        array(
            "id" => 294,
        ),
        array(
            "id" => 295,
        ),
        array(
            "id" => 296,
        ),
        array(
            "id" => 297,
        ),
        array(
            "id" => 298,
        ),
        array(
            "id" => 299,
        ),
        array(
            "id" => 300,
        ),
        array(
            "id" => 301,
        ),
        array(
            "id" => 302,
        ),
        array(
            "id" => 303,
        ),
        array(
            "id" => 304,
        ),
        array(
            "id" => 305,
        ),
        array(
            "id" => 306,
        ),
        array(
            "id" => 307,
        ),
        array(
            "id" => 308,
        ),
        array(
            "id" => 309,
        ),
        array(
            "id" => 310,
        ),
        array(
            "id" => 311,
        ),
        array(
            "id" => 312,
        ),
        array(
            "id" => 313,
        ),
        array(
            "id" => 314,
        ),
        array(
            "id" => 315,
        ),
        array(
            "id" => 316,
        ),
        array(
            "id" => 317,
        ),
        array(
            "id" => 318,
        ),
        array(
            "id" => 319,
        ),
        array(
            "id" => 320,
        ),
        array(
            "id" => 321,
        ),
    );
    private $amount_usdt = array(
        array(
            "user_id" => 1,
            "amount" => "26.47",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 2,
            "amount" => "12",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 6,
            "amount" => "815",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 8,
            "amount" => "5",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 9,
            "amount" => "5",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 114,
            "amount" => "0.5",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 174,
            "amount" => "2.8",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 178,
            "amount" => "3.1",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 244,
            "amount" => "580.5",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 259,
            "amount" => "0",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 309,
            "amount" => "1",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 310,
            "amount" => "58.4",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 311,
            "amount" => "78.9",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 315,
            "amount" => "815",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 316,
            "amount" => "850",
            "unit" => "USDT",
        ),
        array(
            "user_id" => 320,
            "amount" => "3",
            "unit" => "USDT",
        ),
    );
    private $amount_ctr = array(
        array(
            "user_id" => 1,
            "amount" => "4260311.8632728",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 2,
            "amount" => "986322.06709031",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 6,
            "amount" => "285",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 8,
            "amount" => "808.72107186358",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 13,
            "amount" => "12",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 14,
            "amount" => "10",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 19,
            "amount" => "47.889123020706",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 24,
            "amount" => "40.00144",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 50,
            "amount" => "10",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 51,
            "amount" => "200",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 52,
            "amount" => "535.09817295981",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 54,
            "amount" => "724.62362971985",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 55,
            "amount" => "60",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 56,
            "amount" => "100",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 60,
            "amount" => "9.7",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 61,
            "amount" => "100",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 65,
            "amount" => "4.7785140073082",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 70,
            "amount" => "131565.57207308",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 71,
            "amount" => "151.81411936663",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 72,
            "amount" => "1069.4261632156",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 73,
            "amount" => "300",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 74,
            "amount" => "21",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 75,
            "amount" => "589.8587088916",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 76,
            "amount" => "141.62",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 77,
            "amount" => "49.794884287454",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 78,
            "amount" => "49.794884287454",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 80,
            "amount" => "46.16",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 81,
            "amount" => "5072.1666260658",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 83,
            "amount" => "81.048076209501",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 97,
            "amount" => "36",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 104,
            "amount" => "148",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 107,
            "amount" => "20",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 108,
            "amount" => "36",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 110,
            "amount" => "40",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 115,
            "amount" => "129.4",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 120,
            "amount" => "1367.8353105968",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 121,
            "amount" => "109",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 124,
            "amount" => "39.16",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 134,
            "amount" => "100",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 139,
            "amount" => "111.2",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 142,
            "amount" => "100",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 143,
            "amount" => "171.4",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 150,
            "amount" => "52",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 167,
            "amount" => "1039",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 171,
            "amount" => "48",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 172,
            "amount" => "285",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 173,
            "amount" => "195",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 178,
            "amount" => "85.976",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 179,
            "amount" => "54",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 181,
            "amount" => "48",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 183,
            "amount" => "33",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 190,
            "amount" => "106",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 191,
            "amount" => "14.67",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 195,
            "amount" => "27",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 197,
            "amount" => "32",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 198,
            "amount" => "42",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 199,
            "amount" => "83",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 201,
            "amount" => "42",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 203,
            "amount" => "45",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 226,
            "amount" => "22.31",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 236,
            "amount" => "2.523",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 241,
            "amount" => "2.46",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 244,
            "amount" => "10.4",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 259,
            "amount" => "0",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 262,
            "amount" => "0",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 310,
            "amount" => "20.84",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 311,
            "amount" => "20.42",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 315,
            "amount" => "4.4",
            "unit" => "CTR",
        ),
        array(
            "user_id" => 316,
            "amount" => "2",
            "unit" => "CTR",
        ),
    );
    private $amount_btt = array(
        array(
            "user_id" => 1,
            "amount" => "5750.9179892463",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 2,
            "amount" => "11",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 6,
            "amount" => "813.69497262974",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 13,
            "amount" => "900",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 14,
            "amount" => "80",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 19,
            "amount" => "246",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 50,
            "amount" => "20",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 76,
            "amount" => "15",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 83,
            "amount" => "15.107296137339",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 113,
            "amount" => "193",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 120,
            "amount" => "65",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 167,
            "amount" => "640",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 178,
            "amount" => "3990",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 191,
            "amount" => "1277",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 206,
            "amount" => "329",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 236,
            "amount" => "394",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 241,
            "amount" => "376",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 244,
            "amount" => "1630.1981417277",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 249,
            "amount" => "3",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 250,
            "amount" => "29",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 251,
            "amount" => "9",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 259,
            "amount" => "0",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 267,
            "amount" => "99",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 268,
            "amount" => "1",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 272,
            "amount" => "10",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 273,
            "amount" => "5",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 276,
            "amount" => "10",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 279,
            "amount" => "1395",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 280,
            "amount" => "800",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 282,
            "amount" => "20",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 284,
            "amount" => "20",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 290,
            "amount" => "1500",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 293,
            "amount" => "130",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 294,
            "amount" => "30",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 301,
            "amount" => "10",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 307,
            "amount" => "300",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 310,
            "amount" => "68.324341881158",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 311,
            "amount" => "65.965270665477",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 313,
            "amount" => "300",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 315,
            "amount" => "2037.8535998512",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 316,
            "amount" => "815.2995425508",
            "unit" => "BTT",
        ),
        array(
            "user_id" => 320,
            "amount" => "308",
            "unit" => "BTT",
        ),
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $assets = [];
        foreach ($this->users as $user) {
            array_push($assets, ['user_id' => $user['id'], 'unit' => 'usdt', 'name' => 'USDT', 'amount' => 0, 'logo' => 'usdt', 'type_token' => 20]);
            array_push($assets, ['user_id' => $user['id'], 'unit' => 'ctr', 'name' => 'CoinTread', 'amount' => 0, 'logo' => 'ctr', 'type_token' => 20]);
            array_push($assets, ['user_id' => $user['id'], 'unit' => 'btt', 'name' => 'BitTorrent', 'amount' => 0, 'logo' => 'btt', 'type_token' => 10]);
        }
        Asset::insert($assets);
        foreach ($this->amount_usdt as $usdt) {
            Asset::where('user_id', $usdt['user_id'])->where('unit', 'usdt')->update(['amount' => $usdt['amount']]);
        }
        foreach ($this->amount_ctr as $ctr) {
            Asset::where('user_id', $ctr['user_id'])->where('unit', 'ctr')->update(['amount' => $ctr['amount']]);
        }
        foreach ($this->amount_btt as $btt) {
            Asset::where('user_id', $btt['user_id'])->where('unit', 'btt')->update(['amount' => $btt['amount']]);
        }
    }
}
