@extends('templates.user.master_page')
@section('title_browser')
    Show Ticket {{ $result->code }}
@endsection
@section('style')
    {!! htmlScriptTagJsApi() !!}
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">SHOW TICKET</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row ticket">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-container">
                    Show Ticket {{ $result->code }}
                </div>
                <div class="card-body card-body-container">
                    <div class="content">
                        <h5>{{ $result->title }}</h5>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card shadow mt-2 mb-4">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">Create time
                                                : {{ $result->created_at }}</div>
                                            <div class="col-md-4 col-sm-12">Last update
                                                : {{ $result->ticket_detail[0]->created_at }}</div>
                                            <div class="col-md-4 col-sm-12">Ticket status: {{ $result->status() }}</div>
                                        </div>
                                    </div>
                                </div>
                                @if($result->status!=3)
                                    <button class="btn btn-success mb-4" type="button" data-toggle="collapse"
                                            data-target="#collapseExample" aria-expanded="false"
                                            aria-controls="collapseExample">
                                        answer
                                    </button>
                                    <div class="collapse {{ ($errors->any())?'show':'' }}" id="collapseExample">
                                        <div class="card card-body">
                                            <form method="post" action="{{ route("ticket.update",$result->code) }}"
                                                  enctype="multipart/form-data">
                                                @csrf
                                                <FIELDSET>
                                                    <div class="form-group row">
                                                        <label for="email" class="col-sm-2 col-form-label">email</label>
                                                        <div class="col-sm-10 @error('email') has-danger @enderror">
                                                            <input type="email" class="form-control" id="email"
                                                                   placeholder="email"
                                                                   value="{{ $result->user->email }}"
                                                                   disabled>
                                                            @error('email') <span
                                                                class="text-danger">{{ $message }}</span> @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="title" class="col-sm-2 col-form-label">title</label>
                                                        <div class="col-sm-10 @error('title') has-danger @enderror">
                                                            <input type="text" class="form-control" id="title"
                                                                   placeholder="title" value="{{ $result->title }}"
                                                                   disabled>
                                                            @error('title') <span
                                                                class="text-danger">{{ $message }}</span> @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="description"
                                                               class="col-sm-2 col-form-label">description</label>
                                                        <div
                                                            class="col-sm-10 @error('description') has-danger @enderror">
                                                        <textarea class="form-control" rows="7" name="description"
                                                                  id="description">{{ old('description') }}</textarea>
                                                            @error('description') <span
                                                                class="text-danger">{{ $message }}</span> @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="description"
                                                               class="col-sm-2 col-form-label">attachment</label>
                                                        <div
                                                            class="col-xs-10 col-sm-10 col-md-10 row @error('file') has-danger @enderror">
                                                            <input type="file" name="attachment"
                                                                   class="btn col-xs-12 col-sm-5 form-control">
                                                        </div>
                                                    </div>
                                                    @error('attachment') <span
                                                        class="text-danger">{{ $message }}</span> @enderror
                                                    <hr>
                                                    <div class="form-group row">
                                                        {!! htmlFormSnippet() !!}
                                                    </div>
                                                    @error('g-recaptcha-response') <span
                                                        class="text-danger">{{ $message }}</span> @enderror
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-success">submit</button>
                                                    </div>
                                                </FIELDSET>
                                            </form>
                                        </div>
                                    </div>
                                @endif
                                @foreach($result->ticket_detail as $item)
                                    @if($item->type==1)
                                        <div class="card card-question-answer shadow">
                                            <div class="card-header card-secondary text-white">
                                                <i class="fa fa-comment"></i>
                                                <span class="font-weight-bold name">{{ $item->user->email }}</span>
                                            </div>
                                            <div class="card-body">
                                                {{ $item->description }}
                                            </div>
                                            <div class="card-footer">
                                                <i class="mdi mdi-clock-outline"></i>
                                                <span>{{ \Illuminate\Support\Carbon::parse($item->created_at)->format('Y-m-d H:i') }}</span>
                                                @if($item->attachment)
                                                    <span class="float-right">
                                                        <a title="Attachment" target="_blank"
                                                           href="{{ get_file_ticket($item->attachment) }}">
                                                    <i class="fa fa-file">Attachment </i>
                                                            </a>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    @else
                                        <div class="card card-question-answer shadow">
                                            <div class="card-header card-primary text-white">
                                                <i class="fa fa-comment"></i>
                                                <span
                                                    class="font-weight-bold name">{{ $item->admin->email }} | Support</span>
                                            </div>
                                            <div class="card-body">
                                                {{ $item->description }}
                                            </div>
                                            <div class="card-footer">
                                                <i class="mdi mdi-clock-outline"></i>
                                                <span>{{ \Illuminate\Support\Carbon::parse($item->created_at)->format('Y-m-d H:i') }}</span>
                                                @if($item->attachment)
                                                    <span class="float-right">
                                                        <a title="Attachment" target="_blank"
                                                           href="{{ get_file_ticket($item->attachment) }}">
                                                    <i class="fa fa-file">Attachment </i>
                                                            </a>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
