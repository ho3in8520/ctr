@extends('templates.user.master_page')
@section('title_browser')
    List Ticket
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">LIST TICKET</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12 mb-4">
                        <a class="btn btn-success" href="{{ route('ticket.create') }}" title="ticket new">New Ticket</a>
                    </div>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">code</th>
                            <th scope="col">title</th>
                            <th scope="col">priority</th>
                            <th scope="col">status</th>
                            <th scope="col">created at</th>
                            <th scope="col">operation</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($result->total())
                        @foreach($result as $item)
                            @php
                                $status_type=null;
                                $status_class=null;
                                   $priority_type=null;
                                $priority_class=null;
                                    switch ($item->status){
        case 1;
        $status_class='warning';
        $status_type='pending';
        break;
        case 2;
        $status_class='info';
        $status_type='answered';
        break;
        case 3;
        $status_class='secondary';
        $status_type='closed';
        break;

    }
    switch ($item->priority){
        case 0;
        $priority_class='warning';
        $priority_type='Low';
        break;
        case 1;
        $priority_class='info';
        $priority_type='Medium';
        break;
        case 2;
        $priority_class='danger';
        $priority_type='High';
        break;

    }
                            @endphp
                            <tr>
                                <td>{{ index($result,$loop) }}</td>
                                <td>{{ $item->code }}</td>
                                <td>{{ $item->title }}</td>
                                <td><span class="badge badge-{{ $priority_class }}">{{ $priority_type }}</span></td>
                                <td><span class="badge badge-{{ $status_class }}">{{ $status_type }}</span></td>
                                <td>{{ \Carbon\Carbon::parse($item->created_at)->format("Y-m-d") }}</td>
                                <td>
                                    <a title="show" class="btn btn-primary btn-sm"
                                       href="{{ route('ticket.show',$item->code) }}"><i
                                            class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        @else
                        <tr>
                            <th colspan="7" class="text-center">Nothing found</th>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                    {!! $result->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
