@extends('templates.user.master_page')
@section('title_browser')
    Settings
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h3 class="page-title m-0">Settings</h3>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Google Authenticato(withdraw)</label>
                        <div class="col-sm-10 my-auto">
                            @if($google_2fa != false)
                                <button type="button" class="btn btn-sm btn-info waves-effect waves-light"
                                        data-toggle="modal" data-target="#google2faModal">Activate
                                </button>
                                <!-- sample modal content -->
                                <div id="google2faModal" class="modal fade" tabindex="-1" role="dialog"
                                     aria-labelledby="google2faModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <form method="post" action="{{ route('user.register_authenticator') }}">
                                                @csrf
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0" id="google2faModalLabel">Active Google
                                                        2fa
                                                        Authenticator</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5 class="font-16">Scan Bacode</h5>
                                                    <p class="text-center">Set up your two factor authentication by
                                                        scanning
                                                        the barcode below. Alternatively, you can use the code <code
                                                            style="font-size: 15px;">{{ $google_2fa['secret'] }}</code>
                                                    </p>
                                                    <p class="text-center">You must set up your Google Authenticator app
                                                        before continuing. You will be unable to login otherwise</p>
                                                    <img src="{{ $google_2fa['QR_Image'] }}" class="d-block mx-auto">
                                                    <p class="text-center">
                                                        After scan barcode pls enter code in blow input
                                                    </p>
                                                    <div class="row col-12">
                                                        <label>Authenticator Code</label>
                                                        <input type="text" name="one_time_password" class="form-control"
                                                               placeholder="example: 123456">
                                                        <input type="hidden" name="message">
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary waves-effect"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                    <input type="hidden" name="secret"
                                                           value="{{ $google_2fa['secret'] }}">
                                                    <button type="button"
                                                            class="btn btn-primary waves-effect waves-light ajaxStore">
                                                        Complete
                                                    </button>

                                                </div>
                                            </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            @else
                                <button type="button" class="btn btn-sm btn-danger waves-effect waves-light"
                                        data-toggle="modal" data-target="#google2faModal">Deactivate
                                </button>
                                <!-- sample modal content -->
                                <div id="google2faModal" class="modal fade" tabindex="-1" role="dialog"
                                     aria-labelledby="google2faModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <form method="post" action="{{ route('user.deactivate_authenticator') }}">
                                                @csrf
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0" id="google2faModalLabel">deactivate
                                                        Google
                                                        2fa
                                                        Authenticator</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row col-12">
                                                        <label>Authenticator Code</label>
                                                        <input type="text" name="one_time_password" class="form-control"
                                                               placeholder="example: 123456">
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary waves-effect"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                    <button type="button"
                                                            class="btn btn-danger waves-effect waves-light ajaxStore">
                                                        DeActive
                                                    </button>
                                                </div>
                                            </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Two Factor Authentication (login)</label>
                        <div class="col-sm-10 my-auto">
                            @if($google_2fa != false)
                                <span class="text-danger">please first active Google Authenticator</span>
                            @else
                                @if($user->login_2fa == 0)
                                    <button type="button" class="btn btn-sm btn-success waves-effect waves-light"
                                            data-toggle="modal" data-target="#login_2fa">Active
                                    </button>
                                    <!-- sample modal content -->
                                    <div id="login_2fa" class="modal fade" tabindex="-1" role="dialog"
                                         aria-labelledby="google2faModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <form method="post" action="{{ route('user.login_2fa') }}">
                                                    @csrf
                                                    <input type="hidden" name="type" value="active">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0" id="google2faModalLabel">Active
                                                            2fa login</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row col-12">
                                                            <label>Authenticator Code</label>
                                                            <input type="text" name="one_time_password" class="form-control"
                                                                   placeholder="example: 123456">
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary waves-effect"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                        <button type="button"
                                                                class="btn btn-success waves-effect waves-light ajaxStore">
                                                            Active
                                                        </button>
                                                    </div>
                                                </form>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                @else
                                    <button type="button" class="btn btn-sm btn-danger waves-effect waves-light"
                                            data-toggle="modal" data-target="#login_2fa">Deactivate
                                    </button>
                                    <!-- sample modal content -->
                                    <div id="login_2fa" class="modal fade" tabindex="-1" role="dialog"
                                         aria-labelledby="google2faModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <form method="post" action="{{ route('user.login_2fa') }}">
                                                    @csrf
                                                    <input type="hidden" name="type" value="deactivate">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0" id="google2faModalLabel">deactivate
                                                            2fa login</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row col-12">
                                                            <label>Authenticator Code</label>
                                                            <input type="text" name="one_time_password" class="form-control"
                                                                   placeholder="example: 123456">
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary waves-effect"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                        <button type="button"
                                                                class="btn btn-danger waves-effect waves-light ajaxStore">
                                                            deactivate
                                                        </button>
                                                    </div>
                                                </form>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
