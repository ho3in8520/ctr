@extends('templates.user.master_page')
@section('title_browser')
    notification
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">List notification</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">title</th>
                            <th scope="col">description</th>
                            <th scope="col">created at</th>
                            <th scope="col">operation</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($notifications as $item)
                            @php
                                $class=(!$item->viewNotification)?'font-weight-bold':'';
                            @endphp
                            <tr>
                                <td class="{{ $class }}">{{ index($notifications,$loop) }}</td>
                                <td class="{{ $class }}">{{ $item->title }}</td>
                                <td class="{{ $class }}">{!! \Illuminate\Support\Str::limit($item->description,30) !!}</td>
                                <td class="{{ $class }}">{{ \Carbon\Carbon::parse($item->created_at)->format('Y/m/d') }}</td>
                                <td>
                                    <a title="show" class="btn btn-primary btn-sm"
                                       href="{{ route('notification.show',base64_encode($item->id)) }}"><i
                                            class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $notifications->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
