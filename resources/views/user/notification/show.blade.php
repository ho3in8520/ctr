@extends('templates.user.master_page')
@section('title_browser')
    notification
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">{{ $notification->title }}</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="card faq-box">
        <div class="card-body">
            <div class="faq-icon">
                <i class="dripicons-question h2 icon-one"></i>
                <i class="dripicons-question h2 icon-two"></i>
            </div>
             {!! $notification->description !!}
        </div>
    </div>
@endsection
