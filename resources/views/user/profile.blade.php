@extends('templates.user.master_page')
@section('title_browser')
    profile
@endsection

@section('content')
    <section>
        <form method="post" action="{{route('change-password',$user->id)}}">
        @csrf
        @method('PUT')
        <!-- مدال مربوط به تغییر رمز عبور کاربر -->
            <div id="change_pass_modal" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="change_pass_modalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title mt-0" id="BlockModalLabel">Change Password</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group text-left">
                                <label>Current Password</label>
                                <input type="password" class="form-control"
                                       name="current_password">
                            </div>
                            <div class="form-group text-left">
                                <label>New Password</label>
                                <input type="password" class="form-control"
                                       name="password">
                            </div>
                            <div class="form-group text-left">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control"
                                       name="password_confirmation">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel
                            </button>
                            <button type="button" class="btn btn-info waves-effect waves-light ajaxStore">Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.مدال مربوط به تغییر رمز عبور کاربر -->

        </form>
        <div class="row pt-3">
            <h4>Profile</h4>
        </div>
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('profile.update',$user->id)}}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="row justify-content-center">
                                <div class="col-5">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control" name="first_name"
                                               placeholder="Enter Your First Name" value="{{$user->first_name}}">
                                        <div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control"
                                               name="last_name"
                                               placeholder="Enter Your Last Name" value="{{$user->last_name}}">
                                        <div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center mt-2">
                                <div class="col-5">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <div>
                                            <input type="email" class="form-control" readonly
                                                   name="email" value="{{$user->email}}">
                                        </div>
                                    </div>
                                </div>
                                @if($referral_email)
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label>Referral Email</label>
                                            <div>
                                                <input type="email" class="form-control" readonly
                                                       name="referral_email"
                                                       value="{{$referral_email}}">
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label>Referral Code</label>
                                            <div>
                                                <input type="text" class="form-control"
                                                       name="referral_code">
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row justify-content-center mt-2">
                                <div class="col-5">
                                    <div class="form-group">
                                        <label>Invite Link</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button type="button" title="copy link"
                                                        class="btn btn-warning btn-copy" data-link="invite_link">
                                                    <i class="fa fa-copy"></i></button>
                                            </div>
                                            <input type="text" class="form-control" readonly
                                                   value="{{ route('register',['code'=>base64_encode($user->code)]) }}"
                                                   id="invite_link">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-b-0 text-center mt-2">
                                <div>
                                    <button type="submit" id="my_button"
                                            class="btn btn-primary waves-effect waves-light ajaxStore">
                                        Submit
                                    </button>
                                    <button type="button" id="change_pass"
                                            class="btn btn-warning waves-effect waves-light "
                                            data-target="#change_pass_modal" data-toggle="modal">
                                        change password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@section('script')
    <script>
        $(".btn-copy").on('click', function () {
            let elm = document.getElementById($(this).data('link'));
            if (copyToClipboard(elm) == true) {
                swal('Your invite link copied successfully', '', 'success');
            }
        });

    </script>
@endsection
