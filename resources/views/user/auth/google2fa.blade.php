@extends('templates.user.auth.master_page')
@section('title_browser')
    Authenticator
@endsection
@section('content')
    <div class="col-lg-5 offset-lg-1">
        <div class="card mb-0">
            <div class="card-body">
                @include('component.flash_message')
                <div class="p-2">
                    <h4 class="text-muted float-right font-18 mt-4">Authenticator</h4>
                    <div>
                        <a href="index.html" class="logo logo-admin"><img
                                src="{{ asset('theme/admin/assets/images/CTR-logo.png') }}" height="50"
                                alt="logo"></a>
                    </div>
                </div>

                <div class="p-2">
                    <form class="form-horizontal m-t-20" method="post" action="{{ route('login.step') }}">
                        @csrf
                        <div
                            class="form-group row @error('code') has-danger @enderror @error('code') has-danger @enderror">
                            <div class="col-12">
                                <input class="form-control" type="text" name="code" maxlength="6" required="" value="{{ old('code') }}"
                                       placeholder="Authenticator Code">
                                @error('code') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="form-group text-center row mt-5">
                            <div class="col-12">
                                <button class="btn btn-primary btn-block waves-effect waves-light"
                                        type="submit">Log
                                    In
                                </button>
                            </div>
                        </div>

                        <div class="form-group m-t-10 mb-0 row">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
