@extends('templates.user.auth.master_page')
@section('title_browser')
    login
@endsection
@section('content')
    <div class="col-lg-5 offset-lg-1">
        <div class="card mb-0">
            <div class="card-body">
                @include('component.flash_message')
                <div class="p-2">
                    <h4 class="text-muted float-right font-18 mt-4">Sign In</h4>
                    <div>
                        <a href="index.html" class="logo logo-admin"><img
                                src="{{ asset('theme/admin/assets/images/CTR-logo.png') }}" height="50"
                                alt="logo"></a>
                    </div>
                </div>

                <div class="p-2">
                    <form class="form-horizontal m-t-20" method="post" action="{{ route('login.login') }}">
                        @csrf
                        <div
                            class="form-group row @error('email') has-danger @enderror @error('not_match') has-danger @enderror">
                            <div class="col-12">
                                <input class="form-control" type="email" name="email" required="" placeholder="Email">
                                @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                                @error('not_match') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div
                            class="form-group row @error('password') has-danger @enderror @error('not_match') has-danger @enderror">
                            <div class="col-12">
                                <div class="input-group">
                                    <input class="form-control" type="password" name="password" required=""
                                           placeholder="Password">
                                    <div class="input-group-append">
                                        <button
                                            class="password-strength__visibility show-password btn btn-outline-secondary"
                                            type="button">
                                            <span class="" data-visible="hidden">
                                                <i class="fas fa-eye-slash"></i>
                                            </span>
                                            <span class="d-none" data-visible="visible">
                                                <i class="fas fa-eye"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                                @error('password') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="remember" class="custom-control-input"
                                           id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Remember me</label>
                                </div>
                            </div>
                        </div>
                        {!! htmlFormSnippet() !!}
                        @error('g-recaptcha-response') <span class="text-danger">{{ $message }}</span> @enderror
                        <div class="form-group text-center row mt-5">
                            <div class="col-12">
                                <button class="btn btn-primary btn-block waves-effect waves-light"
                                        type="submit" disabled>Log
                                    In
                                </button>
                            </div>
                        </div>

                        <div class="form-group m-t-10 mb-0 row">
                            <div class="col-sm-7 m-t-20">
                                <a href="{{ route('forget.form') }}" class="text-muted"><i class="mdi mdi-lock"></i>
                                    Forgot
                                    your password?</a>
                            </div>
                            <div class="col-sm-5 m-t-20">
                                <a href="{{ route('register.form') }}" class="text-muted"><i
                                        class="mdi mdi-account-circle"></i>
                                    Create an account</a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
