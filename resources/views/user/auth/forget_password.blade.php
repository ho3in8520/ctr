@extends('templates.user.auth.master_page')
@section('title_browser')
    forget-password
@endsection
@section('content')
    <div class="col-lg-5 offset-lg-1">
        <div class="card mb-0">
            <div class="card-body">
                @include('component.flash_message')
                <div class="text-center">
                    <div>
                        <a href="#" class="logo logo-admin"><img src="{{ asset('theme/landing/images/logo-rtc4.png') }}" height="70" alt="logo"></a>
                    </div>
                    <h4 class="text-muted font-18 mt-4">Reset Password</h4>
                </div>

                <div class="p-2">
                    <form class="form-horizontal m-t-20" method="post" action="{{ route('forget.send-email') }}">
                        @csrf
                        <div class="form-group row @error('email') has-danger @enderror">
                            <div class="col-12">
                                <input class="form-control" type="email" name="email" required="" placeholder="Email">
                                @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group text-center row m-t-20">
                            <div class="col-12">
                                <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Send Email</button>
                            </div>
                        </div>

                    </form>
                    <!-- end form -->
                </div>

            </div>
        </div>
    </div>
@endsection
