@extends('templates.user.auth.master_page')
@section('title_browser')
    register
@endsection
@section('style')
    <style>
        .js-hidden {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="col-lg-5 offset-lg-1">
        <div class="card mb-0">
            <div class="card-body">
                @include('component.flash_message')
                <div class="p-2">
                    <h4 class="text-muted float-right font-18 mt-4">Register</h4>
                    <div>
                        <a href="#" class="logo logo-admin"><img src="{{ asset('theme/landing/images/logo-rtc4.png') }}" height="70" alt="logo"></a>
                    </div>
                </div>
                <div class="p-2">
                    <form class="form-horizontal m-t-20" method="post" action="{{ route('register.store') }}">
                        @csrf
                        <div class="form-group row @error('email') has-danger @enderror">
                            <div class="col-12">
                                <input class="form-control" name="email" type="email" required=""
                                       value="{{ old('email') }}" placeholder="Email" autofocus>
                                @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group row password-strength @error('password') has-danger @enderror">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input class="password-strength__input form-control" name="password" type="password"
                                           id="password-input" aria-describedby="passwordHelp"
                                           placeholder="Enter password"/>
                                    <div class="input-group-append">
                                        <button class="password-strength__visibility btn btn-outline-secondary" type="button">
                                            <span class="password-strength__visibility-icon" data-visible="hidden">
                                                <i class="fas fa-eye-slash"></i>
                                            </span>
                                            <span class="password-strength__visibility-icon js-hidden" data-visible="visible">
                                                <i class="fas fa-eye"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                                <small class="password-strength__error text-danger js-hidden">This symbol is not
                                    allowed!</small>
                                @error('password') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="password-strength__bar-block progress mb-4">
                            <div class="password-strength__bar progress-bar bg-danger" role="progressbar"
                                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="form-group row @error('password_confirmation') has-danger @enderror">
                            <div class="col-12">
                                <div class="input-group">
                                    <input class="form-control password-strength--input" name="password_confirmation" type="password" required=""
                                           placeholder="Confirm Password">
                                    <div class="input-group-append">
                                        <button class="password-strength__visibility show-password btn btn-outline-secondary" type="button">
                                            <span class="" data-visible="hidden">
                                                <i class="fas fa-eye-slash"></i>
                                            </span>
                                            <span class="d-none" data-visible="visible">
                                                <i class="fas fa-eye"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                                @error('password_confirmation') <span
                                    class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group row @error('ref_code') has-danger @enderror">
                            <div class="col-12">
                                @if(isset($ref) && count($ref)>0)
                                    <input class="form-control" name="ref_code" readonly
                                           value="{{ $ref['code']? $ref['code']:old('ref_code') }}"
                                           placeholder="Referral Code">
                                    <span class="text-success ml-2">({{$ref['email']}})</span>
                                @else
                                    <input class="form-control" name="ref_code"
                                           value="{{old('ref_code')}}"
                                           placeholder="Referral Code">
                                @endif
                                @error('ref_code') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group row @error('terms') has-danger @enderror">
                            <div class="col-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="terms" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label font-weight-normal" for="customCheck1">I accept
                                        <a href="#" class="text-primary">Terms and Conditions</a></label>
                                </div>
                                @error('terms') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        {!! htmlFormSnippet() !!}
                        @error('g-recaptcha-response') <span class="text-danger">{{ $message }}</span> @enderror
                        <div class="form-group text-center row m-t-20">
                            <div class="col-12">
                                <button class="btn btn-primary btn-block waves-effect waves-light" type="submit" disabled>
                                    Register
                                </button>
                            </div>
                        </div>

                        <div class="form-group m-t-10 mb-0 row">
                            <div class="col-12 m-t-20 text-center">
                                <a href="{{ route('login.form') }}" class="text-muted">Already have account?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('general/js/password-strength.js') }}"></script>
@endsection
