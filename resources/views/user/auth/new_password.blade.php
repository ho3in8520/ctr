@extends('templates.user.auth.master_page')
@section('title_browser')
    new-password
@endsection
@section('style')
    <style>
        .js-hidden {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="col-lg-5 offset-lg-1">
        <div class="card mb-0">
            <div class="card-body">
                @include('component.flash_message')
                <div class="text-center">
                    <div>
                        <a href="#" class="logo logo-admin"><img src="{{ asset('theme/landing/images/logo-rtc4.png') }}" height="70" alt="logo"></a>
                    </div>
                    <h4 class="text-muted font-18 mt-4">New Password</h4>
                </div>

                <div class="p-2">
                    <form class="form-horizontal m-t-20" method="post" action="{{ route('forget.new-password') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $user->verification }}">
                        <div class="form-group row @error('email') has-danger @enderror">
                            <div class="col-12">
                                <input class="form-control" type="email" name="email" value="{{ $user->email }}" required="" placeholder="Email"
                                       readonly>
                                @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group row password-strength @error('password') has-danger @enderror">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input class="password-strength__input form-control" name="password" type="password"
                                           id="password-input" aria-describedby="passwordHelp"
                                           placeholder="Enter password"/>
                                    <div class="input-group-append">
                                        <button class="password-strength__visibility btn btn-outline-secondary"
                                                type="button"><span class="password-strength__visibility-icon"
                                                                    data-visible="hidden"><i
                                                    class="fas fa-eye-slash"></i></span><span
                                                class="password-strength__visibility-icon js-hidden"
                                                data-visible="visible"><i class="fas fa-eye"></i></span></button>
                                    </div>
                                </div>
                                <small class="password-strength__error text-danger js-hidden">This symbol is not
                                    allowed!</small>
                                @error('password') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="password-strength__bar-block progress mb-4">
                            <div class="password-strength__bar progress-bar bg-danger" role="progressbar"
                                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="form-group row @error('password_confirmation') has-danger @enderror">
                            <div class="col-12">
                                <input class="form-control" name="password_confirmation" type="password" required=""
                                       placeholder="Confirm Password">
                                @error('password_confirmation') <span
                                    class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group text-center row m-t-20">
                            <div class="col-12">
                                <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Change Password</button>
                            </div>
                        </div>

                    </form>
                    <!-- end form -->
                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('general/js/password-strength.js') }}"></script>
@endsection
