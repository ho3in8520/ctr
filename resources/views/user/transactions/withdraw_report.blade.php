@extends('templates.user.master_page')
@section('title_browser')
    Withdraw Report
@endsection
@section('content')

    <section id="basic-form-layouts">
        <div class="row pt-3">
            <h4>Transactions List</h4>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="get" action="">
                            @csrf
                            <div class="row mt-4">
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="type">Transaction Type</label>
                                    <select id="type" name='type' class="form-select form-control">
                                        <option value="">all</option>
                                        <option value="3" {{ request()->type=='3'?'selected':'' }}>withdraw</option>
                                        <option value="4" {{ request()->type=='4'?'selected':'' }}>deposit</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="unit">unit</label>
                                    <select id="unit" name="unit" class="form-select form-control">
                                        <option value="">all</option>
                                        <option value="usdt" {{ request()->unit=='usdt'?'selected':'' }}>
                                            usdt
                                        </option>
                                        <option value="ctr" {{ request()->unit=='ctr'?'selected':'' }} >ctr
                                        </option>
                                        <option value="btt" {{ request()->unit=='btt'?'selected':'' }} >btt
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label>amount</label>
                                    <div class="input-group">
                                        <input class="form-control" name="amount_from" placeholder="from"
                                               value="{{ request()->amount_from?request()->amount_from:'' }}">
                                        <input class="form-control" name="amount_to" placeholder="to"
                                               value="{{ request()->amount_to?request()->amount_to:'' }}">
                                    </div>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label>date</label>
                                    <div class="input-group">
                                        <input class="form-control datePicker" name="date_from" placeholder="from"
                                               value="{{ request()->date_from?request()->date_from:'' }}"
                                               readonly>
                                        <input class="form-control datePicker" name="date_to" placeholder="to"
                                               value="{{ request()->date_to?request()->date_to:'' }}"
                                               readonly>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label>destination address</label>
                                    <div class="input-group">
                                        <input class="form-control" name="destination_address"
                                               value="{{ request()->destination_address?request()->destination_address:'' }}">
                                    </div>
                                </div>
                                <div class="form-group col-12">
                                    <button type="button" class="btn btn-info search-ajax">Search</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped text-center table-bordered">
                                <thead>
                                <tr>
                                    <td>#</td>
                                    <td>unit</td>
                                    <td>amount</td>
                                    <td>tracking code</td>
                                    <td>trans type</td>
                                    <td>description</td>
                                    <td>destination address</td>
                                    <td>date</td>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($withdraws) > 0)
                                    @foreach($withdraws as $withdraw)
                                        @php
                                            $class=[1=>'badge-danger',2=>'badge-success'];
                                                $type=[1=>'Decrease',2=>'Increase']
                                        @endphp
                                        <tr>
                                            <td>{{ index($withdraws,$loop) }}</td>
                                            @if ($withdraw->financeable instanceof \App\Models\Asset)
                                                <td>{{ $withdraw->financeable->unit }}</td>
                                            @elseif($withdraw->financeable instanceof \App\Models\Invest_Shares)
                                                @if ($withdraw->extra_field1 == 5)
                                                    <td>ctr</td>
                                                @elseif($withdraw->extra_field1 == 6)
                                                    <td>btt</td>
                                                @else
                                                    <td>usdt</td>
                                                @endif
                                            @else
                                                <td>-</td>
                                            @endif
                                            <td>{{ $withdraw->amount }}</td>
                                            <td>
                                                @if($withdraw->tracking_code)
                                                    <input id="tracking_{{$withdraw->id}}" type="hidden"
                                                           class="form-control"
                                                           value="{{$withdraw->tracking_code}}" disabled>
                                                    <button type="button" data-copy="tracking_{{$withdraw->id}}"
                                                            class="btn btn-sm btn-primary copy-btn"><i
                                                            class="fa fa-copy"></i></button>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                            <span
                                                class="badge {{ $class[$withdraw->type]}}">{{ $type[$withdraw->type] }}</span>
                                                <p>{{$withdraw->transact_type}}</p>
                                            </td>
                                            <td>
                                                {{$withdraw->description}}
                                            </td>
                                            <td>
                                                @if($withdraw->extra_field1)
                                                    <input id="destination_{{$withdraw->id}}" type="hidden"
                                                           value="{{$withdraw->extra_field1}}">
                                                    <span class="font-12 mr-2">{{$withdraw->extra_field1}}</span>
                                                    <button type="button" data-copy="destination_{{$withdraw->id}}"
                                                            class="btn btn-sm btn-info copy-btn"
                                                            data-destination="{{$withdraw->extra_field1}}"><i
                                                            class="fa fa-copy"></i></button>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                {{ \Illuminate\Support\Carbon::parse($withdraw->created_at)->format("Y-m-d") }}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <th colspan="7">Nothing found!</th>
                                @endif
                                </tbody>

                            </table>
                            {!! $withdraws->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('general/js/popup.js') }}"></script>
    <script>
        $(document).on('click', ".copy-btn", function () {
            let elm = document.getElementById($(this).data('copy'));
            if (copyToClipboard(elm) == true) {
                swal('The text was successfully copied', '', 'success');
            }
        });
    </script>


@endsection
