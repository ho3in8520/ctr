@extends('templates.user.master_page')
@section('title_browser')
    dashboard
@endsection
@section('style')
    <style>
        .list-feature {
            padding-left: 0px;
        }

        .list-feature li {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            list-style: none;
            padding-top: 12px;
        }
        .swal-modal .swal-text {
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <!-- start Share -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">Share</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>

    <div class="row">
        @if ($share)
            <div class="col-xl-4 col-md-6 position-relative">
                <div class="card bg-white mini-stat text-dark">
                    <div class="p-3 mini-stat-desc">
                        <div class="clearfix text-info">
                            <h6 class="mt-0 float-left text-dark-50">Initial value</h6>
                            <h4 class="mb-3 mt-0 float-right">{{ $share->initial_value }} <sub
                                    style="font-size: .5em;"> USDT</sub></h4>
                        </div>
                        <div>
                            <ul class="list-feature">
                                <li>
                                    <span class="text-dark">Your profit</span>
                                    <span
                                        class="text-success font-weight-bold"> +{{ $share->profit }} btt </span>
                                </li>
                                <li>
                                    <span class="text-dark">Created at</span>
                                    <span class="text-secondary">{{ $share->created_at }}</span>
                                </li>
                                <li>
                                    <span class="text-dark">Last update</span>
                                    <span class="text-secondary">{{ $share->updated_at }}</span>
                                </li>
                            </ul>

                        </div>

                    </div>
                    <div class="btn-group btn-group-justified col-12 px-0 row mx-auto">
                        <a href="{{ route('user.shares.logs',$share) }}"
                           class="font-14 m-0 btn btn-info col-12">Logs</a>
                    </div>
                </div>
            </div>
        @else
            <div class="col-12 alert alert-primary">
                you dont have any shares
                <a href="{{ route('user.shares.create') }}" class="btn btn-primary btn-sm mx-2">Buy new shares</a>
            </div>
        @endif
    </div>
    <!--## end Share -->


    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">Invest</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        @if (count($invests) > 0)
            @foreach($invests as $key=>$item)
                <div class="col-xl-4 col-md-6 position-relative">
                    <div class="card bg-white mini-stat text-dark">
                        <div class="p-3 mini-stat-desc">
                            <div class="clearfix text-primary">
                                <h6 class="mt-0 float-left text-dark-50">Initial value</h6>
                                <h4 class="mb-3 mt-0 float-right">{{ $item->initial_value }} <sub
                                        style="font-size: .5em;"> USDT</sub></h4>
                            </div>
                            <div>
                                <ul class="list-feature">
                                    <li>
                                        <span class="text-dark">Your profit</span>
                                        <span
                                            class="text-success font-weight-bold"> +{{ $item->profit }} ctr </span>
                                    </li>
                                    <li>
                                        <span class="text-dark">Created at</span>
                                        <span class="text-secondary">{{ $item->created_at }}</span>
                                    </li>
                                    <li>
                                        <span class="text-dark">Last update</span>
                                        <span class="text-secondary">{{ $item->updated_at }}</span>
                                    </li>
                                    <li>
                                        <span>Cancellation percentage</span>
                                        <span class="text-secondary">{{ intval($cancel_invest['percent'])  }}%</span>
                                    </li>
                                    @if ($cancel_invest['date'] !== true && $cancel_invest['date'] != false)
                                        <li>
                                            <span>Next Date Cancellation <sub> (without damage)</sub></span>
                                            <span class="text-secondary">{{ $cancel_invest['date']  }}</span>
                                        </li>
                                    @endif
                                </ul>

                            </div>

                        </div>
                        <div class="btn-group btn-group-justified col-12 px-0 row mx-auto">
                            <a href="{{ route('user.invest.add_to_share',['invest_shares'=>$item->id]) }}"
                               class="font-14 m-0 btn btn-success col-sm-4 confirm-box" data-val="{{ $item->initial_value }}">Add To Share</a>
                            <a href="{{ route('user.invest.cancel',['invest_shares'=>$item->id]) }}"
                               class="font-14 m-0 btn btn-danger col-sm-4 confirm-box" data-val="{{ $item->initial_value }}">Cancel</a>
                            <a href="{{ route('user.invest.logs',$item) }}"
                               class="font-14 m-0 btn btn-info col-sm-4">Logs</a>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-12 alert alert-primary">
                you dont have any invests
                <a href="{{ route('user.invest.create') }}" class="btn btn-primary btn-sm mx-2">Buy new invest</a>
            </div>
        @endif
    </div>
    <!-- end row -->
@endsection
@section('script')
    <script>
        $(".confirm-box").click(function (event) {
            event.preventDefault();
            let confirm_box = $(this);
            let cancellation_fee= '{{ $cancel_invest['date'] === true?0:$cancel_invest['percent'] }}';
            let val= $(this).data('val');
            let cancellation_price= ((100-cancellation_fee)/100)/val;
            let text= 'It is not possible to return after cancellation Or Add To Share! \n\r';
            text += `percent of cancellation: ${cancellation_fee}% \n\r`
            text += `total price of cancellation: ${cancellation_price.toFixed(4)} usdt \n\r`;
            swal({
                title: "Are You Sure?",
                text: text,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete == true) {
                    window.location.replace(confirm_box.attr('href'));
                }
            });
        })
    </script>
@endsection
