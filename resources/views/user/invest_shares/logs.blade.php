@extends('templates.user.master_page')
@section('title_browser')
    {{ ucfirst($type) }} Transactions Logs
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-12">
                        <h4 class="page-title m-0">Your {{ ucfirst($type) }} Transactions Logs</h4>
                        <a class="btn btn-info float-right"
                           href="{{ route("user.{$type}.index") }}">Your {{ ucfirst($type) }}</a>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="content">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Amount</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (count($invest_shares_log) > 0)
                                    @foreach($invest_shares_log as $key=>$item)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>
                                                @if ($item->type == 1)
                                                    <span class="badge badge-danger">Decrease</span>
                                                @else
                                                    <span class="badge badge-success">Increase</span>
                                                @endif
                                            </td>
                                            <td>{{ $item->amount}}</td>
                                            <td>{{ $item->description}}</td>
                                            <td>{{ $item->created_at}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7" class="text-center">No row</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection
