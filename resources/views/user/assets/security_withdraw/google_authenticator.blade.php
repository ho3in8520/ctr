<form method="post" action="">
    @csrf
    <input type="hidden" value="{{ $token }}" name="token">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="input-group">
                <label class="m-auto">Authenticator code
                    :</label>
                    <input type="text" class="form-control text-right float-right"
                           id="iconLeft4" name="one_time_password" maxlength="6">
            </div>
        </div>
        <div class="col-md-7 row justify-content-center mt-1">
            <button class="btn btn-success button-verify"
                    type="button">submit
            </button>
        </div>
    </div>
</form>
