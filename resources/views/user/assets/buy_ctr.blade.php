@extends('templates.user.master_page')
@section('title_browser')
    Buy CTR
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-12">
                        <h4 class="page-title m-0">Buy CTR</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>

    <!-- Form To Buy Share -->
    <div class="row">
        <div class="card col-12">
            <div class="card-body">
                <form class="form row" method="post" action="{{ route("user.buy-ctr.store") }}">
                    @csrf
                    <div class="form-group col-sm-6">
                        <label>Amount<sub> Price : {{ $base_data->extra_field1 }} usdt</sub></label>
                        <div class="input-group">
                            <input type="text" name="amount" class="form-control" min="1">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-info all-amount">$</button>
                            </div>
                        </div>
                        <span class="text-danger error"></span>
                    </div>
                    <div class="form-group col-sm-6">
                        <label>USDT <sub>Available : {{ $usdt->amount }}</sub></label>
                        <input type="text" class="form-control usdt-input" value="0" disabled>
                    </div>
                    <div class="form-group col-12">
                        <button type="submit" class="btn btn-secondary ajaxStore buy" disabled>Buy</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- Form To Buy Share -->
@endsection
@section('script')
    <script>
        var ctr_price = '{{ $base_data->extra_field1 }}';
        var inventory = '{{ $usdt->amount }}';
        $("input[name='amount']").keyup(function () {
            let val = $(this).val();
            let ctr_to_usdt = val * ctr_price;
            $(this).siblings(".error").addClass('d-none');
            $(".usdt-input").val("0");
            $(".buy").removeClass('btn-info').addClass('btn-secondary').attr('disabled', 'disabled');
            if (!val)
                return
            if (val < 1) {
                $(this).siblings(".error").text('min amount: 1').removeClass('d-none');
                return false;
            }
            if (ctr_to_usdt > inventory) { // موجود خرید اون مقدار داشته باشیم
                $(this).siblings(".error").text('not enough inventory!').removeClass('d-none');
                return false;
            }
            $(".buy").addClass('btn-info').removeClass('btn-secondary').removeAttr('disabled');
            $(".usdt-input").val(ctr_to_usdt);
            $(".buy").removeClass('disabled');

        });
        $(".all-amount").click(function () {
            let all_amount = inventory/ctr_price;
            $("input[name='amount']").val(all_amount).keyup();
        })
    </script>
@endsection
