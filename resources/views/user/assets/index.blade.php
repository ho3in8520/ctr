@extends('templates.user.master_page')
@section('title_browser')
    dashboard
@endsection
@section('style')
    <style>
        .table thead th {
            text-align: center !important;
        }

        .table tbody td {
            text-align: center !important;
        }

        .table tbody td:first-child {
            text-align: left !important;
        }

        .table tbody td:first-child div {
            display: inline-grid;
        }

        .table tbody td p {
            line-height: 0rem;
        }

        .table tbody td small {
            display: block;
        }

        .bd-example-modal-lg .modal-dialog {
            display: table;
            position: relative;
            margin: 0 auto;
            top: calc(50% - 24px);
        }

        .bd-example-modal-lg .modal-dialog .modal-content {
            background-color: transparent;
            border: none;
        }
    </style>
    <style>
        :root {
            --animate-duration: 5s;
        }

        @-moz-keyframes spin {
            100% {
                -moz-transform: rotateY(360deg);
            }
        }

        @-webkit-keyframes spin {
            100% {
                -webkit-transform: rotateY(360deg);
            }
        }

        @keyframes spin {
            100% {
                -webkit-transform: rotateY(360deg);
                transform: rotateY(360deg);
            }
        }

        .animate__flipOutY {
            -webkit-animation: spin 6s linear infinite;
            -moz-animation: spin 6s linear infinite;
            animation: spin 6s linear infinite;
            /*animation: flipOutY 5s linear 0s infinite;*/
            /*-webkit-animation-duration: calc(1s * 0.75);*/
            /*animation-duration: calc(1s * 0.75);*/
            /*-webkit-animation-duration: calc(var(--animate-duration) * 0.75);*/
            /*animation-duration: calc(var(--animate-duration) * 0.75);*/
            /*-webkit-backface-visibility: visible !important;*/
            /*backface-visibility: visible !important;*/
            /*-webkit-animation-name: flipOutY;*/
            /*animation-name: flipOutY;*/
            /*animation-iteration-count: infinite;*/
            /*animation-timing-function: linear;*/
        }

        .table thead th,
        .table tbody td {
            text-align: center !important;
        }

        .table tbody td:first-child div {
            display: inline-grid;
        }

        .table tbody td p {
            line-height: 0rem;
        }

        .table tbody td small {
            display: block;
        }


        @media only screen and (min-width: 600px) {
            .mt-5 {
                margin-top: 4.3rem !important;
            }

        }

    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">Assets</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="content">
                        <section id="data-list-view" class="data-list-view-header">
                            <input type="hidden" class="address" value="{{ $assets[0]->addressHex }}">
                            <input type="hidden" class="action" value="{{ route('assets.get-amount-wallet') }}">
                            <input type="hidden" class="token" value="{{ csrf_token() }}">
                            <!-- DataTable starts -->

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Unit</th>
                                        <th>Asset</th>
                                        <th>Operation</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($assets as $item)
                                        <tr class="unit" data-unit="{{ $item->unit }}">
                                            <td><img src="{{asset('theme/user/assets/icon/'.$item->name.'.png')}}"
                                                     width="40" height="40">
                                                <div>
                                                    <span>{{ $item->name }}</span>
                                                </div>
                                            </td>
                                            <td>{{ strtoupper($item->unit) }}</td>
                                            <td>
                                                <span
                                                    class="amount">{{ $item->amount }} {{ strtoupper($item->unit) }}</span><br>
                                                @if($item->unit=='ctr')
                                                    <small>${{ $currency_prices->ctr*$item->amount }}</small>
                                                @elseif($item->unit!='usdt')
                                                    <small>${{ $currency_prices->currency_to_usdt($item->unit)*$item->amount }}</small>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->unit == 'usdt')
                                                    <button type="button"
                                                            class="btn btn-success mr-1 mb-1 receive"
                                                            data-type="{{ $item->unit }}">Receive
                                                    </button>
                                                @endif
                                                <button type="button" data-trc="{{ $item->type_token }}"
                                                        data-amount="{{ $item->amount }}" data-type="{{ $item->unit }}"
                                                        class="btn btn-danger mr-1 mb-1 withdraw">Withdraw
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- DataTable ends -->

                        </section>
                        <!-- Data list view end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->
    <div class="modal fade text-left receive-modal" id="backdrop" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel4"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel4">Address Wallet</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-body-receive">

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade text-left modal-withdraw" id="staticBackdrop" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">withdraw</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                    @csrf
                    <div class="modal-body body-withdraw">
                        <div class="text-center loading-page" style="display: block">
                            <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">
                                <span class="sr-only">Please Wait...</span>
                            </div>
                            <h4>Please Wait</h4>
                        </div>
                        <div class="row justify-content-center withdraw-content" style="display: none">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="card border-secondary">
                                                    <div class="card-body modal-body-logo-unit text-center">
                                                        <img src="" width="100" class="animate__flipOutY">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card border-secondary">
                                                    <div class="card-body">
                                                        <p class="text-info">unit : <span
                                                                class="font-weight-bold modal-body-info-unit"></span>
                                                        </p>
                                                        <p class="text-success">your wealth : <span
                                                                class="font-weight-bold modal-body-info-amount">0</span>
                                                        </p>
                                                        <p class="text-danger">fee : <span
                                                                class="font-weight-bold modal-body-info-fee">0</span>
                                                        </p>
                                                        <p class="text-warning">Minimum withdrawal : <span
                                                                class="font-weight-bold modal-body-info-minimum-withdrawal">0</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>address</label>
                                <input class="form-control" name="address">
                            </div>
                            <div class="col-md-6">
                                <label>amount</label>
                                <div class="input-group">
                                    <input class="form-control" name="amount">
                                    <div class="input-group-append">
                                        <a class="btn btn-outline-secondary all-amount" title="total"
                                           id="button-addon2"><i
                                                class="fa fa-dollar-sign"></i> </a>
                                    </div>
                                </div>
                                <p class="text-primary">Final amount : <span
                                        class="font-weight-bold modal-body-result-fee">0</span></p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">cancel</button>
                            @if(auth('web')->user()->google2fa_secret)
                            <button type="button" class="btn btn-primary withdraw-submit">submit</button>
                            @else
                            <a href="{{ route('user.settings') }}" class="btn btn-primary">Enable Authenticator</a>
                                @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade text-left security-modal" id="staticBackdrop" data-backdrop="static" tabindex="-1"
         role="dialog"
         aria-labelledby="myModalLabel4"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel4">Confirm the transaction</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body body-security">

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {

            $(document).on('click', '.receive', function () {
                $(".modal-body-receive").html('<div class="text-center">\n' +
                    '                        <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">\n' +
                    '                            <span class="sr-only">Loading...</span>\n' +
                    '                        </div>\n' +
                    '                        <h4>Please Wait</h4>\n' +
                    '                    </div>');
                $(".receive-modal").modal("show");
                var type = $(this).data('type');
                $.post(`{{ route('assets.get-address-wallet') }}`, {
                    type: type,
                    _token: `{{ csrf_token() }}`
                }).done(response => {
                    $(".modal-body-receive").html(response)
                }).fail(error => {
                    swal('error', 'Invalid request', 'error')
                });
            })
            $(document).on('click', '.copy-address', function () {
                var val = $(this).closest(".input-group").find("input").val();
                copy()
            })
            $(document).on('click', '.withdraw', function () {
                var elem = $(this);
                var trc = $(this).data('trc');
                var unit = $(this).data('type');
                var amount = $(this).data('amount');
                var security = $(this).data('security');
                var fee = 0;
                $.post(`{{ route('assets.wallet.withdrawal-fee') }}`, {
                    '_token': `{{ csrf_token() }}`,
                    'unit': unit
                }, function (response) {
                    if (response.status == 100) {
                        fee = response.data['fee']
                        min = response.data['min']
                        feeUnit = response.data['fee']
                        $(".body-withdraw").append("<input type='hidden' value='" + trc + "' name='trc'><input type='hidden' value='" + unit + "' name='unit'>");
                        $(".modal-body-logo-unit").find("img").attr('src', logo_unit(unit));
                        $(".modal-body-info-unit").html(unit.toUpperCase());
                        $(".modal-body-info-amount").html(amount);
                        $(".modal-body-info-fee").html(fee);
                        $(".modal-body-info-minimum-withdrawal").html(min);
                        $(".loading-page").hide();
                        $(".withdraw-content").show();
                    }
                })

                $(".modal-withdraw").modal("show");
                $(document).on('click', '.all-amount', function () {
                    if (amount > fee) {
                        $(this).closest('.input-group').find("input").val(amount)
                        $(".modal-body-result-fee").html(numberFormat(amount - feeUnit));
                    } else {
                        $(this).closest('.input-group').find("input").val(0)
                        $(".modal-body-result-fee").html(0);
                    }
                });
            });
            $(document).on('click', '.withdraw-submit', function () {
                var elem = this;
                var unit = $(this).closest('.modal-content').find("input[name='unit']").val();
                var amount = $(this).closest('.modal-content').find("input[name='amount']").val();
                var address = $(this).closest('.modal-content').find("input[name='address']").val();
                withdraw(unit, amount, address, elem)
            });
            $(document).on('click', '.withdraw-store', function () {
                var data = new FormData($(this).closest('form')[0]);

                $.ajax({
                    url: `{{ route('assets.withdraw.store') }}`,
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == 100) {
                            swalResponse(response.status, response.msg, 'success')
                            setTimeout(function () {
                                window.location.reload()
                            }, 3000)
                        } else {
                            swalResponse(response.status, response.msg, 'error')
                            return false;
                        }
                    }, error: function (xhr) {
                        swalResponse(500, 'Invalid request', 'error')
                    },
                })
            });
            $(document).on('keyup', "input[name='amount']", function () {
                if (($(this).val() - feeUnit) > 0) {
                    $(".modal-body-result-fee").html(numberFormat($(this).val() - feeUnit));
                } else {
                    $(".modal-body-result-fee").html(0);
                }
            });
            $(document).on("click", '.button-verify', function () {
                var elem = $(this);
                var text_btn = elem.text();
                $(".body-security .alert-danger").remove()
                elem.text('Pending ...');
                var data = new FormData($(this).closest('form')[0]);

                $.ajax({
                    url: `{{ route('withdraw.store') }}`,
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == 100) {
                            swalResponse(response.status, response.msg, 'success')
                            setTimeout(function () {
                                window.location.reload()
                            }, 3000)
                        } else {
                            swalResponse(response.status, response.msg, 'error')
                            elem.text(text_btn);
                            return false;
                        }
                    }, error: function (xhr) {
                        var error = '<div class="alert alert-danger"><ul>';
                        $.map(xhr.responseJSON, function (v, i) {
                            $.map(v, function (val, key) {
                                error += '<li>' + val + '</li>'
                            })
                        })
                        error += '</ul></div>'
                        $(".body-security").prepend(error)
                        elem.text(text_btn);
                        swalResponse(500, 'Solve form problems', 'Error')
                    },
                })
            })
        })

        function logo_unit(unit) {
            var logo = '';
            switch (unit) {
                case 'ctr':
                    logo = `{{ asset('theme/user/assets/icon/CoinTread.png') }}`;
                    break;
                case 'usdt':
                    logo = `{{ asset('theme/user/assets/icon/USDT.png') }}`;
                    break;
                case 'trx':
                    logo = `{{ asset('theme/user/assets/icon/Tron.png') }}`;
                    break;
                case 'btt':
                    logo = `{{ asset('theme/user/assets/icon/BitTorrent.png') }}`
                    break;
            }
            return logo;
        }

        function withdraw(unit, amount, address, elem) {
            swal({
                title: "Ensuring the withdrawal of assets?",
                icon: "warning",
                buttons: ['cancel', 'submit'],
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $(elem).text('Pending ...');
                        var data = new FormData($(elem).closest('form')[0]);

                        $.ajax({
                            url: `{{ route('assets.withdraw.security') }}`,
                            type: 'POST',
                            data: data,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                if (response.status) {
                                    swalResponse(response.status, response.msg, 'error')
                                    $(elem).text('submit');
                                    return false;
                                }
                                var form = response
                                form = $(form).append("<input type='hidden' value='" + amount + "' name='amount'><input type='hidden' value='" + address + "' name='address'><input type='hidden' value='" + unit + "' name='unit'>")
                                $(".body-security").html(form)
                                $(".security-modal").modal('show');
                                $(".modal-withdraw").modal('hide');
                                $(elem).text('submit');
                            }, error: function (xhr) {
                                $(elem).text('submit');
                                errorForms(xhr);
                            },
                        })
                    }
                });
        }

        function copy() {
            /* Get the text field */
            var copyText = document.getElementById("address");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            /* Copy the text inside the text field */
            document.execCommand("copy");

            /* Alert the copied text */
            swal('success', 'Copied successfully', 'success')
        }

    </script>
@endsection
