@extends('templates.landing.master_page')

@section('content')
    <!--start home area-->
    <section id="home-area" class="bg-1" data-scroll-index="0">
        <div class="container">
            <div class="row">
                <!--start caption-->
                <div class="col-lg-8">
                    <div class="caption d-table">
                        <div class="d-table-cell align-middle">
                            <h3 class="text-white">We work hard for your convenience and increase your profits and capital.
                                You will soon see great and exciting changes in this project.
                                Stay tuned</h3>
                            <h3 class="text-white" >The CTR cryptocurrency Company is one of the best and most commercial, brilliantly successful companies in the referral and investment industry.</h3>
                            <h4 class="text-light font-open-sans" style="color: #fbed50 !important;font-weight: bold" id="changeText"></h4>
                            <div class="caption-download-btns">
                                <ul>
                                    <li><a href=""><i class="icofont-telegram"></i></a></li>
                                    <li><a href=""><i class="icofont-instagram"></i></a></li>
                                    <li><a href=""><i class="icofont-email"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end caption-->
            </div>
        </div>
    </section>
    <!--start home area-->
    <!--start core feature area-->
    <section id="core-feature-area" class="bg-1" data-scroll-index="1">
        <div class="container">
            <div class="row">
                <!--start section heading-->
                <div class="col-md-10 offset-md-1">
                    <div class="section-heading text-center">
                        <h5>Compensation Plans</h5>
                        <h2>We have plans for everyone. Check out which one fits you.</h2>
                    </div>
                </div>
                <!--end section heading-->
            </div>
            <div class="row">
                <!--start core feature single-->
                <div class="col-lg-3 col-md-6">
                    <div class="core-feature-single text-center">
                        <div class="icon">
                            <img src="{{asset('theme/landing/images/Crash_Team_Racing.png')}}" height="70" style="margin-top: -8px" />
                        </div>
                        <h3>The CTR coin</h3>
                        <p class=""> CTR is used for free-cost financial transaction that is available for large companies around the world without any restrictions, especially in developing countries.</p>
                    </div>
                </div>
                <!--end core feature single-->
                <!--start core feature single-->
                <div class="col-lg-3 col-md-6">
                    <div class="core-feature-single two text-center" style="height: 432px !important;">
                        <div class="icon">
                            <i class="icofont-money"></i>
                        </div>
                        <h3>Blue Diamond Plan</h3>
                        <p class="">{!! \Illuminate\Support\Str::limit('You can make 3% to 20% profit daily up to the step of fetival. ',180) !!}</p>
                    </div>
                </div>
                <!--end core feature single-->
                <!--start core feature single-->
                <div class="col-lg-3 col-md-6">
                    <a href="#">
                        <div class="core-feature-single four text-center" style="height: 432px !important;">
                            <div class="icon four">
                                <i class="icofont-users-alt-3"></i>
                            </div>
                            <h3>Referral Marketing</h3>
                            <p>{!! \Illuminate\Support\Str::limit('This profitable compensation plan is to make money through referral marketing. By introducing other people the first income generation plan, you will receive 20% commission in CTR buying plan and 5% in festival plan(investment) for each person who enters.',130) !!}</p>
                        </div>
                    </a>
                </div>
                <!--end core feature single-->
                <!--start core feature single-->
                <div class="col-lg-3 col-md-6">
                    <div class="core-feature-single three text-center" style="height: 432px !important;">
                        <div class="icon three">
                            <i class="icofont-prestashop"></i>
                        </div>
                        <h3>Buy CTR </h3>
                        <p class="">{!! \Illuminate\Support\Str::limit('50 million of CTR coin have already been generated; 20 million CTR coin have been offered at a price of 3 USDT.
                            With the outlook we have for this currency, it is predictable that the price of the currency will reach $100 by next year.',180) !!}</p>
                    </div>
                </div>
                <!--end core feature single-->

            </div>
            <div class="row">
                <!--start read more button-->
                <div class="col-lg-12">
                    <div class="load-more-btn text-center">
                        <a href="{{route('user.dashboard')}}">Let's Start Now</a>
                    </div>
                </div>
                <!--end read more button-->
            </div>
        </div>
    </section>
    <!--end core feature area-->
    <!--start about area-->
    <section id="about-area" class="bg-1" data-scroll-index="2">
        <div class="container">
            <div class="row">
                <!--start about content-->
                <div class="col-md-6">
                    <div class="about-cont">
                        <h2 >CTR Long Term Ambitions</h2>
                        <p class="text-justify">Crypto currencies will become worldwide the standard “storage of value”, and will replace current methods to store value (fiat money, bank accounts, pension funds, precious metals, derivates, …). Because of the laws of Supply and Demand, the value of crypto currencies will increase fast.</p>
                        <p>CTR project is using the leverage of Referral Marketing and CTR Sales to increase the value of our currency stocks.</p>
                    </div>
                    <div class="about-info row">
                        <div class="col-lg-12">
                            <div class="load-more-btn text-center">
                                <a href="#">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end about content-->
            </div>
        </div>
        <!--start app mocup-->
<!--        <div class="about-app-mocup">
            <a href="https://btfprofits.com" target="_blank" data-toggle="tooltip" data-placement="left" title="BTFprofits.com"><img src="{{asset('theme/landing/images/btf.png')}}" style="width: 600px"  class="img-fluid" alt=""></a>
        </div>-->
        <!--end app mocup-->
    </section>
    <!--end about area-->
    <!--start video area-->
    <section id="video-area" class="bg-1">
        <div class="video-cont d-table text-center" style="margin: 200px auto !important;">
            <div class="d-table-cell align-middle">
                <div class="video-overlay"></div>
                <a class="popup-video goclick" onclick="fclick()"  data-toggle="modal" data-target="#myModal" ><i class="icofont-ui-play"></i></a>
            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Videos</h4>
                        </div>
                        <div class="modal-body">
                            <h5>Blue Diamond</h5>
                            <video controls id="video1" style="width: 50%; height: auto; margin:0 auto; frameborder:0;">
                                <source src="{{asset('theme/landing/videos/Blue Diamond.mp4')}}" type="video/mp4">
                                Your browser doesn't support HTML5 video tag.
                            </video>
                            <hr/>
                            <h5>Know about CTR main plans</h5>
                            <video controls id="video1" style="width: 50%; height: auto; margin:0 auto; frameborder:0;">
                                <source src="{{asset('theme/landing/videos/Know about CTR main plans.mp4')}}" type="video/mp4">                            Your browser doesn't support HTML5 video tag.
                                Your browser doesn't support HTML5 video tag.
                            </video>
                            <hr/>
                            <h5>Registration and Buying CTR guidance</h5>
                            <video controls id="video1" style="width: 50%; height: auto; margin:0 auto; frameborder:0;">
                                <source src="{{asset('theme/landing/videos/Registration and Buying CTR guidance.mp4')}}" type="video/mp4">                            Your browser doesn't support HTML5 video tag.
                                Your browser doesn't support HTML5 video tag.
                            </video>
                            <hr/>
                            <h5>Blue Diamond Plan Instruction</h5>
                            <video controls id="video1" style="width: 50%; height: auto; margin:0 auto; frameborder:0;">
                                <source src="{{asset('theme/landing/videos/Blue Diamond Instruction.mp4')}}" type="video/mp4">                            Your browser doesn't support HTML5 video tag.
                            </video>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="video title">
                        <h2>No boundaries for CTR</h2>
                        <p class="text-justify">The initial coin offering is restricted to 50,000,000. We believe the users should be able to transaction freely whenever they want so we made it possible here and no user is limited. We have our own plain and recognizable white paper, address, definite and explicit economical currency. The price is defined by users in all time unlocked TronLink wallet. It is noticeable that we have set agreement with great companies. All deals are allowed without any authentication or ID required. Features mentioned guaranty CTR’s successful mission. Obviously Just like Bitcoin, CTR will grow fast and conquer the world.
                            All crypto traders are aware of challenges they may encounter just sit back and contact our 24/7 professional customer service team in social media. We have the best solution for each problem you may face</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-8">
                    <div class="counter title" style="padding-top: 70px;">
                        <h5>Take a Look At This Video</h5>
                        <h2 class="text-white">Guidance</h2>
                        <p>We have uploaded a video to guide you how to sign in, buy CTR, invest and make money via referral marketing. Hope it is helpful.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 120px">
                <!--start counter single-->
                <div class="col-md-3 col-6">
                    <div class="counter-single">
                        <div class="icon">
                            <img src="{{asset('theme/landing/images/icon-like.png')}}" class="img-fluid" alt="">
                        </div>
                        <h3 style="font-weight: 600;color: #fff;">{{number_format(50000000)}}</h3>
                        <p style="color: #d2d7fc;text-transform: uppercase;font-size: 16px;">Total  Currency</p>
                    </div>
                </div>
                <!--end counter single-->
                <!--start counter single-->
                <div class="col-md-3 col-6">
                    <div class="counter-single">
                        <div class="icon">
                            <img src="{{asset('theme/landing/images/icon-user.png')}}" class="img-fluid" alt="">
                        </div>
                        <h3 style="font-weight: 600;color: #fff;">35276521</h3>
                        <p style="color: #d2d7fc;text-transform: uppercase;font-size: 16px;">Circulating Currency</p>
                    </div>
                </div>
                <!--end counter single-->
                <!--start counter single-->
                <div class="col-md-3 col-6">
                    <div class="counter-single">
                        <div class="icon">
                            <img src="{{asset('theme/landing/images/icon-cloud.png')}}" class="img-fluid" alt="">
                        </div>
                        <h3 style="font-weight: 600;color: #fff;">0.04$</h3>
                        <p style="color: #d2d7fc;text-transform: uppercase;font-size: 16px;">CTR Price</p>
                    </div>
                </div>
                <!--end counter single-->
                <!--start counter single-->
                <div class="col-md-3 col-6">
                    <div class="counter-single">
                        <div class="icon">
                            <img src="{{asset('theme/landing/images/icon-trophy.png')}}" class="img-fluid" alt="">
                        </div>
                        <h3 style="font-weight: 600;color: #fff;">1,411,061$</h3>
                        <p style="color: #d2d7fc;text-transform: uppercase;font-size: 16px;">Circulating  Value</p>
                    </div>
                </div>
                <!--end counter single-->
            </div>
        </div>
    </section>
    <!--end video area-->
    <!--start how work area-->
    <section id="how-work-area" style="margin-right: 165px">
        <div class="container">
            <div class="row">
                <!--start section heading-->
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-heading text-center">
                        <h2>CTR Roadmap</h2>
                        <p>Thanks for taking the time to check out CTR eye-catching roadmap from the inception to golden future.</p>
                    </div>
                </div>
                <!--end section heading-->
            </div>
        </div>
    </section>
    <!--end how work area-->
    >
    <!--screenshot area-->

    <!--end area-->
    <!--start pricing area-->
    <section id="pricing-area" data-scroll-index="3">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="section-heading">
                        <h2>Make Your Own Profit</h2>
                        <p class="text-justify">Many people have lost their jobs during the outbreak of corona virus under lockdown conditions, at the same time there were many who could make money from their spare room, only through their cell phones. You can also join the thousands of our users who are earning money investing in CTR and compensation plans.</p>
                        <p>Several examples of CTR users who have received their profit at the moment are displayed in the box opposite.</p>
                    </div>
                    <div class="toggle-container">
                        <div class="switch-toggles">
                            <div class="monthly active">Deposit</div>
                            <div class="yearly">Withdraw</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div id="pricing-wrap">
                        <!--start monthly pricing table-->
                        <div class="monthly active">
                            <div class="price-tbl-single highlighted">
                                <div class="table-inner text-center">
                                    <h4>The last deposits</h4>
                                    <ul>
                                        <li>{{'David : 10000 usdt'}}</li>
                                        <li>Ahmed : 5500 usdt</li>
                                        <li>Robert : 3250 usdt</li>
                                        <li>Dorothy : 125600 usdt</li>
                                        <li>Paul : 62300 usdt</li>
                                        <li>Kevin : 220000 usdt</li>
                                        <li>{{'Ali : 10000 usdt'}}</li>
                                        <li>Sharon : 630200 usdt</li>
                                        <li>Amy : 690000 usdt</li>
                                        <li>Brenda : 1000 usdt</li>
{{--                                                                                @foreach($last_deposits as $last_deposit)--}}
{{--                                                                                    <li>{{$last_deposit->name}}</li>--}}
{{--                                                                                @endforeach--}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--end monthly pricing table-->
                        <!--start monthly pricing table-->
                        <div class="yearly">
                            <div class="price-tbl-single highlighted">
                                <div class="table-inner text-center">
                                    <h4>The last Withdrawals</h4>
                                    <ul>
                                        <li>{{'Raymond : 22500 ctr'}}</li>
                                        <li>Benjamin : 18700 ctr</li>
                                        <li>Heather : 526666 ctr</li>
                                        <li>Jerry : 311200 ctr</li>
                                        <li>Leah : 2000 ctr</li>
                                        <li>Liese : 985000 ctr</li>
                                        <li>{{'Hossein : 301000 ctr'}}</li>
                                        <li>Sara : 523608 ctr</li>
                                        <li>Ahmed : 412000 ctr</li>
                                        <li>Dame  : 845203 ctr</li>
                                        {{--                                        @foreach($last_withdraws as $last_withdraw)--}}
                                        {{--                                            <li>{{$last_withdraw->name}}</li>--}}
                                        {{--                                        @endforeach--}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--end monthly pricing table-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end pricing area-->
    <!--start newsletter area-->
    <section id="newsletter-area" class="bg-1" data-scroll-index="4">
        <div class="container">
            <div class="row">
                <!--start section heading-->
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-heading text-center">
                        <h2 class="text-black" >Why us?</h2>
                        <p class=" text-justify" style="color: #000;font-weight: bold">The CTR chairman and founder gathered highly talented experts who are passionate about going steps ahead of the current wave.
                            Three main CTR recommendations for all traders like beginners, business people and great companies.
                            Beginners can choose us:
                            1- To make benefit via referral marketing
                            2- Buying CTR
                            3- Investment
                            In this process anyone with a minimum required balance of a specific CTR can validate transaction and staking rewards. Also, calculation profit simultaneously when you invest so then, Investment is the company’s main offer.
                            Business people:
                            Since the world has entered a new version of trading (crypto currency) if you want to vast your business you’re obliged to ride the latest wave striking the world. Good news is not one CTR has provided the platform for exporting importing and all transaction included, unlike banks doesn’t ask any fees for your transactions.
                            For those who want make their business International:
                            For any project you have in your mind CTR is the first and best answer can make your ideas decentralized financially and no limitation for your transaction. Make money in each point of the world.
                            Moreover you choose us because of:
                            1- Management, operational, physical and user’s security
                            2- Vitally important and standard ERC20 token
                            3- Running a Cron job every 10 minutes hence your data is totally safe
                            4- Hashed users password
                            5- Improved site performance and sped uploading times via cloud-flare</p>
                    </div>
                </div>
                <!--end section heading-->
            </div>
        </div>
    </section>
    <!--end newsletter area-->
    <!--start team area-->
    <section id="team-area" data-scroll-index="5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-heading text-center">
                        <h5>Our Creative Team</h5>
                        <h2>Meet The Team</h2>
                        <p class="text-justify">The CTR coin is started by a small team of unpaid volunteers. The number and composition of the CTR coin Core Team evolves in function of the project phase, skills needed and workload to be done. A major part of the work is done by CTR coin holders organized in the local Telegram Groups. End of 2019 we have more than 1000 members in our telegram groups, including members from different countries.</p>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-left: 100px">
                <!--start team single-->
                <div class="col-lg-2 col-md-6">
                    <div class="team-single text-center">
                        <div class="team-img">
                            <img src="{{asset('theme/landing/images/Manish-Agrawal-new.jpeg')}}" class="img-fluid" alt="">
                            <div class="team-social">
                                <ul>
                                    <li><a href="#"><i class="icofont-facebook"></i></a></li>
                                    <li><a href="#"><i class="icofont-twitter"></i></a></li>
                                    <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-info">
                            <h5>DR. Manish Agrawal</h5>
                            <p>business data communications</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="team-single text-center">
                        <div class="team-img">
                            <img src="{{asset('theme/landing/images/Orobosa-new.jpeg')}}" class="img-fluid" alt="">
                            <div class="team-social">
                                <ul>
                                    <li><a href="#"><i class="icofont-facebook"></i></a></li>
                                    <li><a href="#"><i class="icofont-twitter"></i></a></li>
                                    <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-info">
                            <h5>Dr. Orobosa A. Ihensekhien </h5>
                            <p>Economics</p>
                        </div>
                    </div>
                </div>
                <!--end team single-->
                <!--start team single-->
                <div class="col-lg-2 col-md-6">
                    <div class="team-single text-center">
                        <div class="team-img">
                            <img src="{{asset('theme/landing/images/Raghav-Rao-new.jpeg')}}" class="img-fluid" alt="" >
                            <div class="team-social">
                                <ul>
                                    <li><a href="#"><i class="icofont-facebook"></i></a></li>
                                    <li><a href="#"><i class="icofont-twitter"></i></a></li>
                                    <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-info">
                            <h5>DR. H. Raghav Rao, Ph.D</h5>
                            <p>Information Systems Frontiers</p>
                        </div>
                    </div>
                </div>
                <!--end team single-->
                <!--start team single-->
                <div class="col-lg-2 col-md-6">
                    <div class="team-single text-center">
                        <div class="team-img">
                            <img src="{{asset('theme/landing/images/toni-new.jpeg')}}" class="img-fluid" alt="">
                            <div class="team-social">
                                <ul>
                                    <li><a href="#"><i class="icofont-facebook"></i></a></li>
                                    <li><a href="#"><i class="icofont-twitter"></i></a></li>
                                    <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-info">
                            <h5>DR. Toni Somers</h5>
                            <p>Information Systems Management</p>
                        </div>
                    </div>
                </div>
                <!--end team single-->
                <!--start team single-->
                <div class="col-lg-2 col-md-6">
                    <div class="team-single text-center">
                        <div class="team-img">
                            <img src="{{asset('theme/landing/images/Yue-Cheong-new.jpeg')}}" class="img-fluid" alt="">
                            <div class="team-social">
                                <ul>
                                    <li><a href="#"><i class="icofont-facebook"></i></a></li>
                                    <li><a href="#"><i class="icofont-twitter"></i></a></li>
                                    <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-info">
                            <h5>DR. Yue-Cheong Chan</h5>
                            <p>Economics</p>
                        </div>
                    </div>
                </div>
                <!--end team single-->
            </div>
        </div>
    </section>
    <!--end team area-->
    <!--start testimonial area-->
    <section id="testimonial-area" data-scroll-index="6" style="margin-bottom: 400px">
        <div class="container">
            <div class="row">
                <!--start section heading-->
                <div class="col-md-8 offset-md-2">
                    <div class="section-heading text-center">
                        <h2>Who is CTR sponsored by?</h2>
                        <p >Hence we are committed to cover all mentioned visions which are all planed in details and supported by our experienced professional economist team.</p>
                    </div>
                </div>
                <!--end section heading-->
            </div>
            <div class="testi-wrap" >
                <!--start testimonial single-->
                <div class="client-single active position-1" data-position="position-1">
                    <div class="client-img">
                        <img src="{{asset('theme/landing/images/ctr2.png')}}" alt="">
                    </div>
                    <div class="client-comment">
                        <h4>The CTR coin is a great project proudly sponsored by some great environmentally friendly companies which have been industrialized and beneficial through the years like Saudi Environmental Services Company, Oman Environmental Services Holding Company S.A.O.C (be'ah), Pentacodes, Qatar Petroleum Company, The Egyptian Natural Gas Company (GASCO) and Western Auto Company. Transparency and great future outlook of CTR motivated them to play the role of sponsor of this excellent project.</h4>
                        <span><i class="icofont-quote-left"></i></span>
                    </div>
                </div>
                <!--end testimonial single-->
                <!--start testimonial single-->
                <div class="client-single inactive position-2" data-position="position-2">
                    <div class="client-img">
                        <img src="{{asset('theme/landing/images/Saudi Arabia.png')}}" alt="">
                    </div>
                    <div class="client-comment">
                        <h3>Installation was pretty easy.We have been Arribo customers for years, and we have had nothing but amazing experiences with the Arribo and well-designed mobile app. Arribo provided that for us with easy-to-use software and personalized support. I like this app. Thank you</h3>
                        <span><i class="icofont-quote-left"></i></span>
                    </div>
                    <div class="client-info">
                        <h3>Fatih Senel</h3>
                        <p>Digilite Web Solutions</p>
                    </div>
                </div>
                <!--end testimonial single-->
                <!--start testimonial single-->
                <div class="client-single inactive position-3" data-position="position-3">
                    <div class="client-img">
                        <img src="{{asset('theme/landing/images/GASCO.jpg')}}" alt="" width="250" height="250">
                    </div>
                    <div class="client-comment">
                        <h3>Installation was pretty easy.We have been Arribo customers for years, and we have had nothing but amazing experiences with the Arribo and well-designed mobile app. Arribo provided that for us with easy-to-use software and personalized support. I like this app. Thank you</h3>
                        <span><i class="icofont-quote-left"></i></span>
                    </div>
                    <div class="client-info">
                        <h3>Fatih Senel</h3>
                        <p>Digilite Web Solutions</p>
                    </div>
                </div>
                <!--end testimonial single-->
                <!--start testimonial single-->
                <div class="client-single inactive position-4" data-position="position-4">
                    <div class="client-img">
                        <img src="{{asset('theme/landing/images/Qatar.jpg')}}" alt="" width="150" height="150">
                    </div>
                    <div class="client-comment">
                        <h3>Installation was pretty easy.We have been Arribo customers for years, and we have had nothing but amazing experiences with the Arribo and well-designed mobile app. Arribo provided that for us with easy-to-use software and personalized support. I like this app. Thank you</h3>
                        <span><i class="icofont-quote-left"></i></span>
                    </div>
                    <div class="client-info">
                        <h3>Fatih Senel</h3>
                        <p>Digilite Web Solutions</p>
                    </div>
                </div>
                <!--end testimonial single-->
                <!--start testimonial single-->
                <div class="client-single inactive position-5" data-position="position-5" >
                    <div class="client-img" style="margin-top: 10px">
                        <img src="{{asset('theme/landing/images/KNPC.gif')}}" alt="" width="190" height="190">
                    </div>
                    <div class="client-comment">
                        <h3>Installation was pretty easy.We have been Arribo customers for years, and we have had nothing but amazing experiences with the Arribo and well-designed mobile app. Arribo provided that for us with easy-to-use software and personalized support. I like this app. Thank you</h3>
                        <span><i class="icofont-quote-left"></i></span>
                    </div>
                    <div class="client-info">
                        <h3>Fatih Senel</h3>
                        <p>Digilite Web Solutions</p>
                    </div>
                </div>
                <!--end testimonial single-->
                <!--start testimonial single-->
                <div class="client-single inactive position-6" data-position="position-6">
                    <div class="client-img">

                        <img src="{{asset('theme/landing/images/Pentacodes.jpg')}}" alt="">
                    </div>
                    <div class="client-comment">
                        <h3>Installation was pretty easy.We have been Arribo customers for years, and we have had nothing but amazing experiences with the Arribo and well-designed mobile app. Arribo provided that for us with easy-to-use software and personalized support. I like this app. Thank you</h3>
                        <span><i class="icofont-quote-left"></i></span>
                    </div>
                    <div class="client-info">
                        <h3>Fatih Senel</h3>
                        <p>Digilite Web Solutions</p>
                    </div>
                </div>
                <!--end testimonial single-->
                <!--start testimonial single-->
                <div class="client-single inactive position-7" data-position="position-7">
                    <div class="client-img">
                        <img src="{{asset('theme/landing/images/Oman.jpg')}}" alt="">
                    </div>
                    <div class="client-comment">
                        <h3>Installation was pretty easy.We have been Arribo customers for years, and we have had nothing but amazing experiences with the Arribo and well-designed mobile app. Arribo provided that for us with easy-to-use software and personalized support. I like this app. Thank you</h3>
                        <span><i class="icofont-quote-left"></i></span>
                    </div>
                    <div class="client-info">
                        <h3>Fatih Senel</h3>
                        <p>Digilite Web Solutions</p>
                    </div>
                </div>
                <!--end testimonial single-->
            </div>
        </div>
    </section>
    <!--end testimonial area-->
    <!--start app download area-->
    <!--end app download area-->

    <div class="float-right" style="position: fixed; bottom: 20px;right: 30px;color: white">
        <a type="button" href="{{url('register')}}" class="btn btn-info  waves-effect waves-light">Sign up for free</a>
    </div>
    <div class="caption-download-btns " style="position: fixed; bottom: 20px;left: 20px;" >
        <ul>
            <li><a href="" ><i class="icofont-telegram"></i></a></li>
        </ul>
    </div>
    <script>

        function fclick() {

            var element= document.getElementById('header');
            element.classList.remove("sticky")
        }
    </script>

    <!--end contact area-->
    <!--start footer-->
@endsection
