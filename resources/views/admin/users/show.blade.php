@extends('templates.admin.master_page')
@section('title_browser')
    اطلاعات کاربر
@endsection
@section('style')
    <style>
        .card-border {
            box-shadow: unset !important;
            border: solid 1px lightgrey !important;
            border-radius: 4px;
        }

        .avatar {
            width: 85px;
            border-radius: 50%;
            border: solid 2px;
        }

        .custom-card-header {
            background: #204f6f;
            padding: 6px;
            color: #fff;
            text-align: center;
            font-weight: bold;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <h3 class="page-title m-0">اطلاعات کاربر</h3>
                        <a target="_blank" href="{{ route('admin.users.login-panel',$user) }}"
                           class="btn btn-primary mx-2 float-right">رفتن به پنل کاربر</a>

                        <form method="post" action="{{ route('admin.users.block_unblock',$user) }}">
                            @csrf
                            @if ($user->status != 2)
                                <button type="button" class="btn btn-danger mx-2 float-right" data-toggle="modal"
                                        data-target="#BlockModal">بلاک کردن کاربر
                                </button>
                                <!-- مدال مربوط به دلیل بلاک کردن کاربر -->
                                <div id="BlockModal" class="modal fade" tabindex="-1" role="dialog"
                                     aria-labelledby="BlockModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title mt-0" id="BlockModalLabel">بلاک کردن کاربر</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <h5 class="font-16 text-left">لطفا دلیل بلاک کردن خود را در باکس زیر شرح
                                                    دهید</h5>
                                                <textarea class="form-control" name="reject_reason" rows="4"></textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary waves-effect"
                                                        data-dismiss="modal">بستن
                                                </button>
                                                <button type="button"
                                                        class="btn btn-primary waves-effect waves-light ajaxStore">بلاک
                                                    شود
                                                </button>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.مدال مربوط به دلیل بلاک کردن کاربر -->
                            @else
                                <button class="btn btn-success mx-2 float-right ajaxStore">رفع بلاک کاربر</button>
                            @endif
                        </form>

                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <!-- end page title -->
    <div class="card">
        <div class="card-header pl-0 pr-0">
            <ul class="nav nav-tabs card-header-tabs  ml-0 mr-0" role="tablist">
                <li class="nav-item w-20 text-center">
                    <a class="nav-link active" id="information-tab_" data-toggle="tab" href="#informationFull"
                       role="tab" aria-controls="information" aria-selected="true">
                        مشخصات کاربر
                    </a>
                </li>
                <li class="nav-item w-20 text-center">
                    <a class="nav-link" id="invest_shares-tab_" data-toggle="tab" href="#invest_shares"
                       role="tab"
                       aria-controls="reports" aria-selected="false">سرمایه گذاری و سهام</a>
                </li>
                <li class="nav-item w-20 text-center">
                    <a class="nav-link" id="increase_wallet-tab_" data-toggle="tab" href="#increase_wallet"
                       role="tab"
                       aria-controls="reports" aria-selected="false">افزایش موجودی</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <!-- محتویات تب اطلاعات کاربر -->
                <div class="tab-pane fade show active" id="informationFull" role="tabpanel"
                     aria-labelledby="information-tab_">

                    <!-- آواتار و اطلاعات نمایشی کاربر -->
                    <div class="card card-border mb-2 flex-md-row flex-sm-column p-2 align-items-center">
                        <div class="">
                            <img src="{{ $user->avatar }}" class="avatar"
                                 style="border-color: {{ $user->status == 1 ? '#3e884f':'#c43d4b' }}">
                        </div>
                        <div
                            class="col-md col-sm-12 d-flex flex-column justify-content-center align-items-md-start align-items-center mt-md-0 mt-2">
                            <h5 class="mb-0"> {{ $user->name }}
                                @if ($user->status == 0)
                                    <span class="badge badge-danger">غیرفعال</span>
                                @elseif($user->status == 2)
                                    <span class="badge badge-danger">بلاک</span>
                                @else
                                    <span class="badge badge-success">فعال</span>
                                @endif
                            </h5>
                        </div>
                    </div><!-- آواتار و اطلاعات نمایشی کاربر -->

                    <!-- اطلاعات شخصی -->
                    <div class="card card-border mb-2">
                        <div class="custom-card-header border-bottom-2">
                            <h6 class="m-0 float-left">اطلاعات شخصی</h6>
                            <button class="btn btn-info float-right btn-sm open-edit" data-form="personal-information">
                                ویرایش
                            </button>
                        </div>
                        <div class="card-body">
                            <form method="post" id="personal-information"
                                  action="{{ route('admin.users.update',$user->id) }}" class="row">
                                @csrf
                                {{ method_field('put') }}
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>نام</label>
                                    <input type="text" class="form-control" name="first_name"
                                           value="{{ $user->first_name }}" disabled>
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>نام خانوادگی</label>
                                    <input type="text" class="form-control" name="last_name"
                                           value="{{ $user->last_name }}" disabled>
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>ایمیل</label>
                                    <input type="email" class="form-control" name="email" value="{{ $user->email }}"
                                           disabled>
                                </div>
                                @if ($user->status != 2)
                                    <div class="form-group col-md-3 col-sm-4">
                                        <label>وضعیت پنل</label>
                                        <select class="form-control" name="status" disabled>
                                            <option value="0" {{ $user->status == 0?'selected':'' }}>غیرفعال</option>
                                            <option value="1" {{ $user->status == 1?'selected':'' }}>فعال</option>
                                        </select>
                                    </div>
                                @endif
                                <div class="form-group col-md-3 col-sm-4 col-6">
                                    <label>رمز عبور <sub>در صورت عدم تغییر پسورد این
                                            فیلد را خالی بگذارید</sub></label>
                                    <div class="input-group">
                                        <input class="form-control" name="password" type="password"
                                               placeholder="Enter password" autocomplete="off">
                                        <div class="input-group-append">
                                            <button class="show-password btn btn-outline-secondary" type="button">
                                                <span><i class="fas fa-eye"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-12">
                                    <button type="button" class="btn btn-success ajaxStore float-right" disabled>ثبت
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div><!--## اطلاعات شخصی ##-->
                </div><!--## محتویات تب اطلاعات کاربر ##-->
                <!-- محتویات تب گزارشات -->
                <div class="tab-pane fade" id="invest_shares" role="tabpanel"
                     aria-labelledby="invest_shares-tab_">
                    <h6 class="mb-4">سرمایه گذاری و سهام</h6>
                    <div class="col-12 table-responsive">
                        <table id="datatable" class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>نوع</th>
                                <th>مقدار</th>
                                <th>درصد سود</th>
                                <th>مقدار سوددهی</th>
                                <th>وضعیت</th>
                                <th>تاریخ ایجاد</th>
                                <th>آخرین تغییر</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invest_shares as $key=>$item)
                                <tr dir="ltr">
                                    <td>{{ $key+1 }}</td>
                                    <td class="{{ $item->type == 'invest'?'text-primary':'text-warning' }} font-weight-bold">
                                        {{ $item->type=='invest'?'سرمایه گذاری':'سهام' }}
                                    </td>
                                    <td>{{ $item->initial_value }} USDT</td>
                                    <td>{{ $item->percent}}%</td>
                                    <td class="text-success font-weight-bold">{{ $item->profit }} {{ $item->type=='invest'?'ctr':'btt' }}</td>
                                    <td class="{{ $item->status==1?' text-success':'text-danger' }} font-weight-bold">
                                        {{ $item->status==1?'فعال':'غیرفعال' }}
                                    </td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>{{ $item->updated_at }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div><!--## محتویات تب گزارشات ##-->
                <!-- محتویات تب کیف پول -->
                <div class="tab-pane fade" id="increase_wallet" role="tabpanel"
                     aria-labelledby="invest_shares-tab_">
                    <form action="{{route('admin.users.change_wallet')}}" method="post">
                        @csrf
                        <input name="user_id" type="hidden" value="{{$user->id}}">
                        <div class="row mt-5">
                            <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                <label for="unit" class="">ارز</label>
                                <select id="unit" name="unit" class="form-control unit">
                                    <option value="">انتخاب کنید...</option>
                                    <option value="USDT">USDT</option>
                                    <option value="ctr">CoinTread</option>
                                    <option value="btt">BitTorrent</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 amount_div">
                                <label for="amount" class="">مقدار</label>
                                <input id="amount" type="text" name="amount" class="form-control @error('amount') has-danger @enderror">
                                @error('amount') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                <label for="total_amount" class="">موجودی</label>
                                <input id="total_amount" type="text" name="total_amount" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                <label for="transact_type" class="">نوع تراکنش</label>
                                <select id="transact_type" name="transact_type" class="form-control">
                                    <option value="increase">افزایش موجودی</option>
                                    <option value="decrease">کاهش موجودی</option>
                                </select>
                            </div>
                        </div>
                        <div class="row justify-content-end mt-3">
                            <div class="form-group col-lg-1 col-md-4 col-sm-6">
                                <button type="submit" class="btn btn-info btn-lg ajaxStore submit">ثبت</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!--## محتویات تب کیف پول ##-->
        </div>
    </div>
    </div>
@endsection

@section('script')
    <script>
        $(".open-edit").click(function () {
            let id = $(this).data('form');
            $(`#${id} *`).removeAttr('disabled').removeClass('disabled');
            $(this).removeClass('btn-info').text('در حال ویرایش');
        });

        $(document).ready(function () {
            $('#unit').on('change', function () {
                $.ajax({
                    type: 'get',
                    url: '{{route('admin.get_amount')}}',
                    data: {
                        unit: $('select.unit').val(),
                        user_id: {{$user->id}},
                    },
                    success: function (data) {
                        $('#total_amount').val(numberFormat(data[0].amount));
                    },
                });
            });

            $('#amount').on('keyup',function (){
                $('.text-danger').remove();
                $('.submit').prop('disabled',false);
                $('.amount_div').removeClass("has-danger");
                let total_amount=parseInt($('#total_amount').val());
                let transact_type=$('#transact_type').val();
                if (parseInt($(this).val()) > total_amount && transact_type === 'decrease'){
                    $('.amount_div').addClass("has-danger");
                    $('.submit').prop('disabled',true);
                    $('.amount_div').append('<span class="text-danger">user asset is less than amount</span>')
                }
            });

            $('#transact_type').on('change',function (){
                $('.text-danger').remove();
                $('.submit').prop('disabled',false);
                $('.amount_div').removeClass("has-danger");
                let total_amount=parseInt($('#total_amount').val());
                let amount=parseInt($('#amount').val());
                if (amount > total_amount && $(this).val() === 'decrease'){
                    $('.amount_div').addClass("has-danger");
                    $('.submit').prop('disabled',true);
                    $('.amount_div').append('<span class="text-danger">user asset is less than amount</span>')
                }
            })
        });
    </script>
@endsection
