@extends('templates.admin.master_page')

@section('title_browser')
    ویرایش اطلاعات کاربر
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs " role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="first-tab" data-toggle="tab" href="#first" role="tab"
                       aria-controls="first" aria-selected="true">مرحله 1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="second-tab" data-toggle="tab" href="#second" role="tab"
                       aria-controls="second" aria-selected="false">مرحله 2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="third-tab" data-toggle="tab" href="#third" role="tab"
                       aria-controls="third" aria-selected="false">مرحله 3</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="first" role="tabpanel"
                     aria-labelledby="first-tab">

                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"> فرم</h5>

                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">شماره موبایل</label>
                                        <input type="text" class="form-control" value="{{$user->mobile}}"
                                               id="inputEmail4" placeholder="شماره موبایل" disabled>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">ایمیل</label>
                                        <input type="text" class="form-control" value="{{$user->email}}"
                                               id="inputEmail4" placeholder="ایمیل" disabled>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade " id="second" role="tabpanel"
                     aria-labelledby="second-tab">

                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"> فرم</h5>

                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">نام بانک</label>
                                        <input disabled type="text" class="form-control" value="{{ (!empty($user->bank[0]))?$user->bank[0]->name:'' }}"
                                               id="inputEmail4" placeholder="نام بانک">
                                    </div>
                                    {{--                                    @foreach($user->bank as $bank)--}}
                                    {{--                                    <div class="form-group col-md-4">--}}
                                    {{--                                        <label for="inputPassword4">شماره حساب</label>--}}
                                    {{--                                        <input disabled type="text" class="form-control" value="{{$bank->account_number}}" id="inputPassword4" placeholder="شماره حساب">--}}
                                    {{--                                    </div>--}}
                                    {{--                                    @endforeach--}}
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">شماره شبا</label>
                                        <input disabled type="email" class="form-control" value="{{ (!empty($user->bank[0]))?$user->bank[0]->sheba_number:'' }}"
                                               id="inputEmail4" placeholder="شماره شبا">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">شماره حساب</label>
                                        <input disabled type="email" class="form-control" value="{{ (!empty($user->bank[0]))?$user->bank[0]->account_number:'' }}"
                                               id="inputEmail4" placeholder="شماره شبا">
                                    </div>
                                </div>

                                @if($user->step_complate <= '2' && $user->step >= '3')
                                    <a class="btn btn-success text-white  mt-3"
                                       href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3')}}">تایید
                                        صلاحیت</a>
                                    <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(2)">رد صلاحیت</a>
                                @elseif($user->step_complate >= '3'  )
                                    <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(2)"> غیر فعال </a>
                                @else

                                @endif
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="third" role="tabpanel" aria-labelledby="third-tab">


                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"> فرم</h5>

                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">نام</label>
                                        <input disabled type="text" class="form-control" value="{{$user->first_name}}"
                                               id="inputEmail4" placeholder="نام">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">نام خانوادگی</label>
                                        <input disabled type="text" class="form-control" value="{{$user->last_name}}"
                                               id="inputPassword4" placeholder="نام خانوادگی">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">جنسیت</label>
                                        <input disabled type="email" class="form-control" value=
                                                        @switch($user->gender_id)
                                                            @case(1)
                                                                  مرد
                                                            @break
                                                            @case(2)
                                                                زن
                                                            @break
                                                                @default
                                                                   نامشخص
                                                        @endswitch
                                               id="inputEmail4" placeholder="جنسیت">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">تاریخ تولد</label>
                                        <input disabled type="text" class="form-control" value="{{$user->birth_day}}"
                                               id="inputEmail4" placeholder="تاریخ تولد">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">کد ملی</label>
                                        <input disabled type="text" class="form-control"
                                               value="{{$user->national_code}}" id="inputPassword4"
                                               placeholder="کد ملی">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">شماره تلفن ثابت</label>
                                        <input disabled type="email" class="form-control" value="{{$user->phone}}"
                                               id="inputEmail4" placeholder="شماره تلفن ثابت">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputCity">شهر</label>
                                        <input disabled type="text" class="form-control" value="{{!empty($user->city_name->name) ? $user->city_name->name : ' '}}"
                                               id="inputCity">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputState">استان</label>
                                        <input disabled type="text" class="form-control" value="{{!empty($user->state_name->name) ? $user->state_name->name : ' '}}"
                                               id="inputState">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputCity">آدرس</label>
                                        <textarea type="text" class="form-control"
                                                  id="inputCity" disabled>{{$user->address}}</textarea>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="col-xs-6 col-lg-3 col-12 mb-4">
                                        <div class="card bg-dark text-white">
                                            <img class="card-img"
                                                 src="{{ (!empty($images['national']))?getImage($images['national']->path):asset('theme/admin/img/cards/thumb-1.jpg') }}"
                                                 alt="Card image">
                                        </div>
                                        <button type="button" class="btn btn-outline-primary mt-2 " data-toggle="modal"
                                                data-target="#exampleModalPopovers">
                                            کارت ملی
                                        </button>
                                    </div>
                                </div>


                                <div id="exampleModalPopovers" class="modal fade show" tabindex="-1" role="dialog"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalPopoversLabel">تصویر</h5>

                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-center">
                                                <img class="card-img"
                                                     src="{{(!empty($images['national']))?getImage($images['national']->path):asset('theme/admin/img/cards/thumb-1.jpg')}}"
                                                     alt="Card image">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    بستن
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="exampleModalPopovers2" class="modal fade show" tabindex="-1" role="dialog"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalPopoversLabel">تصویر 2</h5>

                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-center">
                                                <img class="card-img"
                                                     src="{{(!empty($images['phone_bill']))?getImage($images['phone_bill']->path):asset('theme/admin/img/cards/thumb-1.jpg')}}"
                                                     alt="Card image">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    بستن
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                @if($user->step_complate <= '3'  && $user->step >= '4')
                                    <a class="btn btn-success text-white  mt-3"
                                       href="{{url('admin/user/changeStatus?id='.$user->id.'&status=4')}}">تایید
                                        صلاحیت</a>
                                    <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(3)">رد صلاحیت</a>
                                @elseif($user->step_complate >= '4' )
                                    <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(3)">غیر فعال</a>
                                @else

                                @endif
                            </form>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="third" role="tabpanel" aria-labelledby="third-tab">
                    <div id="accordion">
                        <div>
                            <button class="btn btn-link p-0 pb-2 font-weight-bold"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <p>1. میتوانم سفارشم را بصورت اقساطی ( اعتباری ) پرداخت کنم؟</p>
                            </button>

                            <div id="collapseOne" class="collapse show" data-parent="#accordion">
                                <div class="pb-4">
                                    بله، دیجی کالا تسهیلاتی را فراهم کرده، شما میتوانید تا سقف 20 میلیون
                                    تومان به صورت اقساط با بازپرداخت 6 ، 9 و 12 ماهه از سایت دیجی کالا
                                    خرید کنید.
                                </div>
                            </div>
                        </div>
                        <div>
                            <button class="btn btn-link collapsed p-0 pb-2 font-weight-bold"
                                    data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                    aria-controls="collapseTwo">
                                <p>2. چطور میتوانم سفارشم را لغو کنم ؟</p>
                            </button>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="pb-4">
                                    <ul>
                                        <li>
                                            شما میتوانید با مراجعه به پروفایل خود سفارش یا مرسوله ایی که
                                            از ارسال آن منصرف شدید را لغو نمایید.
                                        </li>
                                        <li>
                                            میتوانید برای مشاهده روند لغو سفارش به توضیحات تکمیلی مراجعه
                                            کنید.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button class="btn btn-link collapsed p-0 pb-2 font-weight-bold"
                                    data-toggle="collapse" data-target="#collapseThree"
                                    aria-expanded="false" aria-controls="collapseThree">
                                <p>3. رمز عبور خود را فراموش کردم، چطور میتوانم از طریق نسخه وب رمز خود
                                    را بازیابی کنم؟</p>
                            </button>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="pb-4">
                                    لطفا به قسمت توضیحات تکمیلی وارد شوید.
                                </div>
                            </div>
                        </div>
                        <div>
                            <button class="btn btn-link collapsed p-0 pb-2 font-weight-bold"
                                    data-toggle="collapse" data-target="#collapseFour" aria-expanded="false"
                                    aria-controls="collapseFour">
                                <p>4. در صورت بروز تاخیر در تامین کالا چطور اطلاع رسانی صورت میگیرد؟</p>
                            </button>
                            <div id="collapseFour" class="collapse" data-parent="#accordion">
                                <div class="pb-4">
                                    در صورتی که تا تاریخ انتخابی شما برای ارسال ،کالا آماده ارسال نشود،
                                    صبح همان روز پیامکی برای شما جهت اطلاع رسانی تاخیر تحویل سفارش ارسال
                                    می شود.
                                </div>
                            </div>
                        </div>
                        <div>
                            <button class="btn btn-link collapsed p-0 pb-2 font-weight-bold"
                                    data-toggle="collapse" data-target="#collapseFive" aria-expanded="false"
                                    aria-controls="collapseFive">
                                <p>5. چه کالاهایی از طریق باربری ارسال می شوند؟</p>
                            </button>
                            <div id="collapseFive" class="collapse" data-parent="#accordion">
                                <div class="pb-4">
                                    باربری برای لوازم خانگی سنگین یا بزرگ مانند یخچال معمولی و
                                    ساید‌بای‌ساید، ماشین لباس‌شویی، ماشین ظرف‌شویی، اجاق گاز و فرتوکار،
                                    پیانو، میز تلویزیون ،کولر گازی ، کولرآبی ،مبلمان اداری و غیر اداری،
                                    کتابخانه، میز تلویزیون، میز کامپیوتر، میز جلومبلی، میز برش، میز
                                    ناهارخوری ، صندلی ماساژور، ماشین بازی بزرگ، تردمیل، موتوربرق، پرینتر
                                    و دستگاه کپی حجیم و ایستاده و سایر دستگاه‌های حجیم است و کالا
                                    به‌صورت پس‌کرایه (پرداخت هزینه باربری هنگام تحویل مرسوله) ارسال
                                    خواهد شد.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        function rejectUser(status) {

            swal("دلیل لغو رد صلاحیت خود را بنویسید:", {
                content: "input",
            })
            .then((value) => {
                if(value !== null){
                    $.ajax({

                        url: "{{url('admin/user/disableStatus')}}",
                        type: 'get',
                        data: {status: status, id: "{{$user->id}}", value: value, "_token": "{{csrf_token()}}"},
                        headers:
                            {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            },
                        success: function (data) {
                            swal('موفق', data['msg'], data['type']);
                            setTimeout(function () {
                                location.reload();
                            }, 2000)
                        }
                    });
                    }
                });
        }
    </script>
@endsection
