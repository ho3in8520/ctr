@extends('templates.admin.master_page')
@section('title_browser')
    مدیریت کاربران
@endsection
@section('style')
    <style>
        .table > tbody > tr > td, .table > tfoot > tr > td, .table > thead > tr > td {
            padding-top: 0px;
            padding-bottom: 0px;
            vertical-align: baseline;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h3 class="page-title m-0">کاربران</h3>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12 mb-4">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-2">
                        <div class="card-body">
                            <form method="get" action="" class="row">
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>کد: </label>
                                    <input type="text" class="form-control" name="code" value="{{ request()->code }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>ایمیل: </label>
                                    <input type="email" class="form-control" name="email" value="{{ request()->email }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>نام: </label>
                                    <input type="text" class="form-control" name="first_name" value="{{ request()->first_name }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>نام خانوادگی: </label>
                                    <input type="text" class="form-control" name="last_name" value="{{ request()->last_name }}">
                                </div>

                                <div class="form-group col-md-3 col-sm-4">
                                    <label>وضعیت پنل: </label>
                                    <select class="form-control custom-select" name="status">
                                        <option value="" {{ !request()->status?'selected':'' }}>همه</option>
                                        <option value="1" {{ request()->status=='1'?'selected':'' }}>فعال</option>
                                        <option value="0" {{ request()->status=='0'?'selected':'' }}>غیرفعال</option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <button type="submit" class="btn btn-success float-right">جستجو</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card m-b-30">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="float-right p-2">
                                    <a title="خروجی فایل excel" data-export="excel" class="btn btn-success pointer text-white export {{ ($users->total() == 0)?'disabled':'' }}"><i class="fa fa-file-excel"></i> </a>
                                    <a title="خروجی فایل pdf" data-export="pdf" class="btn btn-primary pointer text-white export {{ ($users->total() == 0)?'disabled':'' }}"><i class="fa fa-file-pdf"></i> </a>
                                </div>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>کد</th>
                                        <th>ایمیل</th>
                                        <th>نام و نام خانوادگی</th>
                                        <th>وضعیت پنل</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>
                                                <p class="text-muted">{{ $user->code }}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted">{{ $user->email }}</p>
                                            </td>
                                            <td>
                                                <p class="list-item-heading">{{ $user->name }}</p>
                                            </td>
                                            <td>
                                                @if($user->status == 1)
                                                    <h5><span class="badge badge-success">فعال</span></h5>
                                                @else
                                                    <h5><span class="badge badge-danger">غیرفعال</span></h5>
                                                @endif
                                            </td>
                                            <td>
                                                <a title="نمایش مشخصات کاربر" class="btn btn-info btn-sm"
                                                   href="{{route('admin.users.show',$user->id)}}"
                                                   style="color: #fff;cursor: pointer"><i class="fa fa-eye" style="vertical-align: inherit;"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $users->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
