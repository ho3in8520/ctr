@extends('templates.admin.master_page')
@section('title_browser')
    پروفایل
@endsection

@section('content')
    <section>
        <form method="post" action="{{route('admin.change-password',$user->id)}}">
            @csrf
                @method('PUT')
                <!-- مدال مربوط به تغییر رمز عبور مدیر -->
                <div id="change_pass_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="change_pass_modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0" id="BlockModalLabel">تغییر رمز عبور</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group text-left">
                                    <label>رمز عبور جدید</label>
                                    <input type="password" class="form-control"
                                           name="password">
                                </div>
                                <div class="form-group text-left">
                                    <label>تکرار رمز عبور</label>
                                    <input type="password" class="form-control"
                                           name="password_confirmation">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">کنسل</button>
                                <button type="button" class="btn btn-info waves-effect waves-light ajaxStore">تغییر رمز</button>
                            </div>
                        </div>
                    </div>
                </div><!-- /.مدال مربوط به تغییر رمز عبور مدیر -->

        </form>
        <div class="row pt-3">
            <h4>پروفایل</h4>
        </div>
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('admin.profile.update',$user->id)}}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="row justify-content-center">
                                <div class="col-5">
                                    <div class="form-group">
                                        <label>نام</label>
                                        <input type="text" class="form-control" name="name"
                                               placeholder="نام را وارد کنید" value="{{$user->name}}">
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="form-group">
                                        <label>نام خانوادگی</label>
                                        <input type="text" class="form-control"
                                               name="family"
                                               placeholder="نام خانوادگی را وارد کنید" value="{{$user->family}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center mt-2">
                                <div class="col-5">
                                    <div class="form-group">
                                        <label>ایمیل</label>
                                            <input type="email" class="form-control" readonly
                                                   name="email" value="{{$user->email}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-b-0 text-center mt-2">
                                    <button type="button" id="my_button"
                                            class="btn btn-info waves-effect waves-light ajaxStore">
                                        ویرایش
                                    </button>
                                <button type="button" id="change_pass"
                                        class="btn btn-warning waves-effect waves-light " data-target="#change_pass_modal" data-toggle="modal">
                                    تغییر رمز عبور
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

