@extends('templates.admin.master_page')
@section('title_browser') CTR @endsection
@section('style')
    <style>
        .swal-text {
            direction: ltr;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-12">
                        <h4 class="page-title m-0">
                            CTR
                        </h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>

    <!-- Setting to buy ctr -->
    <div class="row">
        <div class="card col-12">
            <div class="card-body">
                <h5 class="mb-3">تنظیمات سود</h5>
                <form class="form row container-row data-form" method="post"
                      action="{{ route('admin.ctr.inviter_profit.store') }}">
                    @csrf
                    <div class="form-group col-md-4">
                        <label>نوع ارز</label>
                        <select class="form-control" name="unit">
                            <option value="ctr" {{ $profit_inviter->extra_field1=='ctr'?'selected':'' }}>ctr</option>
                            <option value="btt" {{ $profit_inviter->extra_field1=='btt'?'selected':'' }}>btt</option>
                            <option value="usdt" {{ $profit_inviter->extra_field1=='usdt'?'selected':'' }}>usdt</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>درصد سود</label>
                        <input type="text" class="form-control" name="percent"
                               value="{{ $profit_inviter->extra_field2 }}">
                    </div>
                    <div class="form-group col-md-4 d-flex">
                        <button type="submit" class="btn btn-info float-left ajaxStore mt-auto">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- Setting to buy ctr -->
@endsection
@section('script')
    <script>

    </script>
@stop
