@extends('templates.admin.master_page')
@section('title_browser')
    واریز و برداشت
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h3 class="page-title m-0">تنظیمات واریز و برداشت</h3>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <form action="{{route('admin.settings.currency.store')}}" method="post">
                        @csrf
                        <div class="form-group row @error('min_withdraw_usdt') has-danger @enderror">
                            <label for="example-text-input" class="col-sm-2 col-form-label">حداقل برداشت usdt</label>
                            <div class="col-lg-3 col-md-5 col-sm-10 my-auto">
                                <input type="number" name="min_withdraw_usdt" min="0"
                                       value="{{ $currencies_value['min_withdraw_usdt'] }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row @error('fee_withdraw_usdt') has-danger @enderror">
                            <label for="example-text-input" class="col-sm-2 col-form-label">کارمزد برداشت usdt</label>
                            <div class="col-lg-3 col-md-5 col-sm-10 my-auto">
                                <input type="number" name="fee_withdraw_usdt" min="0"
                                       value="{{ $currencies_value['fee_withdraw_usdt'] }}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row mt-5 @error('min_withdraw_ctr') has-danger @enderror">
                            <label for="example-text-input" class="col-sm-2 col-form-label">حداقل برداشت ctr</label>
                            <div class="col-lg-3 col-md-5 col-sm-10 my-auto">
                                <input type="number" name="min_withdraw_ctr" min="0"
                                       value="{{ $currencies_value['min_withdraw_ctr'] }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row @error('fee_withdraw_ctr') has-danger @enderror">
                            <label for="example-text-input" class="col-sm-2 col-form-label">کارمزد برداشت ctr</label>
                            <div class="col-lg-3 col-md-5 col-sm-10 my-auto">
                                <input type="number" name="fee_withdraw_ctr" min="0"
                                       value="{{ $currencies_value['fee_withdraw_ctr'] }}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row mt-5 @error('min_withdraw_btt') has-danger @enderror">
                            <label for="example-text-input" class="col-sm-2 col-form-label">حداقل برداشت btt</label>
                            <div class="col-lg-3 col-md-5 col-sm-10 my-auto">
                                <input type="number" name="min_withdraw_btt" min="0"
                                       value="{{ $currencies_value['min_withdraw_btt'] }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row @error('fee_withdraw_btt') has-danger @enderror">
                            <label for="example-text-input" class="col-sm-2 col-form-label">کارمزد برداشت btt</label>
                            <div class="col-lg-3 col-md-5 col-sm-10 my-auto">
                                <input type="number" name="fee_withdraw_btt" min="0"
                                       value="{{ $currencies_value['fee_withdraw_btt'] }}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12 my-auto">
                                <button type="submit" class="btn btn-success ajaxStore float-right">ذخیره تنظیمات
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
