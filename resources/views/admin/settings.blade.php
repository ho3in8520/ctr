@extends('templates.admin.master_page')
@section('title_browser')
    تنظیمات
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h3 class="page-title m-0">تنظیمات</h3>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <form action="{{ route('admin.settings.general.store') }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">قیمت ارز ctr <sub>بر اساس
                                    تتر</sub></label>
                            <div class="col-lg-3 col-md-5 col-sm-10 my-auto">
                                <input type="text" name="ctr_price" min="0" value="{{ $general['ctr_price'] }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 my-auto">
                                <button type="button" class="btn btn-success ajaxStore float-right">ذخیره تنظیمات
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-header">
                    پیام خوش آمد کاربر
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.settings.message.update') }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">عنوان</label>
                            <div class="col-lg-3 col-md-5 col-sm-10 my-auto">
                                <input type="text" name="extra_field1" value="{{ ($message->extra_field1)?$message->extra_field1:'' }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">متن پیام</label>
                            <div class="col-lg-8 col-md-5 col-sm-10 my-auto">
                                <textarea class="form-control" rows="4" name="extra_field2">{{ ($message->extra_field2)?$message->extra_field2:'' }}</textarea>
                            </div>
                            @if(!empty($message->extra_field3))
                            <div class="col-lg-2 col-md-5 col-sm-10 my-auto">
                                <table class="table table-bordered table-striped">
                                    <thead><tr><th>مقادیر</th></tr></thead>
                                    <tbody>
                                    @foreach(json_decode($message->extra_field3) as $item)
                                        <tr>
                                            <td>{{ $item }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                                @endif
                        </div>
                        <div class="form-group row">
                            <div class="col-12 my-auto">
                                <button type="button" class="btn btn-success ajaxStore float-right">ذخیره
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
