@extends('templates.admin.auth.master_page')
@section('title_browser')
Admin Login
@endsection
@section('content')
    <div class="col-lg-5">
        <div class="card mb-0">
            <div class="card-body">
                <div class="p-2">
                    <h4 class="text-muted float-right font-18 mt-4">ورود</h4>
                    <div>
                        <a href="" class="logo logo-admin"><img src="{{ asset('theme/admin/assets/images/CTR-logo.png') }}" height="50"
                                                                          alt="logo"></a>
                    </div>
                </div>

                <div class="p-2">
                    <form class="form-horizontal m-t-20" method="post" action="{{ route('admin.login') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-12">
                                <input class="form-control @error('email') has-danger @enderror @error('not_match') has-danger @enderror" type="email" name="email" required="" placeholder="email">
                                @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                                @error('not_match') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <input class="form-control @error('password') has-danger @enderror @error('not_match') has-danger @enderror" type="password" name="password" required="" placeholder="Password">
                                @error('password') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">مرا بخاطر بسپار</label>
                                </div>
                            </div>
                        </div>
                        {!! htmlFormSnippet() !!}
                        @error('g-recaptcha-response') <span class="text-danger">{{ $message }}</span> @enderror
                        <div class="form-group text-center row m-t-20">
                            <div class="col-12">
                                <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">
                                    ورود
                                </button>
                            </div>
                        </div>

<!--                        <div class="form-group m-t-10 mb-0 row">
                            <div class="col-sm-7 m-t-20">
                                <a href="pages-recoverpw.html" class="text-muted"><i class="mdi mdi-lock"></i> Forgot
                                    your password?</a>
                            </div>
                            <div class="col-sm-5 m-t-20">
                                <a href="pages-register.html" class="text-muted"><i class="mdi mdi-account-circle"></i>
                                    Create an account</a>
                            </div>
                        </div>-->
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
