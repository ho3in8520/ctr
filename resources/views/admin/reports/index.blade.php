@extends('templates.admin.master_page')
@section('title_browser')
    گزارشات سامانه
@endsection
@section('content')
    <section id="basic-form-layouts">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-12">
                        <h4 class="page-title m-0">گزارشات سامانه</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="row" method="get" action="">
                        <div class="form-group col-md-3">
                            <label>واریز/برداشت</label>
                            <select name="type" class="form-control">
                                <option value="" {{ !request()->type?'selected':'' }}>همه</option>
                                <option value="2" {{ request()->type==2?'selected':'' }}>واریز</option>
                                <option value="1"  {{ request()->type==1?'selected':'' }}>برداشت</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>مقدار</label>
                            <input type="number" class="form-control" name="amount" value="{{ request()->amount }}">
                        </div>
                        <div class="form-group col-md-3">
                            <label>تاریخ شروع</label>
                            <input type="datetime-local" class="form-control" name="start_created_at" value="{{ request()->start_created_at }}">
                        </div>
                        <div class="form-group col-md-3">
                            <label>تاریخ پایان</label>
                            <input type="datetime-local" class="form-control" name="end_created_at" value="{{ request()->start_created_at }}">
                        </div>
                        <div class="form-group col-md-3">
                            <label>ارز</label>
                            <select name="currency" class="form-control">
                                <option value="">همه</option>
                                @foreach($assets as $item)
                                    <option value="{{ $item->unit }}" {{ request()->currency==$item->unit?'selected':'' }}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>کد</label>
                            <input type="number" class="form-control" name="code" value="{{ request()->code }}">
                        </div>
                        <div class="form-group col-md-3">
                            <label>ایمیل</label>
                            <input type="text" class="form-control" name="email" value="{{ request()->email }}">
                        </div>
                        <div class="col-12">
                            <button type="button" class="btn btn-success float-right search-ajax">جستجو</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="content">
                        <div class="table-responsive">
                            <div class="float-right p-2">
                                <a title="خروجی فایل excel" data-export="excel" class="btn btn-success pointer text-white export {{ ($reports->total() == 0)?'disabled':'' }}"><i class="fa fa-file-excel"></i> </a>
                                <a title="خروجی فایل pdf" data-export="pdf" class="btn btn-primary pointer text-white export {{ ($reports->total() == 0)?'disabled':'' }}"><i class="fa fa-file-pdf"></i> </a>
                            </div>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>واریز/برداشت</th>
                                    <th>کد</th>
                                    <th>ایمیل</th>
                                    <th>مقدار</th>
                                    <th>ارز</th>
                                    <th>توضیحات</th>
                                    <th>تاریخ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (count($reports) > 0)
                                    @foreach($reports as $key=>$item)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td><span style="font-size: 13px;" class="badge badge-{{ $item->type==1?"danger":"success" }}">{{ $item->type==1?"برداشت":"واریز" }}</span> </td>
                                            <td>{{ $item->user->code }}</td>
                                            <td>{{ $item->user->email }}</td>
                                            <td>{{ $item->amount }}</td>
                                            <td>{{ $item->financeable instanceof \App\Models\Invest_Shares ? $item->financeable->asset : $item->financeable->unit }}</td>
                                            <td>{{ $item->description }}</td>
                                            <td>{{ $item->created_at }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7" class="text-center">No row</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {{ $reports->withQueryString()->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
@endsection
@section('script')
@endsection
