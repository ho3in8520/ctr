@extends('templates.admin.master_page')
@section('title_browser')
    New Ticket
@endsection
@section('style')
    {!! htmlScriptTagJsApi() !!}
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">NEW TICKET</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row ticket">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body card-body-container">
                    <div class="content">
                        <h5>title ticket</h5>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-body">
                                    <form method="post" action="{{ route("ticket.store") }}" enctype="multipart/form-data">
                                        @csrf
                                        <FIELDSET>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10 @error('email') has-danger @enderror">
                                                    <input type="email" class="form-control" id="email"
                                                           placeholder="email" value="{{auth()->guard('admin')->user()->email}}" disabled>
                                                    @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="title" class="col-sm-2 col-form-label">Title</label>
                                                <div class="col-sm-10 @error('title') has-danger @enderror">
                                                    <input type="text" class="form-control" name="title" id="title"
                                                           placeholder="title">
                                                    @error('title') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="title" class="col-sm-2 col-form-label">Priority</label>
                                                <div class="col-sm-10 @error('priority') has-danger @enderror">
                                                    <select class="form-control" name="priority">
                                                        <option value="0" class="text-warning">Low</option>
                                                        <option value="1" class="text-info">Medium</option>
                                                        <option value="2" class="text-danger">High</option>
                                                    </select>
                                                    @error('priority') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="description"
                                                       class="col-sm-2 col-form-label">Description</label>
                                                <div class="col-sm-10 @error('description') has-danger @enderror">
                                                    <textarea class="form-control" rows="7" name="description"></textarea>
                                                    @error('description') <span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="description"
                                                       class="col-sm-2 col-form-label">Attachment</label>
                                                <div class="col-xs-10 col-sm-10 col-md-10 row @error('file') has-danger @enderror">
                                                    <input type="file" name="attachment"
                                                           class="btn col-xs-12 col-sm-5 form-control">
                                                </div>
                                            </div>
                                            @error('attachment') <span class="text-danger">{{ $message }}</span> @enderror
                                            <hr>
                                            <div class="form-group row">
                                                {!! htmlFormSnippet() !!}
                                            </div>
                                            @error('g-recaptcha-response') <span class="text-danger">{{ $message }}</span> @enderror
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success">register</button>
                                            </div>
                                        </FIELDSET>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(document).on("click",'.ajaxStore',function () {
                $(".image-captcha").attr('src','/captcha/image?_=1163735864&amp;_='+Math.random())
            })
        })
    </script>
    @endsection
