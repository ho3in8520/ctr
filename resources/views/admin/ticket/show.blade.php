@extends('templates.admin.master_page')
@section('title_browser')
    مشاهده تیکت {{ $result->code }}
@endsection
@section('style')
    {!! htmlScriptTagJsApi() !!}
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">مشاهده تیکت</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row ticket">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-container">
                    مشاهده تیکت {{ $result->code }}
                </div>
                <div class="card-body card-body-container">
                    @if($result->status!=3)
                    <span class="float-right">
                        <button type="button" data-key="{{ $result->id }}" class="btn btn-primary close-ticket">بستن تیکت</button>
                    </span>
                    @endif
                    <div class="content">
                        <h5>{{ $result->title }}</h5>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card shadow mt-2 mb-4">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">تاریخ ایجاد
                                                : {{ $result->created_at }}</div>
                                            <div class="col-md-4 col-sm-12">آخرین آپدیت
                                                : {{ $result->ticket_detail[0]->created_at }}</div>
                                            <div class="col-md-4 col-sm-12">وضعیت: {{ $result->status('fa') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-success mb-4" type="button" data-toggle="collapse"
                                        data-target="#collapseExample" aria-expanded="false"
                                        aria-controls="collapseExample">
                                    پاسخ
                                </button>
                                <div class="collapse {{ ($errors->any())?'show':'' }}" id="collapseExample">
                                    <div class="card card-body">
                                        <form method="post" action="{{ route("admin.ticket.update",$result->code) }}" enctype="multipart/form-data">
                                            @csrf
                                            <FIELDSET>
                                                <div class="form-group row">
                                                    <label for="email" class="col-sm-2 col-form-label">ایمیل</label>
                                                    <div class="col-sm-10 @error('email') has-danger @enderror">
                                                        <input type="email" class="form-control" id="email"
                                                               placeholder="email" value="{{ $result->user->email }}"
                                                               disabled>
                                                        @error('email') <span
                                                            class="text-danger">{{ $message }}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="title" class="col-sm-2 col-form-label">عنوان</label>
                                                    <div class="col-sm-10 @error('title') has-danger @enderror">
                                                        <input type="text" class="form-control" id="title"
                                                               placeholder="title" value="{{ $result->title }}"
                                                               disabled>
                                                        @error('title') <span
                                                            class="text-danger">{{ $message }}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="description"
                                                           class="col-sm-2 col-form-label">توضیحات</label>
                                                    <div class="col-sm-10 @error('description') has-danger @enderror">
                                                        <textarea class="form-control" rows="7" name="description"
                                                                  id="description">{{ old('description') }}</textarea>
                                                        @error('description') <span
                                                            class="text-danger">{{ $message }}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="description"
                                                           class="col-sm-2 col-form-label">فایل ضمیمه</label>
                                                    <div
                                                        class="col-xs-10 col-sm-10 col-md-10 row @error('file') has-danger @enderror">
                                                        <input type="file" name="attachment"
                                                               class="btn col-xs-12 col-sm-5 form-control">
                                                    </div>
                                                </div>
                                                @error('attachment') <span
                                                    class="text-danger">{{ $message }}</span> @enderror
                                                <hr>
                                                <div class="form-group row">
                                                    {!! htmlFormSnippet() !!}
                                                </div>
                                                @error('g-recaptcha-response') <span
                                                    class="text-danger">{{ $message }}</span> @enderror
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success">ثبت</button>
                                                </div>
                                            </FIELDSET>
                                        </form>
                                    </div>
                                </div>
                                @foreach($result->ticket_detail as $item)
                                    @if($item->type==1)
                                        <div class="card card-question-answer shadow">
                                            <div class="card-header card-secondary text-white">
                                                <i class="fa fa-comment"></i>
                                                <span class="font-weight-bold name">{{ $item->user->email }}</span>
                                            </div>
                                            <div class="card-body">
                                                {{ $item->description }}
                                            </div>
                                            <div class="card-footer">
                                                <i class="mdi mdi-clock-outline"></i>
                                                <span>{{ \Illuminate\Support\Carbon::parse($item->created_at)->format('Y-m-d H:i') }}</span>
                                                @if($item->attachment)
                                                    <span class="float-right">
                                                        <a title="Attachment" target="_blank"
                                                           href="{{ get_file_ticket($item->attachment) }}">
                                                    <i class="fa fa-file">Attachment </i>
                                                            </a>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    @else
                                        <div class="card card-question-answer shadow">
                                            <div class="card-header card-primary text-white">
                                                <i class="fa fa-comment"></i>
                                                <span
                                                    class="font-weight-bold name">{{ $item->admin->email }} | Support</span>
                                            </div>
                                            <div class="card-body">
                                                {{ $item->description }}
                                            </div>
                                            <div class="card-footer">
                                                <i class="mdi mdi-clock-outline"></i>
                                                <span>{{ \Illuminate\Support\Carbon::parse($item->created_at)->format('Y-m-d H:i') }}</span>
                                                @if($item->attachment)
                                                    <span class="float-right">
                                                        <a title="Attachment" target="_blank"
                                                           href="{{ get_file_ticket($item->attachment) }}">
                                                    <i class="fa fa-file">Attachment </i>
                                                            </a>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(".close-ticket").click(function () {
                var val = $(this).data('key');
                swal({
                    title: "اطمینان از بستن تیکت دارید؟",
                    icon: "warning",
                    buttons: ['انصراف', 'تایید'],
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.post(`{{ route('admin.ticket.close') }}`, {
                                _token: `{{ csrf_token() }}`,
                                'key': val
                            }, function (response) {
                                if (response.status == 100) {
                                    swal('موفق!', response.msg, 'success')
                                    setTimeout(function () {
                                        location.replace("{{ route('admin.ticket.index') }}")
                                    }, 3000)
                                } else {
                                    swal('خطا!', response.msg, 'error')
                                    setTimeout(function () {
                                        window.location.reload()
                                    }, 3000)
                                }
                            })
                        }
                    });
            })
        })
    </script>
@endsection
