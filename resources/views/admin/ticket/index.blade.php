@extends('templates.admin.master_page')
@section('title_browser')
    لیست تیکت ها
@endsection
@section('content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h4 class="page-title m-0"> لیست تیکت ها</h4>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end page-title-box -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="get" action="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>کد تیکت</label>
                                        <input class="form-control" name="code" value="{{ request()->query('code') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>عنوان</label>
                                        <input class="form-control" name="title"
                                               value="{{ request()->query('title') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>الویت</label>
                                        <select class="form-control" name="priority">
                                            <option value="">انتخاب کنید...</option>
                                            <option value="3" {{ (request()->query('priority')==3)?'selected':'' }}>کم
                                            </option>
                                            <option value="1" {{ (request()->query('priority')==1)?'selected':'' }}>
                                                متوسط
                                            </option>
                                            <option value="2" {{ (request()->query('priority')==2)?'selected':'' }}>زیاد
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>وضعیت</label>
                                        <select class="form-control" name="status">
                                            <option value="">انتخاب کنید...</option>
                                            <option value="1" {{ (request()->query('status')==1)?'selected':'' }}>
                                                درانتظار
                                                پاسخ
                                            </option>
                                            <option value="2" {{ (request()->query('status')==2)?'selected':'' }}>پاسخ
                                                داده
                                                شده
                                            </option>
                                            <option value="3" {{ (request()->query('status')==3)?'selected':'' }}>بسته
                                                شده
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary mt-4 search-ajax">اعمال فیلتر
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">کد</th>
                                <th scope="col">عنوان</th>
                                <th scope="col">الویت</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">تاریخ ثبت</th>
                                <th scope="col">عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($result->total())
                            @foreach($result as $item)
                                @php
                                    $status_type=null;
                                    $status_class=null;
                                       $priority_type=null;
                                    $priority_class=null;
                                        switch ($item->status){
            case 1;
            $status_class='warning';
            $status_type='در انتظار پاسخ';
            break;
            case 2;
            $status_class='info';
            $status_type='پاسخ داده شده';
            break;
            case 3;
            $status_class='secondary';
            $status_type='بسته شده';
            break;

        }
        switch ($item->priority){
            case 0;
            $priority_class='warning';
            $priority_type='کم';
            break;
            case 1;
            $priority_class='info';
            $priority_type='متوسط';
            break;
            case 2;
            $priority_class='danger';
            $priority_type='زیاد';
            break;

        }
                                @endphp
                                <tr>
                                    <td>{{ index($result,$loop) }}</td>
                                    <td>{{ $item->code }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td><span class="badge badge-{{ $priority_class }}">{{ $priority_type }}</span></td>
                                    <td><span class="badge badge-{{ $status_class }}">{{ $status_type }}</span></td>
                                    <td>{{ \Carbon\Carbon::parse($item->created_at)->format("Y-m-d") }}</td>
                                    <td>
                                        <a title="مشاهده" class="btn btn-primary btn-sm"
                                           href="{{ route('admin.ticket.show',$item->code) }}"><i
                                                class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            @else
                                <tr>
                                    <th colspan="7" class="text-center">موردی یافت نشد</th>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        {!! $result->appends(request()->query())->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('general/js/popup.js') }}"></script>
@endsection
