<!doctype html>
<html lang="zxx">


<!-- Mirrored from pixner.net/arribo/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Dec 2019 11:42:43 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>CTR</title>
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="{{asset('theme/landing/images/logo-rtc4.png')}}" />
    <!--bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('theme/landing/css/bootstrap.min.css')}}">
    <!--owl carousel css-->
    <link rel="stylesheet" type="text/css" href="{{asset('theme/landing/css/owl.carousel.min.css')}}">
    <!--magnific popup css-->
    <link rel="stylesheet" type="text/css" href="{{asset('theme/landing/css/magnific-popup.css')}}">
    <!--font awesome css-->
    <link rel="stylesheet" type="text/css" href="{{asset('theme/landing/css/fontawesome-all.min.css')}}">
    <!--icomoon icon css-->
    <link rel="stylesheet" type="text/css" href="{{asset('theme/landing/css/icomoon.css')}}">
    <!--icofont css-->
    <link rel="stylesheet" type="text/css" href="{{asset('theme/landing/css/icofont.min.css')}}">
    <!--animate css-->
    <link rel="stylesheet" type="text/css" href="{{asset('theme/landing/css/animate.css')}}">
    <!--main css-->
    <link rel="stylesheet" type="text/css" href="{{asset('theme/landing/css/style.css')}}">
    <!--responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('theme/landing/css/responsive.css')}}">
    <style>
        .goclick  {
            position: relative;
            z-index: 9;

        }


    </style>
    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="6a1a7c25-9230-4098-82f1-0209ceddfc4a";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
</head>

<body ontouchstart="">
<!--Start Preloader-->
<div class="preloader">
    <div class="d-table">
        <div class="d-table-cell align-middle">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
</div>
<!--End Preloader-->
<!--start header-->
<header id="header" >
    <div class="container">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <!-- Logo -->
{{--                <a class="logo mx-1" target="_blank" href="https://btfprofits.com" data-toggle="tooltip" data-placement="bottom" title="BTFprofits.com"><img src="{{asset('theme/landing/images/btf-logo.jpg')}}" alt="logo" width="55" style="border-radius: 50%"></a>--}}
                <a class="logo" title="WHITEPAPER" href="/"><img src="{{asset('theme/landing/images/logo-rtc4.png')}}" alt="logo" width="55"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"><i class="icofont-navigation-menu"></i></span>
                </button>
                <!-- navbar links -->
                <div class="collapse navbar-collapse" id="navbarContent" >
                    <ul class="navbar-nav ml-auto">
                        @if(isset($nav) and $nav === false)
                            <li class="nav-item">
                                <a class="nav-link active" href="{{route('home')}}" >Home</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link active" href="#" data-scroll-nav="0">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-scroll-nav="1">Compensation Plans</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-scroll-nav="2">Ambitions</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-scroll-nav="3">Records</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-scroll-nav="4">Why us?</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-scroll-nav="5">Team</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-scroll-nav="6">Sponsors</a>
                            </li>
                        @endif

                        @if(!(auth()->user()))
                            <li class="nav-item ">
                                <a class="nav-link" href="{{route('login.form')}}" >Login</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="{{route('register.form')}}" >Register</a>
                            </li>
                        @else
                            <li class="nav-item ">
                                <a class="nav-link"  href="{{ route('user.logout') }}">{{ __('Log Out') }}</a>

                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="{{route('user.dashboard')}}" >Dashboard</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!--end header-->
@yield('content')
<footer id="footer" class="bg-1">
    <div class="container">

        <div class="footer-cont">
            <a href="https://icomarks.com/ico/ctr-coin" target="_blank" rel="nofollow" title="CTR Coin ICO"><img border="0" src="https://icomarks.com/widget/c/ctr-coin/horizontal.svg" width="400px" height="125px" alt="CTR Coin ICO Rating"/></a>

            <div class="row">

                <!--start footer widget single-->
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <ul>
                            <li><img src="{{asset('theme/landing/images/buckwallet.png')}}" alt="" width="100"></li>
                            <li><img src="{{asset('theme/landing/images/coinbene.png')}}" alt="" width="100"></li>
                            <li><img src="{{asset('theme/landing/images/coinone1.png')}}" alt="" width="100"></li>
                            <li><img src="{{asset('theme/landing/images/ledger.png')}}" alt=""  width="100"></li>
                            <li><img src="{{asset('theme/landing/images/oapple1.png')}}" alt="" width="50"></li>
                        </ul>
                    </div>
                </div>
                <!--end footer widget single-->
                <!--start footer widget single-->
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <ul>
                            <li><img src="{{asset('theme/landing/images/justswap1-coin.png')}}" alt="" width="120"></li>
                            <li><img src="{{asset('theme/landing/images/renre.png')}}" alt="" width="100"></li>
                            <li><img src="{{asset('theme/landing/images/sam.png')}}" alt="" width="100"></li>
                            <li><img src="{{asset('theme/landing/images/Tronlink.png')}}" alt="" width="100"></li>
                            <li><img src="{{asset('theme/landing/images/poloniex.png')}}" alt="" width="100"></li>
                        </ul>
                    </div>
                </div>
                <!--end footer widget single-->
            </div>
        </div>

        <div class="footer-copyright">
            <div class="row">
                <div>
                    <p class="col-lg-6 col-md-7">
                    <p>&copy; 2021 RTC | All right reserved.</p>
                    <p style="display: block">Address: 916 54 St, Abraj Al Mamzar - United Arab Emirates</p>
                    <p >27 Broadway Suite #2112, New York, NY 10005, United States</p>
                </div>
                <div class="col-lg-6 col-md-5">
                    <div class="footer-social text-right">
                        <ul>
                            <li><a href="#"><i class="icofont-telegram"></i></a></li>
                            <li><a href="#"><i class="icofont-instagram"></i></a></li>
                            <li><a href="#"><i class="icofont-email"></i></a></li>
                            <li><a href="#"><i class="icofont-whatsapp"></i></a></li>
                            <li><a href="#"><i class="icofont-whatsapp"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--end footer-->
<!--jQuery js-->
<script src="{{asset('theme/landing/js/jquery-3.3.1.min.js')}}"></script>

<!--proper js-->
<script src="{{asset('theme/landing/js/popper.min.js')}}"></script>
<!--bootstrap js-->
<script src="{{asset('theme/landing/js/bootstrap.min.js')}}"></script>
<!--counter js-->
<script src="{{asset('theme/landing/js/waypoints.js')}}"></script>
<script src="{{asset('theme/landing/js/counterup.min.js')}}"></script>
<!--magnic popup js-->
<script src="{{asset('theme/landing/js/magnific-popup.min.js')}}"></script>
<!--owl carousel js-->
<script src="{{asset('theme/landing/js/owl.carousel.min.js')}}"></script>
<!--owl scrollIt js-->
<script src="{{asset('theme/landing/js/scrollIt.min.js')}}"></script>
<!--validator js-->
<script src="{{asset('theme/landing/js/validator.min.js')}}"></script>
<!--contact js-->
<script src="{{asset('theme/landing/js/contact.js')}}"></s  cript>
<!--ajax newsletter js-->
<script src="{{asset('theme/landing/js/ajax-newsletter-form.js')}}"></script>
<!--main js-->
<script src="{{asset('theme/landing/js/custom.js')}}"></script>

<script>

    var text = ["Hi! Welcome To Our Website","Live the Life You Deserve", "We Come From a Future with an Opportunity for You Now", "Go Ahead You Can Rely on Us","We Gather the Best for the Best","No Restriction","Trust Worthy Wallet","All Time Unlocked Wallet","Customer Based, Valid Prices","Authentic Whitepaper"];
    var counter = 0;
    var elem = document.getElementById("changeText");
    var inst = setInterval(change, 5000);

    function change() {
        elem.innerHTML = text[counter];
        counter++;
        if (counter >= text.length) {
            counter = 0;
            // clearInterval(inst); // uncomment this if you want to stop refreshing after one cycle
        }
    }
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })



</script>
</body>



<!-- Mirrored from pixner.net/arribo/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Dec 2019 11:44:19 GMT -->
</html>
