<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title_browser')</title>
    <meta content="Admin Dashboard" name="description"/>
    <meta content="ThemeDesign" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="shortcut icon" href="assets/images/favicon.ico">
    {!! htmlScriptTagJsApi() !!}
    @include('templates.user.layouts.header')
    @yield('style')
</head>


<body class="fixed-left">

<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>

<div class="account-pages">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <p style="text-align: left; margin-left: 50px;"><img
                        src="{{ asset('theme/landing/images/logo-rtc4.png') }}"
                        style="width: 260.582px; height: 261px;"><br></p>
                <p style="margin-left: 50px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></p>
            </div>
            @yield('content')
        </div>
        <!-- end row -->
    </div>
</div>
@include('templates.user.layouts.footer')
@yield('script')

</body>
</html>
