<!-- jQuery  -->
<script src="{{ asset('theme/user/assets/js/jquery.min.js') }}"></script>
<script src="{{ mix('/js/app.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/modernizr.min.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/detect.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/fastclick.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/waves.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/jquery.scrollTo.min.js') }}"></script>

<!--Morris Chart-->
<script src="{{ asset('theme/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('theme/plugins/raphael/raphael.min.js') }}"></script>

<!-- dashboard js -->
<script src="{{ asset('theme/user/assets/pages/dashboard.int.js') }}"></script>

<!-- App js -->
<script src="{{ asset('theme/user/assets/js/app.js') }}"></script>
<script src="{{ asset('general/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('general/datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('general/js/func.js') }}"></script>
<script src="{{ asset('general/js/project.js') }}"></script>
<script src="{{ asset('general/tron/TronWeb.js') }}"></script>
{{--<script src="{{ asset('general/tron/ethers-5.0.esm.min.js') }}"></script>--}}
