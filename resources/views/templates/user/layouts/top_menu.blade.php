<!-- Top Bar Start -->
<div class="topbar">
    <nav class="navbar-custom">

        <div style="width: auto !important;" class="topbar-left	d-none d-md-block">
            <div class="text-center">
                <a href="index.html" class="logo"><img src="{{ asset('theme/admin/assets/images/CTR-logo.png') }}"
                                                       height="50"
                                                       alt="logo"></a>
            </div>

        </div>

        <ul class="list-inline d-inline-block menu-left mb-0">
            <li class="list-inline-item">
                <button type="button" class="button-menu-mobile open-left waves-effect">
                    <i class="mdi mdi-menu"></i>
                </button>
            </li>
        </ul>

        <ul class="list-inline float-right mb-0">
            <li class="list-inline-item notification-list">
                <span class="text-white px-3">{{ auth('web')->user()->code }}</span>
            </li>

            <li class="list-inline-item dropdown notification-list nav-user">
                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#"
                   role="button"
                   aria-haspopup="false" aria-expanded="false">
                    <i class="mdi mdi-bell-outline noti-icon"></i>
                    @if(countNotificationNotView() > 0)
                        <span
                            class="badge badge-danger badge-pill noti-icon-badge">{{ countNotificationNotView() }}</span>
                    @endif
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg dropdown-menu-animated">
                    <!-- item-->
                    <div class="dropdown-item noti-title">
                        <h5>Notification ({{ countNotificationNotView() }})</h5>
                    </div>

                    <div class="slimscroll-noti">
                    @if(countNotificationNotView())
                        @foreach(notificationNotView() as $item)
                            <!-- item-->
                                <a href="{{ route('notification.show',base64_encode($item->id)) }}"
                                   class="dropdown-item notify-item">
                                    <div class="notify-icon bg-{{ $item->color }}"><i class="{{ $item->icon }}"></i>
                                    </div>
                                    <p class="notify-details"><b>{{ $item->title }}</b><span
                                            class="text-muted">{!! \Illuminate\Support\Str::limit($item->description,25) !!}</span>
                                    </p>
                                </a>
                            @endforeach
                        @else
                            <a
                                class="dropdown-item notify-item text-center">
                                <p class="notify-details">Nothing found</p>
                            </a>
                        @endif
                    </div>
                    <!-- All-->
                    <a href="{{ route('notification.index') }}" class="dropdown-item notify-all">
                        View All
                    </a>

                </div>
            </li>


            <li class="list-inline-item dropdown notification-list nav-user">
                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#"
                   role="button"
                   aria-haspopup="false" aria-expanded="false">
                    <span class="d-inline-block ml-1" style="font-size: 19px">
                        <i class="mdi mdi-chevron-down"></i>
                        <i class="mdi mdi-account"></i>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown" style="width: auto">
                    <span class="dropdown-item">
                        <i class="dripicons-mail text-muted"></i>
                        {{ auth()->user()->email }}
                    </span>
                    <a class="dropdown-item" href="{{route('profile')}}"><i class="dripicons-user text-muted"></i>
                        Profile</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('user.logout') }}"><i class="dripicons-exit text-muted"></i>
                        Logout</a>
                </div>
            </li>

        </ul>
    </nav>
    <marquee direction="right"  class="navbar-custom .marquee" style="background-color: #ffffff59; padding: 3.5px" scrolldelay="300" onmouseover="this.stop();" onmouseout="this.start();">
        <ul class="list-inline d-inline-block menu-left mb-0 nav-user">
            <li class="ml-4 btn btn-warning btn-sm" style="font-size: 13px;">
                <span>CTR: </span>
                <span>${{ $price_currency['ctr'] }}</span>
            </li>
            <li class="ml-4 btn btn-danger btn-sm" style="font-size: 13px;">
                <span>BTT: </span>
                <span>${{ $price_currency['btt'] }}</span>
            </li>
        </ul>
    </marquee>

</div>
<!-- Top Bar End -->
