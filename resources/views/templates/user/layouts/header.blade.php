<!-- morris css -->
<link rel="stylesheet" href="{{ asset('theme/plugins/morris/morris.css') }}">

<link href="{{ asset('theme/user/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/user/assets/css/fontawesome.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/user/assets/css/icons.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/user/assets/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('general/datePicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/user/assets/css/custom.css') }}" rel="stylesheet" type="text/css">
