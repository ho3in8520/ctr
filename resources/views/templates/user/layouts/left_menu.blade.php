<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="mdi mdi-close"></i>
    </button>

    <div class="left-side-logo d-block d-lg-none">
        <div class="text-center">

            <a href="index.html" class="logo"><img src="assets/images/logo_dark.png" height="20" alt="logo"></a>
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">

        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">Main</li>

                <li>
                    <a href="{{ route('user.dashboard') }}" class="waves-effect">
                        <i class="dripicons-home"></i>
                        <span> Dashboard <span class="badge badge-success badge-pill float-right">3</span></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('user.settings')}}" class="waves-effect">
                        <i class="fas fa-history"></i>
                        <span>Settings</span>
                    </a>
                </li>

                <li>
                    <a href="{{route('withdraw-report')}}" class="waves-effect">
                        <i class="fas fa-history"></i>
                        <span> Transaction History</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('user.invest.index')}}" class="waves-effect">
                        <i class="fas fa-history"></i>
                        <span>Invest</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('user.shares.index')}}" class="waves-effect">
                        <i class="fas fa-history"></i>
                        <span>Shares</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('assets')}}" class="waves-effect">
                        <i class="fas fa-history"></i>
                        <span>Assets</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('user.buy-ctr')}}" class="waves-effect">
                        <i class="fas fa-history"></i>
                        <span>Buy ctr</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('ticket.index')}}" class="waves-effect">
                        <i class="fas fa-history"></i>
                        <span>Ticket</span>
                    </a>
                </li>


                <li class="menu-title">Extra</li>

{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-copy"></i><span> Pages </span> <span class="menu-arrow float-right"><i class="mdi mdi-chevron-right"></i></span></a>--}}
{{--                    <ul class="list-unstyled">--}}
{{--                        <li><a href="pages-blank.html">Blank Page</a></li>--}}
{{--                        <li><a href="pages-login.html">Login</a></li>--}}
{{--                        <li><a href="pages-register.html">Register</a></li>--}}
{{--                        <li><a href="pages-recoverpw.html">Recover Password</a></li>--}}
{{--                        <li><a href="pages-lock-screen.html">Lock Screen</a></li>--}}
{{--                        <li><a href="pages-404.html">Error 404</a></li>--}}
{{--                        <li><a href="pages-500.html">Error 500</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}


            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>
<!-- Left Sidebar End -->
