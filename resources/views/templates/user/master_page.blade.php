<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title_browser')</title>
    <meta content="Admin Dashboard" name="description"/>
    <meta content="ThemeDesign" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="shortcut icon" href="assets/images/favicon.ico">

    @include('templates.user.layouts.header')
    @yield('style')
    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="6a1a7c25-9230-4098-82f1-0209ceddfc4a";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
</head>


<body class="fixed-left">

@include('sweetalert::alert')

<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>

<!-- Begin page -->
<div id="wrapper">

@include('templates.user.layouts.left_menu')

<!-- Start right Content here -->

    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            @include('templates.user.layouts.top_menu')

            <div class="page-content-wrapper ">

                <div class="container-fluid" id="app">
                    @yield('content')
                </div><!-- container fluid -->

            </div> <!-- Page content Wrapper -->

        </div> <!-- content -->

    </div>
    <!-- End Right content here -->

</div>
<!-- END wrapper -->
@include('templates.user.layouts.footer')
@yield('script')
</body>
</html>
