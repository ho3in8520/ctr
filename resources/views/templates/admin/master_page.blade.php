<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title_browser')</title>
    <meta content="Admin Dashboard" name="description"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="ThemeDesign" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="shortcut icon" href="assets/images/favicon.ico">
    @include('templates.admin.layouts.header')
    @yield('style')
</head>


<body class="fixed-left">

<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>

<!-- Begin page -->
<div id="wrapper">

@include('templates.admin.layouts.right_menu')

<!-- Start right Content here -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            @include('templates.admin.layouts.top_menu')
            <div class="page-content-wrapper ">
                <div class="container-fluid">
                    @yield('content')
                </div><!-- container fluid -->

            </div> <!-- Page content Wrapper -->

        </div> <!-- content -->

        <footer class="footer">
            © 2019 CTR <span class="d-none d-md-inline-block"> Crafted with</span>
        </footer>

    </div>
    <!-- End Right content here -->
</div>
<!-- END wrapper -->
@include('templates.admin.layouts.footer')
@yield('script')
</body>
</html>
