<!-- morris css -->
<link rel="stylesheet" href="{{ asset('theme/plugins/morris/morris.css') }}">


<!-- DataTables -->
<link href="{{ asset('theme/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('theme/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('general/datePicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/plugins/bootstrap-iconpicker/css/bootstrap-iconpicker.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/admin/assets/css/icons.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/admin/assets/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/admin/assets/css/custom.css') }}" rel="stylesheet" type="text/css">
