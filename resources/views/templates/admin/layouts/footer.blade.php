<!-- jQuery  -->
<script src="{{ asset('theme/admin/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('theme/admin/assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('theme/admin/assets/js/modernizr.min.js') }}"></script>
<script src="{{ asset('theme/admin/assets/js/detect.js') }}"></script>
<script src="{{ asset('theme/admin/assets/js/fastclick.js') }}"></script>
<script src="{{ asset('theme/admin/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('theme/admin/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('theme/admin/assets/js/waves.js') }}"></script>
<script src="{{ asset('theme/admin/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('theme/admin/assets/js/jquery.scrollTo.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('theme/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<!-- Datatable init js -->
<script src="{{ asset('general/datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('theme/plugins/bootstrap-iconpicker/js/bootstrap-iconpicker.bundle.min.js') }}"></script>
<script src="{{ asset('theme/admin/assets/pages/datatables.init.js') }}"></script>

<!--Morris Chart-->
<script src="{{ asset('theme/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('theme/plugins/raphael/raphael.min.js') }}"></script>

<!-- dashboard js -->
<script src="{{ asset('theme/admin/assets/pages/dashboard.int.js') }}"></script>
<script src="{{ asset('theme/plugins/select2/js/select2.js') }}"></script>
<script src="{{ asset('theme/plugins/ckeditor/ckeditor.js') }}"></script>


<!-- App js -->
<script src="{{ asset('theme/admin/assets/js/app.js') }}"></script>
<script src="{{ asset('general/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('general/js/func.js') }}"></script>
<script src="{{ asset('general/js/popup.js') }}"></script>
<script src="{{ asset('general/js/project.js') }}"></script>

