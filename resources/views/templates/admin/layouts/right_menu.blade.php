<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="mdi mdi-close"></i>
    </button>

    <div class="left-side-logo d-block d-lg-none">
        <div class="text-center">

            <a href="index.html" class="logo"><img src="assets/images/logo_dark.png" height="20" alt="logo"></a>
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">

        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">منو اصلی</li>

                <li>
                    <a href="{{ route('admin.dashboard') }}" class="waves-effect">
                        <i class="dripicons-home"></i>
                        <span> داشبورد <span class="badge badge-success badge-pill float-right">3</span></span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.settings.general') }}" class="waves-effect">
                        <i class="dripicons-gear"></i>
                        <span> تنظیمات </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.users.index') }}" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span> کاربران </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.invest_shares.index',['type'=>'invest']) }}" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span> Invest </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.invest_shares.index',['type'=>'shares']) }}" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span> Shares </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.ctr.index') }}" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span> CTR </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.reports.index') }}" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span> گزارشات </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.notifications.index') }}" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span> پیغام ها </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.settings.withdraws') }}" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span> واریز و برداشت </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.ticket.index') }}" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span> تیکت ها
                        @if(countTicketNotAnswer())
                                <span class="badge badge-danger badge-pill float-right">{{ countTicketNotAnswer() }}</span>
                            @endif
                        </span>
                    </a>
                </li>

{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-medical"></i><span> Extras </span> <span class="menu-arrow float-right"><i class="mdi mdi-chevron-right"></i></span></a>--}}
{{--                    <ul class="list-unstyled">--}}
{{--                        <li><a href="extras-pricing.html">Pricing</a></li>--}}
{{--                        <li><a href="extras-invoice.html">Invoice</a></li>--}}
{{--                        <li><a href="extras-timeline.html">Timeline</a></li>--}}
{{--                        <li><a href="extras-faqs.html">FAQs</a></li>--}}
{{--                        <li><a href="extras-maintenance.html">Maintenance</a></li>--}}
{{--                        <li><a href="extras-comingsoon.html">Coming Soon</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>
<!-- Left Sidebar End -->
