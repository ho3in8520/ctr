<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title_browser')</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="ThemeDesign" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="assets/images/favicon.ico">
    {!! htmlScriptTagJsApi() !!}

    @include('templates.admin.layouts.header')

</head>
<body class="fixed-left">

<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>

<!-- Begin page -->

<div class="account-pages">

    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 offset-lg-1">
                <div class="text-left">
                    <div>
                        <a href="index.html" class="logo logo-admin"><img src="{{ asset('theme/admin/assets/images/logo_dark.png') }}" height="28" alt="logo"></a>
                    </div>
                    <h5 class="font-14 text-muted mb-4">Responsive Bootstrap 4 Admin Dashboard</h5>
                    <p class="text-muted mb-4">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>

                    <h5 class="font-14 text-muted mb-4">Terms :</h5>
                    <div>
                        <p><i class="mdi mdi-arrow-right text-primary mr-2"></i>At solmen va esser necessi far uniform paroles.</p>
                        <p><i class="mdi mdi-arrow-right text-primary mr-2"></i>Donec sapien ut libero venenatis faucibus.</p>
                        <p><i class="mdi mdi-arrow-right text-primary mr-2"></i>Nemo enim ipsam voluptatem quia voluptas sit .</p>
                    </div>
                </div>
            </div>
            @yield('content')
        </div>
        <!-- end row -->
    </div>
</div>

@include('templates.admin.layouts.footer')

</body>
</html>
