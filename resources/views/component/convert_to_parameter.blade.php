<input type="hidden" class="address" value="{{ $address }}">
<input type="hidden" class="amount" value="{{ $amount * 1000000 }}">
<input type="hidden" class="route" value="{{ route('convert-to-parameter-save') }}">
<input type="hidden" class="csrf" value="{{ csrf_token() }}">
<input type="hidden" class="id" value="{{ $id_transact }}">

<script src="{{ asset('theme/user/assets/js/jquery.min.js') }}"></script>
<script type="module" src="{{ asset('general/js/convert_to_parametr.js') }}"></script>
