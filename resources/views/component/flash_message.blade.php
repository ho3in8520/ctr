@if(\Illuminate\Support\Facades\Session::has('flash'))
    <div class="alert alert-{{ session()->get('flash')['type'] }}" role="alert">
        {{ session()->get('flash')['msg'] }}
    </div>
@endif
